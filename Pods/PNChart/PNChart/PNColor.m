//
//  PNColor.m
//  PNChart
//
//  Created by kevin on 13-6-8.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//

#import "PNColor.h"
#import <UIKit/UIKit.h>

@implementation PNColor

	static NSArray *colorArray;

- (instancetype)init{
	self = [super init];
	if(self){

	}
	return self;
}

- (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return img;
}

+ (UIColor*)getColorAtIndex:(NSInteger)index{
	
	colorArray = [[NSArray alloc] initWithObjects: PNLightBlue,  PNGreen, PNButtonGrey, PNLightGreen, PNFreshGreen, PNDeepGreen, PNRed, PNMauve, PNBrown, PNBlue, PNDarkBlue, PNYellow, PNWhite, PNDeepGrey, PNPinkGrey, PNHealYellow, PNLightGrey, PNCleanGrey, PNLightYellow, PNDarkYellow, PNPinkDark, PNCloudWhite, PNBlack, PNStarYellow, PNTwitterColor, PNWeiboColor,nil];
	
	if(index < [colorArray count]){
		return (UIColor*)colorArray[index];
	}
	return PNGreen;
}
@end
