//
//  NotificationCenterManager.h
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/11/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#ifndef MoneyLover_NotificationCenterManager_h
#define MoneyLover_NotificationCenterManager_h

@interface NotificationCenterManager : NSObject
+(id)getInstance;
-(void)createNotificationWithContent:(NSString *)content andTitle:(NSString*)title;
@end

#endif
