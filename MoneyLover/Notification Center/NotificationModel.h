//
//  NotificationModel.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/9/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationModel : NSObject

@property (assign, nonatomic) NSInteger ID;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSString *iconName;
@property (copy, nonatomic) NSString *title;

- (instancetype)initWithContent:(NSString *)content iconName:(NSString *)iconName andTitle:(NSString *)title;

@end
