//
//  NotificationModel.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/9/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "NotificationModel.h"

@implementation NotificationModel

- (instancetype)initWithContent:(NSString *)content iconName:(NSString *)iconName andTitle:(NSString *)title{
	self = [super init];
	if(self){
		self.ID = 0;
		self.content = content;
		self.iconName = iconName;
		self.title = title;
	}
	return self;
}

@end
