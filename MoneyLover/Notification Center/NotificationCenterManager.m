//
//  NotificationCenterManager.m
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/11/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationCenterManager.h"
#import "NotificationModel.h"
#import "SQLiteManager.h"

@implementation NotificationCenterManager

+(id) getInstance {
    static NotificationCenterManager* instance = nil;
    static dispatch_once_t once = 0;
    dispatch_once(&once, ^{instance = [[self alloc] initPrivate];});
    return instance;
}

-(id)initPrivate {
    if ((self = [super init])) {
        
    }
    return self;
}

-(id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(void)createNotificationWithContent:(NSString *)content andTitle:(NSString*)title {
    // Tạo thông báo
    UILocalNotification *localNotifi = [[UILocalNotification alloc] init];
    //	localNotifi.fireDate = [NSDate date];
    //	localNotifi.timeZone = [[NSCalendar currentCalendar] timeZone];
    localNotifi.alertBody = NSLocalizedString(content, nil);
    localNotifi.hasAction = YES;
    localNotifi.alertAction = NSLocalizedString(@"View", nil);
    localNotifi.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    //	NSLog(@"[UIApplication sharedApplication].applicationIconBadgeNumber: %d", [UIApplication sharedApplication].applicationIconBadgeNumber);
    //		localNotifi.userInfo = @{@"SavingName" : [NSString stringWithFormat:@"%@", @"SD"]};
    localNotifi.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotifi];
    [UIApplication sharedApplication].applicationIconBadgeNumber = localNotifi.applicationIconBadgeNumber;
    // Chèn thống báo vào CSDL
    NotificationModel *noti = [[NotificationModel alloc] initWithContent:content iconName:@"ic_bab_budget.png" andTitle:title];
    [[SQLiteManager database] insertNotification:noti];
}

@end
