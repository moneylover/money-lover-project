//
//  NotificationCenter.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/7/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "NotificationCenter.h"
#import "SQLiteManager.h"
#import "NotificationModel.h"
#import "NotificationCell.h"


@interface NotificationCenter () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *notificationArray;
@end

@implementation NotificationCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib
//	[self.tableView registerClass:Notification  forCellReuseIdentifier:@"CellIden"];
	[self.tableView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"CellIden"];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;

	
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	
	self.notificationArray = [[SQLiteManager database] getNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableDataSource

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
					 editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

	return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return _notificationArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 125;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	NotificationCell *cell = (NotificationCell *) [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
	
	NotificationModel *noti = (NotificationModel *)self.notificationArray[indexPath.row];
	cell.titleLabel.text = noti.title;
	cell.iconView.image = [UIImage imageNamed:noti.iconName];
	cell.textView.text = noti.content;
	return cell;
}



#pragma mark - UITableDelegate

- (void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath{
	if (editingStyle == UITableViewCellEditingStyleDelete){
		/* First remove this object from the source */
		NotificationModel *noti = (NotificationModel *)self.notificationArray[indexPath.row];
		[[SQLiteManager database] deleteNotificationWithId:noti.ID];
		
		[self.notificationArray removeObjectAtIndex:indexPath.row];
		/* Then remove the associated cell from the Table View */
		[tableView deleteRowsAtIndexPaths:@[indexPath]
										 withRowAnimation:UITableViewRowAnimationLeft];
	}
}


#pragma mark - IBAction

- (IBAction)backAction:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteNotificationAction:(id)sender {
	
	[[SQLiteManager database] deleteAllNotifications];
	self.notificationArray = [[SQLiteManager database] getNotifications];
	[self.tableView reloadData];
}


@end
