//
//  SQLiteManager.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 7/16/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "SQLiteManager.h"
#import <sqlite3.h>
#import "Transaction.h"
#import "CategoryTransaction.h"
#import "DataSourceInASection.h"
#import "Saving.h"
#import "SavingToCaculate.h"
#import "NotificationModel.h"
#import "SavingManager.h"
#import "Budget.h"
#import "BudgetManager.h"
#import "RecurringTransaction.h"
#import "RecurringTransactionManager.h"

@implementation SQLiteManager

+ (SQLiteManager*)database{
	static SQLiteManager *_database;
	
  if(_database == nil){
	_database = [[SQLiteManager alloc] initPrivate];
  }
  return _database;
}

-(id)initPrivate{
  if((self = [super init])){
		NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDir = [documentPaths objectAtIndex:0];
		NSString *databasePath = [documentsDir stringByAppendingString:@"/moneyloverdb.sqlite"];
		NSLog(@"%@", databasePath);
		NSFileManager *fileManager = [NSFileManager defaultManager];
		if([fileManager fileExistsAtPath:databasePath] == NO){
			NSString *databasePathFromBundle = [[NSBundle mainBundle] pathForResource:@"moneyloverdb" ofType:@"sqlite"];
			[fileManager copyItemAtPath:databasePathFromBundle toPath:databasePath error:nil];
		}else{
			// Do nothing
		}
		
	if(sqlite3_open([databasePath UTF8String], &_databaseSQLite) == SQLITE_OK){
	  NSLog(@"Successful to open database!");
			sqlite3_exec(_databaseSQLite, "PRAGMA foreign_keys = on", NULL, NULL, NULL);
	}else{
	  NSLog(@"Failed to open database!");
	}
  }
  return self;
}

- (id)init{
	[self doesNotRecognizeSelector:_cmd];
	return  nil;
}

- (void)dealloc{
	NSLog(@"SQLite Manager delloc");
  sqlite3_close(_databaseSQLite);
}


#pragma mark - Methods for transaction table

-(NSMutableArray*)getTransactionsFromDate:(NSDate *)startDate andEndDate:(NSDate *)endDate{
	

	NSMutableArray *transactionArray = [[ NSMutableArray alloc] init];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
	NSString *stringStartDate = [dateFormatter stringFromDate:startDate];
	NSString *stringEndDate = [dateFormatter stringFromDate:endDate];
	
	NSString *query ;
	query = [[NSString alloc] initWithFormat:@"SELECT category_id, money, note, who, date FROM \"transaction\" WHERE date <= \"%@\" AND date >= \"%@\" ORDER BY \"date\" DESC", stringEndDate, stringStartDate];
	
//	NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
	
			NSInteger category_id = sqlite3_column_int(statement, 0);
			long long money = sqlite3_column_int64(statement, 1);
			char *note = (char*) sqlite3_column_text(statement, 2);
			char *who = (char*) sqlite3_column_text(statement, 3);
			char *date = (char*) sqlite3_column_text(statement, 4);

			
			NSString *noteString = [[NSString alloc] initWithUTF8String:note];
			NSString *whoString = [[NSString alloc] initWithUTF8String:who];
			NSString *dateString = [[NSString alloc] initWithUTF8String:date];
			
			Transaction *transaction = [[Transaction alloc] init];
			transaction.category_id = category_id;
			transaction.money = money;
			transaction.note = noteString;
			transaction.who = whoString;
			transaction.date = dateString;
			transaction.category = [self getCategoryWithId:category_id];
			
			[transactionArray addObject:transaction];
			
		}
		
		sqlite3_finalize(statement);
		
	}else{
		NSLog( @"Get transaction array failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return transactionArray;
	
}

-(NSMutableArray*)getTransactionsArrayWithDisplayMode:(NSInteger)displayMode
														startDate:(NSDate*)startDate
														andEndDate:(NSDate*)endDate{
	
	NSString *query ;
	NSMutableArray *transactionsArrayForSecsion = [[ NSMutableArray alloc] init];

	if(startDate != nil){
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
		NSString *stringStartDate = [dateFormatter stringFromDate:startDate];
		NSString *stringEndDate = [dateFormatter stringFromDate:endDate];
//      NSLog(@"SQLite: %@ \n %@", stringStartDate, stringEndDate);
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"transaction\" WHERE date <= \"%@\" AND date >= \"%@\" ORDER BY \"date\" DESC", stringEndDate, stringStartDate];
	}else{
		// Get all transaction
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"transaction\" ORDER BY \"date\" DESC"];
	}
//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			NSInteger Id = sqlite3_column_int(statement, 0);
			NSInteger category_id = sqlite3_column_int(statement, 1);
			long long money = sqlite3_column_int64(statement, 2);
			char *note = (char*) sqlite3_column_text(statement, 3);
			char *who = (char*) sqlite3_column_text(statement, 4);
			char *date = (char*) sqlite3_column_text(statement, 5);
			CGFloat interest_rate = (float)sqlite3_column_double(statement, 6);
			NSInteger compounding = sqlite3_column_int(statement, 7);
			NSInteger monthsPerCompounding = (float)sqlite3_column_int(statement, 8);
			
			NSString *noteString = [[NSString alloc] initWithUTF8String:note];
			NSString *whoString = [[NSString alloc] initWithUTF8String:who];
			NSString *dateString = [[NSString alloc] initWithUTF8String:date];

			Transaction *transaction = [[Transaction alloc] init];
			transaction.Id = Id;
			transaction.category_id = category_id;
			transaction.money = money;
			transaction.note = noteString;
			transaction.who = whoString;
			transaction.date = dateString;
			transaction.category = [self getCategoryWithId:category_id];
			transaction.interest_rate = interest_rate;
			transaction.compounding = compounding;
			transaction.monthsPerCompounding = monthsPerCompounding;
			
//          NSLog(@" %ld, %ld %lld %@  %@  %@", Id, category_id, money, noteString, whoString , dateString);
			
			if([transactionsArrayForSecsion count] == 0){
				DataSourceInASection *dataSourceInASection = [[DataSourceInASection alloc] init];
				[dataSourceInASection.transactionsArray addObject:transaction];
				dataSourceInASection.numberOfRowForSection = 1;
				dataSourceInASection.totalMoneyInSection += transaction.money;
				[transactionsArrayForSecsion addObject:dataSourceInASection];
			}else{
				NSInteger i;
				for(i = 0; i < [transactionsArrayForSecsion count]; i ++){
					DataSourceInASection *dataSourceInASection = ((DataSourceInASection*)transactionsArrayForSecsion[i]);
					BOOL tempt = NO;
					if(displayMode == 1){
						// Showing by SHOWINGBY_TRANSACTION
						tempt = [((Transaction*)dataSourceInASection.transactionsArray[0]).date isEqualToString:transaction.date];
					}else{
						// Showing by SHOWINGBY_CATEGORY
						tempt = (((Transaction*)dataSourceInASection.transactionsArray[0]).category_id == transaction.category_id);
					}
					
					if(tempt){
						dataSourceInASection.numberOfRowForSection += 1;
						dataSourceInASection.totalMoneyInSection += transaction.money;
						[dataSourceInASection.transactionsArray addObject:transaction];
						break;
					}
				}

				if( i == [transactionsArrayForSecsion count]){
					
					DataSourceInASection *dataSourceInASection = [[DataSourceInASection alloc] init];
					[dataSourceInASection.transactionsArray addObject:transaction];
					dataSourceInASection.numberOfRowForSection = 1;
					dataSourceInASection.totalMoneyInSection += transaction.money;
					
					[transactionsArrayForSecsion addObject:dataSourceInASection];
				}
			}
			
		}
		
		sqlite3_finalize(statement);
		
	}else{
		NSLog( @"Get transaction array failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	return transactionsArrayForSecsion;
}

- (NSMutableArray *)getMoneyTransactionForTrendingFeatureWithStartDate:(NSDate *)startDate
																															 endDate:(NSDate *)endDate
																											isShowingByMonth:(BOOL)isShowingByMonth
																												 andIsIncoming:(BOOL)isIncoming{
	
	NSMutableArray *result = [[ NSMutableArray alloc] init];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
	NSString *startDateString = [dateFormatter stringFromDate:startDate];
  NSString *endDateString = [dateFormatter stringFromDate:endDate];
//  NSLog(@"Startdate: %@, endDate: %@", startDateString, endDateString);
	NSString *query ;
	if(isIncoming){
			query = [[NSString alloc] initWithFormat:@"SELECT date,money FROM \"transaction\" WHERE money > 0 AND date <= \"%@\" AND date >= \"%@\"", endDateString, startDateString];
	}else{
			query = [[NSString alloc] initWithFormat:@"SELECT date,money FROM \"transaction\" WHERE money < 0 AND date <= \"%@\" AND date >= \"%@\"", endDateString, startDateString];
	}

	
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			char *date = (char*) sqlite3_column_text(statement, 0);
			long long money = sqlite3_column_int64(statement, 1);

			NSString *dateString = [[NSString alloc] initWithUTF8String:date];
			Transaction *transaction = [[Transaction alloc] init];
			transaction.money = money;
			transaction.date = dateString;
			
			if([result count] == 0){
				[result addObject:transaction];
			}else{
				NSInteger i;
				for( i = 0; i < [result count]; i++){
					Transaction *transactionFromArray = (Transaction *)result[i];
					BOOL resultCompare = NO;
					if(isShowingByMonth){
						resultCompare = [transactionFromArray.date isEqualToString:transaction.date];
					}else{
						resultCompare = [[transactionFromArray.date substringToIndex:7] isEqualToString:[transaction.date substringToIndex:7]];
					}
					
					if(resultCompare){
						transactionFromArray.money += transaction.money;
						break;
					}
				}
				
				if(i == [result count]){
					[result addObject:transaction];
				}
			}
		}
		
		sqlite3_finalize(statement);
		
	}else{
		NSLog( @"Get money for trending feature failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return result;
}

-(Transaction*) getTransactionWithId:(NSInteger) uniqueId{
	Transaction *transaction = nil;
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT id, category_id, money, note, who, date FROM \"transaction\" WHERE id = %ld",(long) uniqueId];
	sqlite3_stmt *statement;
  
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			NSInteger Id = sqlite3_column_int(statement, 0);
			NSInteger category_id = sqlite3_column_int(statement, 1);
			long long money = sqlite3_column_int64(statement, 2);
			char *note = (char*) sqlite3_column_text(statement, 3);
			char *who = (char*) sqlite3_column_text(statement, 4);
			char *date = (char*) sqlite3_column_text(statement, 5);
			CGFloat interest_rate = (float)sqlite3_column_double(statement, 6);
			NSInteger compounding = sqlite3_column_int(statement, 7);
			NSInteger monthsPerCompounding = (float)sqlite3_column_int(statement, 8);

			NSString *noteString = [[NSString alloc] initWithUTF8String:note];
			NSString *whoString = [[NSString alloc] initWithUTF8String:who];
			NSString *dateString = [[NSString alloc] initWithUTF8String:date];

			transaction = [[Transaction alloc] init];
			transaction.Id = Id;
			transaction.category_id = category_id;
			transaction.money = money;
			transaction.note = noteString;
			transaction.who = whoString;
			transaction.date = dateString;
			transaction.interest_rate = interest_rate;
			transaction.compounding = compounding;
			transaction.monthsPerCompounding = monthsPerCompounding;
				
			break;
		}
	}else{
			NSLog( @"Get transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
  sqlite3_finalize(statement);

  return transaction;
}

- (void)insertTransaction:(Transaction*)transaction{
	NSString *query = [[NSString alloc]
					 initWithFormat:@"INSERT INTO \"transaction\" (category_id, money, note, who, date) VALUES(\"%ld\",%lld,\"%@\", \"%@\", \"%@\")",
						(long)transaction.category_id, transaction.money, transaction.note, transaction.who, transaction.date];
	sqlite3_stmt *statement;
//	NSLog(@"query = %@", query);

	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {

		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Sucessful to insert transaction");
		} else {
			NSLog(@"Insert transaction failed: %s",sqlite3_errmsg(_databaseSQLite));
		}
		
	}else{
		NSLog( @"Insert transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	sqlite3_finalize(statement);


	
	[[SavingManager getInstance] caculateSaving];

	[[BudgetManager getInstance] checkTransaction:transaction];

}

- (void)updateTransaction:(Transaction*)transaction{

	NSString *query = [[NSString alloc] initWithFormat:@"UPDATE \"transaction\" SET category_id=\"%ld\", money=%lld, note=\"%@\", who=\"%@\", date=\"%@\" WHERE id=%ld",(long) transaction.category_id, transaction.money, transaction.note, transaction.who, transaction.date, (long)transaction.Id];
//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done update transaction");
		}else{
			NSLog(@"Update transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Update transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	
	[[SavingManager getInstance] caculateSaving];
	
	[[BudgetManager getInstance] checkTransaction:transaction];

}

- (void)deleteTransaction:(Transaction*)transaction {

	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"transaction\" WHERE id=%ld", (long)transaction.Id];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete transaction ");
		}else{
			NSLog(@"Delete transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
		
	}else{
		NSLog( @"Delete transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	
	[[SavingManager getInstance] caculateSaving];

//	[[BudgetManager getInstance] checkTransaction:transaction];

}

- (void)deleteAllTransaction{
	
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"transaction\" "];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete all transaction ");
		}else{
			NSLog(@"Delete transaction all failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete transaction all failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

}

- (long long)getAmountOfMoneyFromDate:(NSDate*)startDate{
	long long result = 0;
	if(startDate == nil){
		return 0;
	}
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyy-MM-dd EEEE"];
	NSString *startDateString = [dateFormatter stringFromDate:startDate];
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\" WHERE date >= \"%@\"", startDateString];
	//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			result = sqlite3_column_int64(statement, 0);
			
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return result;
}

- (long long)getAmountOfMoneyToDate:(NSDate*)endDate{
	
	long long result = 0;
	if(endDate == nil){
		return 0;
	}
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyy-MM-dd EEEE"];
	NSString *endDateString = [dateFormatter stringFromDate:endDate];
	

	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\" WHERE date < \"%@\"", endDateString];
//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
//                  NSLog(@"%@", query);
					result  = sqlite3_column_int64(statement, 0);
			
			}
		
			sqlite3_finalize(statement);
		
	}else{
		
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	
	return result;
}

- (long long)getTotalMoneyInTransactionTable{
	long long result = 0;

	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\""];
	//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			result = sqlite3_column_int64(statement, 0);
			
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get amount of money in all transactions failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return result;

}

- (long long)getTotalMoneyWithCategoryTransactionId:(NSInteger )categoryTransactionId{
	long long result = 0;
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT money, id FROM \"transaction\" WHERE \"category_id\" = %ld", (long) categoryTransactionId];
	//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			long long money = sqlite3_column_int64(statement, 0);
			NSInteger transactionId = sqlite3_column_int(statement, 1);
			
			NSString *newQuery = [[NSString alloc] initWithFormat:@"UPDATE \"transaction\" SET money=%lld WHERE id=%ld",money*-1, (long)transactionId];
			//  NSLog(@"%@", query);
			sqlite3_stmt *newStatement;
			
			if(sqlite3_prepare_v2(_databaseSQLite, [newQuery UTF8String], -1, &newStatement, NULL) == SQLITE_OK){
				if(sqlite3_step(newStatement) == SQLITE_DONE){
					NSLog(@"Done update money for transaction");
				}else{
					NSLog(@"Update money for transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
				}
				
				sqlite3_finalize(newStatement);
			}else{
				NSLog( @"Update money for transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
			}
			
			result += money;
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get amount of money with category_id failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return result;
	
}



#pragma mark - Methods for category transaction table

- (CategoryTransaction*)getCategoryWithId:(NSInteger)category_id{

	NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"categoryTransaction\" WHERE id=%ld", (long)category_id];
	CategoryTransaction *aCategoryTransaction = [[CategoryTransaction alloc] init];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			NSInteger Id = sqlite3_column_int(statement, 0);
			char *name = (char*) sqlite3_column_text(statement, 1);
			NSInteger isEarning = sqlite3_column_int(statement, 2);
			char *iconName = (char*) sqlite3_column_text(statement, 3);
			
			NSString *nameString = [[NSString alloc] initWithUTF8String:name];
			NSString *iconNameString = [[NSString alloc] initWithUTF8String:iconName];
			
//          CategoryTransaction *aCategoryTransaction = [[CategoryTransaction alloc] init];
			aCategoryTransaction.Id = Id;
			aCategoryTransaction.name = nameString;
			aCategoryTransaction.isEarning = isEarning?YES:NO;
			aCategoryTransaction.iconName = iconNameString;
			break;
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get category failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return aCategoryTransaction;
}

- (NSMutableArray*)getCategorysTransactionIsEarningType:(BOOL)isEarning;{
	
	NSMutableArray *result = [[NSMutableArray alloc] init];
	NSString *query;
	
	if(isEarning == YES){
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"categoryTransaction\" WHERE \"isEarning\"=1"];
	}else{
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"categoryTransaction\" WHERE \"isEarning\"=0"];
	}
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			NSInteger Id = sqlite3_column_int(statement, 0);
			char *name = (char*) sqlite3_column_text(statement, 1);
			NSInteger isEarning = sqlite3_column_int(statement, 2);
			char *iconName = (char*) sqlite3_column_text(statement, 3);
			
			NSString *nameString = [[NSString alloc] initWithUTF8String:name];
			NSString *iconNameString = [[NSString alloc] initWithUTF8String:iconName];

			CategoryTransaction *aCategoryTransaction = [[CategoryTransaction alloc] init];
			aCategoryTransaction.Id = Id;
			aCategoryTransaction.name = nameString;
			aCategoryTransaction.isEarning = isEarning?YES:NO;
			aCategoryTransaction.iconName = iconNameString;
			
			[result addObject:aCategoryTransaction];
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get categoryTransaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return result;
}

- (void)insertCategoryTransaction:(CategoryTransaction*)categoryTransaction{

	NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO \"categoryTransaction\" (\"name\", \"isEarning\", \"iconName\") VALUES (\"%@\", %d, \"%@\")", categoryTransaction.name, (categoryTransaction.isEarning)?1:0, categoryTransaction.iconName];
	sqlite3_stmt *statement;

	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done insert category transaction");
		}else{
			 NSLog(@"Insert category transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @" Insert category transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (BOOL)checkForExistingOfCategoryName:(NSString*)categoryName
										andTypeTransaction:(BOOL)isEarning{
	
	NSString *query;
	if(isEarning){
			query = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM \"categoryTransaction\" WHERE name=\"%@\" AND isEarning=1",categoryName];
	}else{
			query = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM \"categoryTransaction\" WHERE name=\"%@\" AND isEarning=0",categoryName];
	}

	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			if(sqlite3_column_int(statement, 0) == 0){
				return NO;
			}else{
				return YES;
			}
		}
	
	sqlite3_finalize(statement);
	}else{
		NSLog( @"checkForExistingOfCategoryName Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	return NO;
}

- (void)deleteCategoryTransactionWithId:(NSInteger)categoryId{
	
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"categoryTransaction\" WHERE id=%ld",(long)categoryId];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete category transaction ");
		}else{
			NSLog(@"Delete category transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete category transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (void)updateCategoryTransaction:(CategoryTransaction*)categoryTransaction{
	
	NSString *query = [[NSString alloc] initWithFormat:@"UPDATE \"categoryTransaction\" SET name=\"%@\", isEarning=%d, iconName=\"%@\" WHERE id=%ld", categoryTransaction.name, (categoryTransaction.isEarning)?1:0, categoryTransaction.iconName, (long)categoryTransaction.Id];
//	NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done update category transaction");
		}else{
			NSLog(@"Update category transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Update category transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (NSString *)getIconForCategoryId:(NSInteger)category_id{
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT \"iconName\" FROM \"categoryTransaction\" WHERE id=%ld", (long)category_id];
	NSString *iconNameString;
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			char *iconName =  (char*)sqlite3_column_text(statement, 0);
			iconNameString = [NSString stringWithUTF8String:iconName];
			break;
				}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get iconNAme category failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	return iconNameString;
}

- (NSString *)getNameForCategoryId:(NSInteger)category_id{
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT \"name\" FROM \"categoryTransaction\" WHERE id=%ld", (long)category_id];
	NSString *nameString;
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			char *name =  (char*)sqlite3_column_text(statement, 0);
			nameString = [NSString stringWithUTF8String:name];
			break;
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get name category failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return nameString;
}

- (BOOL)categoryWithIdIsEarning:(NSInteger)category_id{
	
	NSInteger isEarning;
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT \"isEarning\" FROM \"categoryTransaction\" WHERE id=%ld", (long)category_id];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			isEarning =  sqlite3_column_int(statement, 0);
			break;
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get category type failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	
	return isEarning?YES:NO;
}



#pragma mark - Methods for saving table

- (NSMutableArray *)getSavingsIsExpried:(BOOL )expried{
	
	NSMutableArray *result = [[NSMutableArray alloc] init];
	
	NSString *query;
	if(expried == YES){
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"saving\" WHERE \"expried\"=1 ORDER BY endDateString DESC"];
	}else{
		query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"saving\" WHERE \"expried\"=0 ORDER BY endDateString ASC"];
	}
//  NSLog(@"Query: %@", query);
	sqlite3_stmt *statement;
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		BOOL walletIsOutOfMoney = NO;
		
		if(amountOfMoneyInWallet < 0){
			walletIsOutOfMoney = YES;
			amountOfMoneyInWallet = amountOfMoneyInWallet*-1;
		}
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			NSInteger Id = sqlite3_column_int(statement, 0);
			char *name = (char *) sqlite3_column_text(statement, 1);
			char  *iconName = (char *)sqlite3_column_text(statement, 2);
			long long money = sqlite3_column_int64(statement, 3);
			double endDate = sqlite3_column_double(statement, 4);
			char *endDateChar = (char *)sqlite3_column_text(statement, 5);
			NSInteger expried = sqlite3_column_int(statement, 6);
			long long savedMoney = sqlite3_column_int64(statement, 7);
			
			Saving *saving = [[Saving alloc] init];
			saving.Id = Id;
			saving.name = [NSString stringWithUTF8String:name];
			saving.iconName = [NSString stringWithUTF8String:iconName];
			saving.money = money;
			saving.endDate = endDate;
			saving.endDateString = [NSString stringWithUTF8String:endDateChar];
			saving.expried = (expried == 1)?YES:NO;
			saving.savedMoney = savedMoney;

//			NSLog(@"%@ %lld", saving.name, savedMoney);

			if(saving.expried == NO){
				if(walletIsOutOfMoney == YES){
					// Nếu ví âm, khoản tiết kiệm đầu tiên sẽ phải tiết kiệm sao cho đủ tiền của nó + hồi lại ví
					if(amountOfMoneyInWallet > 0){
						saving.savedMoney = -amountOfMoneyInWallet;
						amountOfMoneyInWallet = 0;
						
					}else{
						saving.savedMoney = 0;
					}
				}else{
					// Nếu ví dương thì ta lần lượt gửi vào các khoản tiết kiệm gần nhất
					if(amountOfMoneyInWallet >= money){
						saving.savedMoney = money;
						amountOfMoneyInWallet -= money;
					}else{
						if(amountOfMoneyInWallet > 0){
							saving.savedMoney = amountOfMoneyInWallet;
							amountOfMoneyInWallet = 0;
						}else{
							saving.savedMoney = 0;
						}
					}
				}
			}

			[result addObject:saving];
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get savings failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	return result;
}

- (BOOL)checkForExistingSavingName:(NSString*)savingName
											andIsExpried:(BOOL)expried{
	NSString *query;
	if(expried == YES){
		query = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM \"saving\" WHERE \"expried\"=1 AND \"name\"=\"%@\"", savingName];
	}else{
		query = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM \"saving\" WHERE \"expried\"=0 AND \"name\"=\"%@\"", savingName];
	}
	
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			if(sqlite3_column_int(statement, 0) == 0){
				return NO;
			}else{
				return YES;
			}
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Checking for existing name saving Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	return NO;
}

- (void)updateSaving:(Saving *)saving{
	
	NSString *query = [[NSString alloc] initWithFormat:@"UPDATE \"saving\" SET name=\"%@\", iconName=\"%@\", money=%lld, endDate=%f, endDateString=\"%@\", expried=%d WHERE id=%ld", saving.name, saving.iconName, saving.money, saving.endDate, saving.endDateString, saving.expried?1:0, (long)saving.Id];
//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done update saving transaction");
		}else{
			NSLog(@"Update category saving failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Update category saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (void)deleteSavingWithId:(NSInteger)savingId{
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"saving\" WHERE id=%ld",(long)savingId];
	
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete saving transaction ");
		}else{
			NSLog(@"Delete saving failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (void)insertSaving:(Saving *)saving{
	NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO \"saving\" (\"name\", \"iconName\", \"money\", \"endDate\", \"endDateString\", \"expried\") VALUES (\"%@\", \"%@\", %lld, %f, \"%@\", %d)", saving.name, saving.iconName, saving.money, saving.endDate, saving.endDateString, saving.expried?1:0];
	
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done insert saving");
		}else{
			NSLog(@"Insert saving failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @" Insert saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (NSMutableArray *)getRuningSavingsToCaculateSaving{
	NSMutableArray *result = [[NSMutableArray alloc] init];
	NSString *query;
	
	query = [[NSString alloc] initWithFormat:@"SELECT \"money\", \"endDate\", \"endDateString\"  FROM \"saving\" WHERE \"expried\"=0 ORDER BY endDateString ASC"];

//  NSLog(@"Query: %@", query);
	sqlite3_stmt *statement;
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		BOOL walletIsOutOfMoney = NO;
//      NSLog(@"Money: %lld", amountOfMoneyInWallet);
		if(amountOfMoneyInWallet < 0){
			walletIsOutOfMoney = YES;
			amountOfMoneyInWallet = amountOfMoneyInWallet*-1;
		}
		
		while(sqlite3_step(statement) == SQLITE_ROW){

			long long money = sqlite3_column_int64(statement, 0);
			double endDate = sqlite3_column_double(statement, 1);
			char *endDateChar = (char *)sqlite3_column_text(statement, 2);
			
			SavingToCaculate *saving = [[SavingToCaculate alloc] init];
			if(walletIsOutOfMoney == YES){
				// Nếu ví âm, khoản tiết kiệm đầu tiên sẽ phải tiết kiệm sao cho đủ tiền của nó + hồi lại ví
				if(amountOfMoneyInWallet > 0){
					saving.savedMoneyInMonths = -amountOfMoneyInWallet;
					saving.savingMoneyInMonths = money + amountOfMoneyInWallet;
					amountOfMoneyInWallet = 0;
				}else{
					saving.savedMoneyInMonths = 0;
					saving.savingMoneyInMonths = money;
				}
			}else{
				// Nếu ví dương thì ta lần lượt gửi vào các khoản tiết kiệm gần nhất
				if(amountOfMoneyInWallet >= money){
					saving.savedMoneyInMonths = money;
					saving.savingMoneyInMonths = 0;
					amountOfMoneyInWallet -= money;
				}else{
					if(amountOfMoneyInWallet > 0){
						saving.savedMoneyInMonths = amountOfMoneyInWallet;
						saving.savingMoneyInMonths = money - amountOfMoneyInWallet;
						amountOfMoneyInWallet = 0;
					}else{
						saving.savedMoneyInMonths = 0;
						saving.savingMoneyInMonths = money;
					}
				}
			}

			saving.endDate = endDate;
			saving.endDateString = [NSString stringWithUTF8String:endDateChar];
			
			
			if(result.count == 0){
				[result addObject:saving];
				// Tính khoảng thời gian cho khoản TK đầu tiên so với hiện tại
				NSDateComponents *dateCom = [[NSDateComponents alloc] init];
				NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
				dateCom = [[NSCalendar currentCalendar] components:unit fromDate:[NSDate date]];
				[dateCom setDay:1];
				
				NSDate *firstDayOfMonth = [[NSCalendar currentCalendar] dateFromComponents:dateCom];
				NSDate *endDateInSaving = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
				
				saving.numberOfMonth = [[[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:firstDayOfMonth  toDate:endDateInSaving options:0] month];
				saving.moneyPerMonth = saving.savingMoneyInMonths/saving.numberOfMonth;
			}else{
				NSInteger i = 0;
				for (i = 0; i < [result count]; i++) {
					SavingToCaculate *savingToCompare = (SavingToCaculate *)result[i];
					if([saving.endDateString isEqualToString:savingToCompare.endDateString]){
						savingToCompare.savingMoneyInMonths += saving.savingMoneyInMonths;
						savingToCompare.savedMoneyInMonths += saving.savedMoneyInMonths;
						savingToCompare.moneyPerMonth = savingToCompare.savingMoneyInMonths/savingToCompare.numberOfMonth;
						break;
					}
				}
				if(i == result.count){
					SavingToCaculate *savingToCompare = (SavingToCaculate *)result[i-1];
					NSDate *startDateInSaving = [NSDate dateWithTimeIntervalSinceReferenceDate:savingToCompare.endDate];
					NSDate *endDateInSaving = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
					saving.numberOfMonth = [[[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:startDateInSaving toDate:endDateInSaving options:0] month];
					saving.moneyPerMonth = saving.savingMoneyInMonths/saving.numberOfMonth;
					[result addObject:saving];
				}
				
			}
			
//          NSLog(@"\nSaving money: %lld\nSaved money: %lld\nEndDate: %f\nEndDateString: %@\nNumber of month: %ld\nMoney per month: %lld",saving.savingMoneyInMonths, saving.savedMoneyInMonths, saving.endDate, saving.endDateString, (long)saving.numberOfMonth, saving.moneyPerMonth);
			
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get savings failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

	return result;
}


- (void)updateExpriedSaving{
	
	NSDate *toDay = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"YYYY - MM";
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"saving\" WHERE expried=0 AND endDateString <= \"%@\"",[dateFormatter stringFromDate:toDay]];
	//	NSLog(@"%@", query);

	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			NSInteger Id = sqlite3_column_int(statement, 0);
			char *name = (char *) sqlite3_column_text(statement, 1);
			char  *iconName = (char *)sqlite3_column_text(statement, 2);
			long long money = sqlite3_column_int64(statement, 3);
			double endDate = sqlite3_column_double(statement, 4);
			char *endDateChar = (char *)sqlite3_column_text(statement, 5);
			NSInteger expried = sqlite3_column_int(statement, 6);
			long long savedMoney = sqlite3_column_int64(statement, 7);
			
			Saving *saving = [[Saving alloc] init];
			saving.Id = Id;
			saving.name = [NSString stringWithUTF8String:name];
			saving.iconName = [NSString stringWithUTF8String:iconName];
			saving.money = money;
			saving.endDate = endDate;
			saving.endDateString = [NSString stringWithUTF8String:endDateChar];
			saving.expried = (expried == 1)?YES:NO;
			saving.savedMoney = savedMoney;

			
			
			NSString *content = [NSString stringWithFormat:@"Khoản tiết kiệm \'%@\' đã hoàn thành.", saving.name];
			
			NotificationModel *noti = [[NotificationModel alloc] initWithContent:content iconName:@"ic_bab_budget.png" andTitle:@"Hoàn thành khoản tiết kiệm"];
			
			[self insertNotification:noti];
			[self setExpriedSaving:saving];
			
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Update running saving to expried saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (void)setExpriedSaving:(Saving *)saving{

//	long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];

	long long moneyWhenSavingExpried = [[SQLiteManager database] getAmountOfMoneyToDate: [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate]];
	long long savedMoney = (moneyWhenSavingExpried >= saving.money) ? saving.money : moneyWhenSavingExpried;
	
	NSString *query = [[NSString alloc] initWithFormat:@"UPDATE \"saving\" SET expried=1, savedMoney=%lld WHERE name= \"%@\"",savedMoney, saving.name];
	sqlite3_stmt *statement;
	//	NSLog(@"%@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done set expried saving notification");
		}else{
			NSLog(@"Insert set expried saving failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Insert set expried saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}


#pragma mark - Methods for notification table

- (void)insertNotification:(NotificationModel *)noti{
	NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO \"notification\" (\"content\", \"iconName\", \"title\") VALUES (\"%@\", \"%@\", \"%@\")", noti.content, noti.iconName, noti.title];
//	NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done insert notification");
		}else{
			NSLog(@"Insert notification failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @" Insert notification failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (NSMutableArray *)getNotifications{
	NSMutableArray *result = [[NSMutableArray alloc] init];
	NSString *query;
	query = [[NSString alloc] initWithFormat:@"SELECT \"id\", \"content\", \"iconName\", \"title\" FROM \"notification\" ORDER BY id DESC"];

	sqlite3_stmt *statement;
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			NSInteger notiId = sqlite3_column_int(statement, 0);
			char *content = (char*) sqlite3_column_text(statement, 1);
			char *iconName = (char*) sqlite3_column_text(statement, 2);
			char *title = (char*) sqlite3_column_text(statement, 3);
			NSString *contentString = [[NSString alloc] initWithUTF8String:content];
			NSString *iconNameString = [[NSString alloc] initWithUTF8String:iconName];
			NSString *titleString = [[NSString alloc] initWithUTF8String:title];
			
			NotificationModel *noti = [[NotificationModel alloc] initWithContent:contentString iconName:iconNameString andTitle:titleString];
			noti.ID = notiId;
			
			[result addObject:noti];
		}
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Get categoryTransaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	return result;
}

- (void)deleteAllNotifications{
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"notification\""];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete all notification ");
		}else{
			NSLog(@"Delete all notification failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete all notification failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

- (void)deleteNotificationWithId:(NSInteger )notificationID{
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"notification\" WHERE \"id\"=%ld", (long)notificationID];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete notification with specific ID");
		}else{
			NSLog(@"Delete all notification with specific ID failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete all notification with specific ID failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

#pragma - Methods for recurring transaction table

-(void)insertRecurringTransaction:(RecurringTransaction*)transaction {
	// NSInteger lastTime = [[NSCalendar currentCalendar] dateFromComponents:
	//                       [[RecurringTransactionManager getInstance] getDateOfTransaction:transaction withFactor:-1]].timeIntervalSinceReferenceDate;
	// transaction.nextTime = [[RecurringTransactionManager getInstance] getDateOfTransaction:transaction withFactor:0];
	NSString *query = [[NSString alloc] 
					   initWithFormat:@"INSERT INTO \"recurringTransaction\" (cateId, amount, note,\
					   timeMode, step, durationMode, untilDate, numberOfEvents,\
					   modeRepeatMonth, lastRepeat, startDate) VALUES (%ld, %lld,\"%@\", %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld)",
					   (long)transaction.categoryTransaction.Id, (long long)transaction.amount, transaction.note, (long)transaction.timeMode,
					   (long)transaction.step, (long)transaction.durationMode,(long)transaction.untilDate, (long)transaction.numberOfEvents,
					   (long)transaction.modeRepeatMonth, (long)transaction.lastRepeat, (long)transaction.startDate];
										 
	sqlite3_stmt *statement;
	NSLog(@"Insert recurring transaction query = %@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Sucessful to insert transaction");
		}else{
			NSLog(@"Insert recurring transaction failed: %s",sqlite3_errmsg(_databaseSQLite));
		}

	}else{
		NSLog( @"Insert recurring transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	sqlite3_finalize(statement);    
}

-(NSMutableArray*)getScheduledTransactions {
	NSString* query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"recurringTransaction\""];
	NSMutableArray* transactions = nil;
	sqlite3_stmt *statement;
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
		transactions = [[NSMutableArray alloc] init];
		while (sqlite3_step(statement) == SQLITE_ROW) {
			RecurringTransaction *transaction = [[RecurringTransaction alloc] init];
			transaction.Id = sqlite3_column_int(statement, 0);
			transaction.amount = sqlite3_column_int64(statement, 1);
			transaction.note = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
			transaction.timeMode = sqlite3_column_int(statement, 3);
			transaction.step = sqlite3_column_int(statement, 4);
			transaction.durationMode = sqlite3_column_int(statement, 5);
			transaction.untilDate = sqlite3_column_int(statement, 6);
			transaction.numberOfEvents = sqlite3_column_int(statement, 7);
			transaction.modeRepeatMonth = sqlite3_column_int(statement, 8);
			transaction.categoryTransaction = [self getCategoryWithId:sqlite3_column_int(statement, 9)];
			transaction.lastRepeat = sqlite3_column_int(statement, 10);
			transaction.startDate = sqlite3_column_int(statement, 11);
			// transaction.nextTime = [[RecurringTransactionManager getInstance] getDateOfTransaction:transaction withFactor:1];
			
			[transactions addObject:transaction];
		}
		sqlite3_finalize(statement);
	} else {
		NSLog( @"Get recurring transaction array failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	return transactions;
}

-(void)updateRecurringTransaction:(RecurringTransaction*)transaction {
	// NSInteger lastTime = [[NSCalendar currentCalendar] dateFromComponents:
	//                       [[RecurringTransactionManager getInstance] getDateOfTransaction:transaction withFactor:-1]].timeIntervalSinceReferenceDate;
	NSString *query = [[NSString alloc]
						   initWithFormat:@"UPDATE \"recurringTransaction\" SET cateId=\"%ld\", amount=%lld, note=\"%@\", \
						   timeMode=\"%ld\", step=\"%ld\", durationMode=\"%ld\", untilDate=\"%ld\", numberOfEvents=\"%ld\", \
						   modeRepeatMonth=\"%ld\", lastRepeat=\"%ld\", startDate = %ld WHERE id=%ld",
						   (long)transaction.categoryTransaction.Id, (long long)transaction.amount, transaction.note, (long)transaction.timeMode,
						   (long)transaction.step, (long)transaction.durationMode,(long)transaction.untilDate, (long)transaction.numberOfEvents,
						   (long)transaction.modeRepeatMonth, (long)transaction.lastRepeat, (long)transaction.startDate, (long)transaction.Id];
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {
		if(sqlite3_step(statement) == SQLITE_DONE) {
			NSLog(@"Done update recurring transaction");
		} else {
			NSLog(@"Update recurring transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
	
		sqlite3_finalize(statement);
	} else {
		NSLog( @"Update recurring transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

-(void)deleteRecurringTransaction:(RecurringTransaction*)transaction {
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"recurringTransaction\" WHERE id=%ld",(long)transaction.Id];
//	NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete recurring transaction ");
		}else{
			NSLog(@"Delete recurring transaction failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete recurring transaction failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}




#pragma mark - Methods for budget table

- (long long)getAmountOfMoneyForCategory:(NSInteger)categoryID fromDate:(NSDate *)startDate toDate:(NSDate *)endDate{
	
	long long result = 0;

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyy-MM-dd EEEE"];
	NSString *startDateString = [dateFormatter stringFromDate:startDate];
	NSString *endDateString = [dateFormatter stringFromDate:endDate];
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\" WHERE category_id =%ld AND date >= \"%@\" AND date <= \"%@\" ", (long)(long)categoryID, startDateString, endDateString];
	//  NSLog(@"%@", query);
	
	sqlite3_stmt *statement;
	
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
				result = sqlite3_column_int64(statement, 0);
			
			}
		
		sqlite3_finalize(statement);
		
	}else{
		
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	return result;
}



-(void)insertBudget:(Budget*)budget {
	NSString *query = [[NSString alloc]
					   initWithFormat:@"INSERT INTO \"budget\" (startDate, endDate, maximumAmount, cateId, state) VALUES(%ld, %ld, %lld, %ld, 0)",
					   (long)budget.startDate, (long)budget.endDate, budget.maximumAmount, (long)budget.category.Id];
	sqlite3_stmt *statement;
//	NSLog(@"query = %@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
					NSLog(@"Sucessful to insert budget");
		}else{
			NSLog(@"Insert budget failed: %s",sqlite3_errmsg(_databaseSQLite));
		}
	}else{
		NSLog( @"Insert budget failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
  sqlite3_finalize(statement);  
}

-(long long)getAmountOfMoneyForBudget:(Budget*)budget {
	long long result = 0;

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyy-MM-dd EEEE"];
	// NSString *startDateString = [dateFormatter stringFromDate:startDate];
	// NSString *endDateString = [dateFormatter stringFromDate:endDate];
	
	NSString *startDateString = [dateFormatter
			stringFromDate: [NSDate dateWithTimeIntervalSinceReferenceDate:budget.startDate]];

	NSString *endDateString = [dateFormatter
			stringFromDate: [NSDate dateWithTimeIntervalSinceReferenceDate:budget.endDate]];

	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\" WHERE category_id =%ld AND date >= \"%@\" AND date <= \"%@\" ", (long)budget.category.Id, startDateString, endDateString];
	// NSLog(@"%@", query);
	
	sqlite3_stmt *statement;
	
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
				result = sqlite3_column_int64(statement, 0);

			}
		
		sqlite3_finalize(statement);
		
	} else {
		
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	return result;
}

-(NSMutableArray*)getAllBudgets {
	NSString* query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"budget\" ORDER BY startDate ASC"];
	NSMutableArray* budgets = nil;
	sqlite3_stmt *statement;
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
		budgets = [[NSMutableArray alloc] init];
		while (sqlite3_step(statement) == SQLITE_ROW) {
					
			Budget* budget = [[Budget alloc] init];
			budget.budgetId = sqlite3_column_int(statement, 0);
			budget.startDate = sqlite3_column_int(statement, 1);
			budget.endDate = sqlite3_column_int(statement, 2);
			budget.maximumAmount = sqlite3_column_int(statement, 3);
			budget.category = [self getCategoryWithId:sqlite3_column_int(statement, 4)];
			budget.state = sqlite3_column_int(statement, 5);
			budget.currentAmount = [self getAmountOfMoneyForBudget:budget];
			[budgets addObject:budget];
						
		}
		sqlite3_finalize(statement);
	} else {
		NSLog( @"Get budgets array failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	return budgets;
}

-(void)updateBudget:(Budget*)budget {
	NSString *query = [[NSString alloc]
					   initWithFormat:@"UPDATE \"budget\" SET startDate=%ld, endDate=%ld, \
					   maximumAmount = %lld, cateId = %ld, state = %ld, finalAmount = %lld WHERE budgetId=%ld",
					   (long)budget.startDate, (long)budget.endDate, budget.maximumAmount, (long)budget.category.Id, (long)budget.state, budget.currentAmount, (long)budget.budgetId];

	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {
		if(sqlite3_step(statement) == SQLITE_DONE) {
			NSLog(@"Done update budget");
		} else {
			NSLog(@"Update budget failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	} else {
		NSLog( @"Update budget failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}

-(void)deleteBudget:(Budget *)budget {

	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"budget\" WHERE budgetId=%ld",(long)budget.budgetId];

	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete budget ");
		}else{
			NSLog(@"Delete budget failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete budget failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}

}

/*
- (long long)getAmountOfMoneyForCategory:(NSInteger)categoryID fromDate:(NSDate *)startDate toDate:(NSDate *)endDate{
	
	long long result = 0;
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyy-MM-dd EEEE"];
	NSString *startDateString = [dateFormatter stringFromDate:startDate];
	NSString *endDateString = [dateFormatter stringFromDate:endDate];
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT SUM(money) FROM \"transaction\" WHERE category_id =%ld AND date >= \"%@\" AND date <= \"%@\" ", (long)(long)categoryID, startDateString, endDateString];
	//  NSLog(@"%@", query);
	
	sqlite3_stmt *statement;
	
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			result = sqlite3_column_int64(statement, 0);
			
		}
		
		sqlite3_finalize(statement);
		
	}else{
		
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	return result;
}

*/



- (void)insertTransactionDebt:(Transaction*)transaction{
	
	NSString *query = [[NSString alloc]
					   initWithFormat:@"INSERT INTO \"debt\" (money, note, who, date, interest_rate, compounding,monthsPerCompounding,isDebt) VALUES(\"%lld\",\"%@\", \"%@\", \"%@\", \"%f\",\"%ld\",\"%ld\",\"%@\")",transaction.money, transaction.note, transaction.who, transaction.date, transaction.interest_rate,(long)transaction.compounding,transaction.monthsPerCompounding,transaction.isDebt];
	
	sqlite3_stmt *statement;
//	NSLog(@"query = %@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Sucessful to insert transaction debt");
		}else{
			NSLog(@"Insert transaction debts failed: %s",sqlite3_errmsg(_databaseSQLite));
		}
		
	}else{
		NSLog( @"Insert transaction debt failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	sqlite3_finalize(statement);
	
	[[BudgetManager getInstance] checkTransaction:transaction];
}

-(NSMutableArray*) getTransactionDebt{
	
	NSString *query;
	NSMutableArray *transactionsArrayForSecsion = [[ NSMutableArray alloc] init];
	
	query = @"SELECT * FROM \"debt\" ORDER BY \"date\" DESC";
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			NSInteger Id = sqlite3_column_int(statement, 0);
			long long money = sqlite3_column_int64(statement, 1);
			char *note = (char*) sqlite3_column_text(statement, 2);
			char *who = (char*) sqlite3_column_text(statement, 3);
			char *date = (char*) sqlite3_column_text(statement, 4);
			CGFloat interest_rate = (float)sqlite3_column_double(statement, 5);
			NSInteger compounding = sqlite3_column_int(statement, 6);
			NSInteger monthsPerCompounding = (float)sqlite3_column_int(statement,7);
			char *isDebt = (char*) sqlite3_column_text(statement, 8);
			
			NSString *noteString = [[NSString alloc] initWithUTF8String:note];
			NSString *whoString = [[NSString alloc] initWithUTF8String:who];
			NSString *dateString = [[NSString alloc] initWithUTF8String:date];
			NSString *isDebtString = [[NSString alloc] initWithUTF8String:isDebt];
			
			Transaction *transaction = [[Transaction alloc] init];
			transaction.Id = Id;
			transaction.money = money;
			transaction.note = noteString;
			transaction.who = whoString;
			transaction.date = dateString;
			transaction.interest_rate = interest_rate;
			transaction.compounding = compounding;
			transaction.monthsPerCompounding = monthsPerCompounding;
			transaction.isDebt = isDebtString;
//			NSLog(transaction.note);
//			NSLog(transaction.date);
			[transactionsArrayForSecsion addObject:transaction];
			
			//
		}
		
		sqlite3_finalize(statement);
		
	}else{
		NSLog( @"Get transaction array failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
	//
	return transactionsArrayForSecsion;
}

- (void)deleteDebt:(Transaction *)debt{
	
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"debt\" WHERE id=%ld",(long)debt.Id];
	
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			NSLog(@"Done delete debt ");
		}else{
			NSLog(@"Delete debt failed: %s", sqlite3_errmsg(_databaseSQLite));
		}
		
		sqlite3_finalize(statement);
	}else{
		NSLog( @"Delete debt failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
	}
}


#pragma mark - Methods for monthlySaving

- (NSMutableArray* )getPlaningMonthlySaving{
	
	NSMutableArray *result = [[ NSMutableArray alloc] init];
	
	NSString *query;
	query = @"SELECT month, amount FROM \"monthlySaving\" ORDER BY \"id\" DESC";
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			
			NSMutableDictionary *aDictionary = [[NSMutableDictionary alloc] init];
			[aDictionary setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)] forKey:@"Month"];
			[aDictionary setValue:[NSNumber numberWithLongLong:sqlite3_column_int64(statement, 1)] forKey:@"Amount"];
			
			[result addObject:aDictionary];
		}
	}

	return result;
}


- (void)deletePlaningMonthlySaving{
	
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE FROM \"monthlySaving\""];
	sqlite3_stmt *statement;
	//	NSLog(@"query = %@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			
			NSLog(@"Sucessful to delete planing saving");
			
		}else{
			
			NSLog(@"Delete planing saving failed: %s",sqlite3_errmsg(_databaseSQLite));
			
		}
	}else{
		
		NSLog( @"Delete planing saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	sqlite3_finalize(statement);
}

- (void)insertPlaningSavingWithMonth:(NSString *)month andAmount:(long long)amount{
	
	NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO \"monthlySaving\" (month, amount) VALUES(\"%@\", %lld)", month, amount];
	sqlite3_stmt *statement;
//		NSLog(@"query = %@", query);
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		if(sqlite3_step(statement) == SQLITE_DONE){
			
			NSLog(@"Sucessful to insert planing saving");
			
		}else{
			
			NSLog(@"Insert planing saving failed: %s",sqlite3_errmsg(_databaseSQLite));
			
		}
	}else{
		
		NSLog( @"Insert planing saving failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	sqlite3_finalize(statement);
	
}

- (long long)getPreviousMonthAmountSaving{
	
	long long result = 0;
	
	NSString *query = [[NSString alloc] initWithFormat:@"SELECT amount FROM \"monthlySaving\"   ORDER BY monthlySaving_id ASC LIMIT 1"];
	//  NSLog(@"%@", query);
	sqlite3_stmt *statement;
	
	if(sqlite3_prepare_v2(_databaseSQLite, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
		
		while(sqlite3_step(statement) == SQLITE_ROW){
			//                  NSLog(@"%@", query);
			
			result = sqlite3_column_int64(statement, 0);
		}
		
		sqlite3_finalize(statement);
		
	}else{
		
		NSLog( @"Get amount of money failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(_databaseSQLite));
		
	}
	
	return result;
}

@end
