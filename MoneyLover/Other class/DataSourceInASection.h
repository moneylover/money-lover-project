//
//  DataSourceInASection.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/29/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "CategoryTransaction.h"

@class  Transaction;


@interface DataSourceInASection : NSObject


@property (strong, nonatomic) NSMutableArray *transactionsArray;
@property (assign, nonatomic) BOOL isShowingCellContent;
@property (assign, nonatomic) NSInteger numberOfRowForSection;
@property (assign, nonatomic) long long totalMoneyInSection;

@end

