//
//  MoneyStringFormatter.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "MoneyStringFormatter.h"

@implementation MoneyStringFormatter

+ (NSString *)stringMoneyWithLongLong:(long long)moneyValue{
	
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
	//		numberFormatter.locale = [NSLocale currentLocale];
	numberFormatter.usesGroupingSeparator = YES;
	
	return [NSString stringWithFormat:@"%@ đ", [numberFormatter stringFromNumber:[NSNumber numberWithLongLong:moneyValue]]];
}

@end
