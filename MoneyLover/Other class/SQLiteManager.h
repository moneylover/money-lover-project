//
//  SQLiteManager.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 7/16/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@class CategoryTransaction;
@class SQLiteManager;
@class Transaction;
@class Saving;
@class NotificationModel;
@class RecurringTransaction;
@class Budget;

@interface SQLiteManager : NSObject{
	/// Đối tượng sqlite3, giao tiếp với SQLite file.
  sqlite3 *_databaseSQLite;
}

+ (SQLiteManager*)database;


#pragma - Methods for transaction table
-(NSMutableArray*)getTransactionsFromDate:(NSDate *)startDate andEndDate:(NSDate *)enddate;
- (Transaction*)getTransactionWithId:(NSInteger) uniqueId;
- (void)updateTransaction:(Transaction*)transaction;
- (void)insertTransaction:(Transaction*)transaction;
-(NSMutableArray*) getTransactionsArrayWithDisplayMode:(NSInteger)displayMode
																						 startDate:(NSDate*)startDate
																						andEndDate:(NSDate*)endDate;
- (NSMutableArray *)getMoneyTransactionForTrendingFeatureWithStartDate:(NSDate *)startDate
																															 endDate:(NSDate *)endDate
																											isShowingByMonth:(BOOL)isShowingByMonth
																												 andIsIncoming:(BOOL)isIncoming;
- (void)deleteTransaction:(Transaction*)transaction;
- (void)deleteAllTransaction;
- (long long)getAmountOfMoneyToDate:(NSDate*)endDate;
- (long long)getAmountOfMoneyFromDate:(NSDate*)endDate;
- (long long)getTotalMoneyInTransactionTable;
- (long long)getTotalMoneyWithCategoryTransactionId:(NSInteger )categoryTransactionId;

#pragma - Methods for category transaction table
- (CategoryTransaction*)getCategoryWithId:(NSInteger)category_id;
- (NSMutableArray*)getCategorysTransactionIsEarningType:(BOOL)isEarning;
- (void)insertCategoryTransaction:(CategoryTransaction*)categorytransaction;
- (void)updateCategoryTransaction:(CategoryTransaction*)categoryTransaction;
- (BOOL)checkForExistingOfCategoryName:(NSString*)categoryName andTypeTransaction:(BOOL)isEarning;
- (void)deleteCategoryTransactionWithId:(NSInteger)categoryId;
- (NSString*)getIconForCategoryId:(NSInteger)category_id;
- (NSString*)getNameForCategoryId:(NSInteger)category_id;
- (BOOL)categoryWithIdIsEarning:(NSInteger)category_id;


#pragma - Methods for saving table
- (NSMutableArray *)getSavingsIsExpried:(BOOL )expried;
- (BOOL)checkForExistingSavingName:(NSString*)savingName andIsExpried:(BOOL)expried;
- (void)insertSaving:(Saving *)saving;
- (void)updateSaving:(Saving *)saving;
- (void)deleteSavingWithId:(NSInteger)savingId;
- (NSMutableArray *)getRuningSavingsToCaculateSaving;
- (void)updateExpriedSaving;


#pragma - Methods for notification table
- (void)insertNotification:(NotificationModel *)noti;
- (NSMutableArray *)getNotifications;
- (void)deleteAllNotifications;
- (void)deleteNotificationWithId:(NSInteger )notificationID;

#pragma - Methods for recurring transaction table
-(void)insertRecurringTransaction:(RecurringTransaction*)transaction;
-(NSMutableArray*)getScheduledTransactions;
-(void)updateRecurringTransaction:(RecurringTransaction*)transaction;
-(void)deleteRecurringTransaction:(RecurringTransaction*)transaction;


#pragma - Methods for budget
-(void)insertBudget:(Budget*)budget;
-(long long)getAmountOfMoneyForBudget:(Budget*)budget;
-(NSMutableArray*)getAllBudgets;
-(void)updateBudget:(Budget*)budget;
-(void)deleteBudget:(Budget*)budget;
- (long long)getAmountOfMoneyForCategory:(NSInteger)categoryID fromDate:(NSDate *)startDate toDate:(NSDate *)endDate;


#pragma - Methods for debt
- (void)insertTransactionDebt:(Transaction*)transaction;
- (NSMutableArray*) getTransactionDebt;
- (void)deleteDebt:(Transaction *)debt;


#pragma mark - Methods for monthlySaving
- (NSMutableArray* )getPlaningMonthlySaving;
- (void)deletePlaningMonthlySaving;
- (void)insertPlaningSavingWithMonth:(NSString *)month andAmount:(long long)amount;
- (long long)getPreviousMonthAmountSaving;

@end


