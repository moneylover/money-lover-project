//
//  DataSourceInASection.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/29/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "DataSourceInASection.h"

@implementation DataSourceInASection : NSObject

- (id)init{
	if(self = [super init]){
		self.transactionsArray = [[NSMutableArray alloc] init];
		_isShowingCellContent	 = YES;

		_totalMoneyInSection = 0.0f;
		return self;
	}
	return nil;
}
@end
