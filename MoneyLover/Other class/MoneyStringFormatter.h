//
//  MoneyStringFormatter.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoneyStringFormatter : NSObject

+ (NSString *)stringMoneyWithLongLong:(long long)moneyValue;

@end
