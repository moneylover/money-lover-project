//
//  DeviceName.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/3/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DeviceName : NSObject


+ (NSString*)getDeviceName;

@end
