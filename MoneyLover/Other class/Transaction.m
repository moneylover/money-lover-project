//
//  Transaction.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/16/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "Transaction.h"

@implementation Transaction

- (void)print{
	
	NSLog(@"Transaction infomation:");
	NSLog(@"id: %ld", (long)self.Id);
	NSLog(@"Category Id: %ld", (long)self.category_id);
	NSLog(@"Money: %lld", self.money);
	NSLog(@"Note: %@", self.note);
	NSLog(@"Who: %@", self.who);
	NSLog(@"Date: %@", self.date);
}

- (Transaction *)makeACopy{
	Transaction *result = [[Transaction alloc] init];
	result.Id = self.Id;
	result.category_id = self.category_id;
	result.note = self.note;
	result.who = self.who;
	result.date = self.date;
	result.category = self.category;
	result.money = self.money;
	return result;
}

@end


