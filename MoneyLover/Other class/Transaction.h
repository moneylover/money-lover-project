//
//  Transaction.h
//  SlideMenuMoneyLover
//
//  Created by Ngo Huu Tuan on 9/16/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#ifndef SlideMenuMoneyLover_Transaction_h
#define SlideMenuMoneyLover_Transaction_h

#import <QuartzCore/QuartzCore.h>
#import "CategoryTransaction.h"

@interface Transaction : NSObject

@property (assign, nonatomic) NSInteger Id;
@property (assign, nonatomic) NSInteger category_id;
@property (assign, nonatomic) long long money;
@property (copy, nonatomic) NSString *note;
@property (copy, nonatomic) NSString *who;
@property (copy, nonatomic) NSString *date;
@property (strong, nonatomic) CategoryTransaction *category;

@property (assign, nonatomic) CGFloat interest_rate;
@property (assign, nonatomic) NSInteger compounding;
@property (assign, nonatomic) NSInteger monthsPerCompounding;

@property (assign, nonatomic) NSString *isDebt;

- (Transaction *)makeACopy;

@end

#endif
