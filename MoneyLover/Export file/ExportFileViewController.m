//
//  ExportFileViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/18/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ExportFileViewController.h"
#include "LibXL/libxl.h"
#import "SQLiteManager.h"
#import "Transaction.h"
#import <MessageUI/MessageUI.h>

@interface ExportFileViewController () <MFMailComposeViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) UIView *selectTimeView;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *monthsArray;
@property (strong, nonatomic) NSMutableArray *yearsArray;
@property (assign, nonatomic) BOOL isShowingByMonth;

@property (assign, nonatomic) NSInteger selectedMonth;
@property (assign, nonatomic) NSInteger selectedYear;


@property (weak, nonatomic) IBOutlet UIButton *chooseDurationButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) NSMutableArray *transactionArray;
@end

@implementation ExportFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	self.monthsArray = [NSMutableArray arrayWithArray:@[@"Tháng 1", @"Tháng 2", @"Tháng 3", @"Tháng 4", @"Tháng 5", @"Tháng 6", @"Tháng 7", @"Tháng 8", @"Tháng 9", @"Tháng 10", @"Tháng 11", @"Tháng 12"]];
	
	self.yearsArray = [[ NSMutableArray alloc ]init];
	
	for(int i = 1970; i < 2071; i++){
		
		[self.yearsArray addObject:[NSString stringWithFormat:@"Năm %ld", (long)i]];
		
	}
	self.isShowingByMonth = YES;
	[self.segmentedControl addTarget:self action:@selector(segmentTimeChanged:) forControlEvents:UIControlEventValueChanged];
	self.segmentedControl.selectedSegmentIndex = 0;
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	
	self.selectTimeView = [[UIView alloc] initWithFrame:self.view.frame];
	self.selectTimeView.backgroundColor = [UIColor clearColor];
	UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
	backgroundView.backgroundColor = [UIColor grayColor];
	backgroundView.alpha = 0.8;
	self.selectTimeView.hidden = YES;
	[self.selectTimeView addSubview:backgroundView];
	UITapGestureRecognizer*tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePickerView:)];
	[self.selectTimeView addGestureRecognizer:tapGes];
	
	self.pickerView = [[UIPickerView alloc] init];
	self.pickerView.delegate = self;
	self.pickerView.dataSource = self;
	self.pickerView.center = self.view.center;
	self.pickerView.backgroundColor = [UIColor whiteColor];
	self.pickerView.delegate = self;
	[self.selectTimeView addSubview:self.pickerView];
	
	[self.view addSubview:self.selectTimeView];
	
    NSDateComponents* today = [[NSCalendar currentCalendar]
                                    components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                    fromDate:[NSDate date]];
    
	self.selectedMonth = today.month;
	self.selectedYear = today.year;
	[self setTimeLabel];
	
	self.transactionArray = [[NSMutableArray alloc] init];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

	
	
}

#pragma mark - UISegmentedControl selector

- (void)segmentTimeChanged:(UISegmentedControl *)paramSender{
	
	self.isShowingByMonth = !_isShowingByMonth;
	
	[self.pickerView reloadAllComponents];
	[self setTimeLabel];
}

- (void)exportToExcelFile {
	if(_isShowingByMonth){
        [self monthExportToExcelFileWithName:
         [NSString stringWithFormat:@"Bảng thống kê thu chi tháng %d năm %d.xls", self.selectedMonth, self.selectedYear]];
		
	}else{
        [self yearExportToExcelFileWithName:
         [NSString stringWithFormat:@"Bảng thống kê thu chi năm %d.xls", self.selectedYear]];
	}
}


-(void)monthExportToExcelFileWithName:(NSString*)fileName {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    NSDate* startDate;
    NSDate* endDate;
    [dateComponents setCalendar:calendar];
    
    BookHandle book = xlCreateXMLBook();
    SheetHandle sheet;
    
    dateComponents.year = self.selectedYear;
    
    dateComponents.day = 1;
    dateComponents.month = self.selectedMonth;
    
    NSString *title = [NSString stringWithFormat:@"Tháng %d năm %d", self.selectedMonth, self.selectedYear];
    
    startDate = [dateComponents date];
    
    dateComponents.month = self.selectedMonth + 1;
    if (dateComponents.month == 13) {
        dateComponents.month = 1;
        dateComponents.year += 1;
    }
    endDate = [[dateComponents date] dateByAddingTimeInterval:-86400];
    
    sheet = xlBookAddSheet(book, [title UTF8String] , NULL);
    
    self.transactionArray = [[SQLiteManager database] getTransactionsFromDate:startDate andEndDate:endDate];
    
    long long startDateAmount = [[SQLiteManager database]getAmountOfMoneyToDate:startDate];
    long long endDateAmount = [[SQLiteManager database]getAmountOfMoneyToDate:endDate];
    
    FormatHandle headerFormat = xlBookAddFormat(book, 0);
    xlFormatSetAlignH(headerFormat, ALIGNH_CENTER);
    xlFormatSetBorder(headerFormat, BORDERSTYLE_THIN);
    xlFormatSetFillPattern(headerFormat, FILLPATTERN_SOLID);
    xlFormatSetPatternForegroundColor(headerFormat, COLOR_TAN);
    
    FontHandle font = xlBookAddFont(book, 0);
    xlFontSetColor(font, COLOR_BLACK);
    xlFontSetBold(font, true);
    xlFormatSetFont(headerFormat, font);
    
    FormatHandle titleFormat = xlBookAddFormat(book, 0);
    
    FontHandle fontBig = xlBookAddFont(book, 0);
    xlFontSetColor(fontBig, COLOR_BLACK);
    xlFontSetBold(fontBig, true);
    xlFontSetSize(fontBig, 20);
    xlFormatSetFont(titleFormat, fontBig);
    
    FormatHandle descriptionFormat = xlBookAddFormat(book, 0);
    xlFormatSetBorderLeft(descriptionFormat, BORDERSTYLE_THIN);
    xlFormatSetBorderRight(descriptionFormat, BORDERSTYLE_THIN);
    
    FontHandle fontDescription = xlBookAddFont(book, 0);
    xlFormatSetFont(descriptionFormat, fontDescription);
    
    FormatHandle tableBottomFormat = xlBookAddFormat(book, 0);
    xlFormatSetBorderTop(tableBottomFormat, BORDERSTYLE_THIN);
    
    xlSheetWriteStr(sheet, 1, 0, "TỔNG QUAN", titleFormat);
    xlSheetWriteStr(sheet, 2, 0, "Số dư đầu tháng: ", 0);
    xlSheetWriteNum(sheet, 2, 2, startDateAmount, 0);
    xlSheetWriteStr(sheet, 3, 0, "Chi tiêu: ", 0);
    xlSheetWriteNum(sheet, 3, 2, endDateAmount - startDateAmount, 0);
    xlSheetWriteStr(sheet, 4, 0, "Số dư cuối tháng: ", 0);
    xlSheetWriteNum(sheet, 4, 2, endDateAmount, 0);
    
    xlSheetWriteStr(sheet, 6, 0, "CHI TIẾT", titleFormat);
    xlSheetWriteStr(sheet, 7, 0, "STT", headerFormat);
    xlSheetWriteStr(sheet, 7, 1, "Nội dung giao dịch", headerFormat);
    xlSheetWriteStr(sheet, 7, 2, "Số tiền", headerFormat);
    xlSheetWriteStr(sheet, 7, 3, "Ghi chú", headerFormat);
    xlSheetWriteStr(sheet, 7, 4, "Với ai", headerFormat);
    xlSheetWriteStr(sheet, 7, 5, "Ngày thực hiện", headerFormat);
    
    for (int j = 0; j < self.transactionArray.count; j++) {
        Transaction *newTransaction = (Transaction *)self.transactionArray[j];
        
        xlSheetWriteNum(sheet, j + 8, 0, j + 1, descriptionFormat);
        xlSheetWriteStr(sheet, j + 8, 1, [newTransaction.category.name UTF8String], descriptionFormat);
        xlSheetWriteNum(sheet, j + 8, 2, newTransaction.money, descriptionFormat);
        xlSheetWriteStr(sheet, j + 8, 3, [newTransaction.note UTF8String], descriptionFormat);
        xlSheetWriteStr(sheet, j + 8, 4, ([newTransaction.who isEqualToString:@"(null)"]) ? "" : [newTransaction.who UTF8String], descriptionFormat);
        xlSheetWriteStr(sheet, j + 8, 5, [newTransaction.date UTF8String], descriptionFormat);
    }
    
    int tmp = self.transactionArray.count;
    xlSheetWriteStr(sheet, tmp + 8, 0, "", tableBottomFormat);
    xlSheetWriteStr(sheet, tmp + 8, 1, "", tableBottomFormat);
    xlSheetWriteStr(sheet, tmp + 8, 2, "", tableBottomFormat);
    xlSheetWriteStr(sheet, tmp + 8, 3, "", tableBottomFormat);
    xlSheetWriteStr(sheet, tmp + 8, 4, "", tableBottomFormat);
    xlSheetWriteStr(sheet, tmp + 8, 5, "", tableBottomFormat);
    
    NSString *documentPath =
    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    NSLog(@"%@", filePath);
    xlBookSave(book, [filePath UTF8String]);
    xlBookRelease(book);
    
    NSData* database = [NSData dataWithContentsOfFile: filePath];
    
    if (database != nil) {
        MFMailComposeViewController* picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        [picker setSubject:[NSString stringWithFormat: @"%@ %@", [[UIDevice currentDevice] model], [filePath lastPathComponent]]];
        [picker setSubject:@"Money Lover exports file"];
        
        NSString* filename = [filePath lastPathComponent];
        
        [picker addAttachmentData: database mimeType:@"application/octet-stream" fileName: filename];
        NSString* emailBody = @"Attached is the SQLite data from my iOS device.";
        [picker setMessageBody:emailBody isHTML:YES];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        UIAlertView* warning = [[UIAlertView alloc] initWithTitle: @"Error"
                                                          message: @"Unable to send attachment!"
                                                         delegate: self
                                                cancelButtonTitle: @"Ok"
                                                otherButtonTitles: nil];
        [warning show];
        
    }
    
}

-(void)yearExportToExcelFileWithName:(NSString*)fileName {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    NSDate* startDate;
    NSDate* endDate;
    [dateComponents setCalendar:calendar];
    
    BookHandle book = xlCreateXMLBook();
    SheetHandle sheet[12];
    
    dateComponents.year = self.selectedYear;
    
    
    for (int i = 0; i < 12; i++) {
        dateComponents.day = 1;
        dateComponents.month = i + 1;
        startDate = [dateComponents date];
        
        dateComponents.month = i + 2;
        if (dateComponents.month == 13) {
            dateComponents.month = 1;
            dateComponents.year += 1;
        }
        endDate = [[dateComponents date] dateByAddingTimeInterval:-86400];
        
        NSString *title = [NSString stringWithFormat:@"Tháng %d", i + 1];
        sheet[i] = xlBookAddSheet(book, [title UTF8String] , NULL);
        
        self.transactionArray = [[SQLiteManager database] getTransactionsFromDate:startDate andEndDate:endDate];
        
        long long startDateAmount = [[SQLiteManager database]getAmountOfMoneyToDate:startDate];
        long long endDateAmount = [[SQLiteManager database]getAmountOfMoneyToDate:endDate];
        
        FormatHandle headerFormat = xlBookAddFormat(book, 0);
        xlFormatSetAlignH(headerFormat, ALIGNH_CENTER);
        xlFormatSetBorder(headerFormat, BORDERSTYLE_THIN);
        xlFormatSetFillPattern(headerFormat, FILLPATTERN_SOLID);
        xlFormatSetPatternForegroundColor(headerFormat, COLOR_TAN);

        FontHandle font = xlBookAddFont(book, 0);
        xlFontSetColor(font, COLOR_BLACK);
        xlFontSetBold(font, true);
        xlFormatSetFont(headerFormat, font);
        
        FormatHandle titleFormat = xlBookAddFormat(book, 0);
        
        FontHandle fontBig = xlBookAddFont(book, 0);
        xlFontSetColor(fontBig, COLOR_BLACK);
        xlFontSetBold(fontBig, true);
        xlFontSetSize(fontBig, 20);
        xlFormatSetFont(titleFormat, fontBig);
        
        FormatHandle descriptionFormat = xlBookAddFormat(book, 0);
        xlFormatSetBorderLeft(descriptionFormat, BORDERSTYLE_THIN);
        xlFormatSetBorderRight(descriptionFormat, BORDERSTYLE_THIN);

        FontHandle fontDescription = xlBookAddFont(book, 0);
        xlFormatSetFont(descriptionFormat, fontDescription);

        FormatHandle tableBottomFormat = xlBookAddFormat(book, 0);
        xlFormatSetBorderTop(tableBottomFormat, BORDERSTYLE_THIN);
        
        xlSheetWriteStr(sheet[i], 1, 0, "TỔNG QUAN", titleFormat);
        xlSheetWriteStr(sheet[i], 2, 0, "Số dư đầu tháng: ", 0);
        xlSheetWriteNum(sheet[i], 2, 2, startDateAmount, 0);
        xlSheetWriteStr(sheet[i], 3, 0, "Chi tiêu: ", 0);
        xlSheetWriteNum(sheet[i], 3, 2, endDateAmount - startDateAmount, 0);
        xlSheetWriteStr(sheet[i], 4, 0, "Số dư cuối tháng: ", 0);
        xlSheetWriteNum(sheet[i], 4, 2, endDateAmount, 0);

        xlSheetWriteStr(sheet[i], 6, 0, "CHI TIẾT", titleFormat);
        xlSheetWriteStr(sheet[i], 7, 0, "STT", headerFormat);
        xlSheetWriteStr(sheet[i], 7, 1, "Nội dung giao dịch", headerFormat);
        xlSheetWriteStr(sheet[i], 7, 2, "Số tiền", headerFormat);
        xlSheetWriteStr(sheet[i], 7, 3, "Ghi chú", headerFormat);
        xlSheetWriteStr(sheet[i], 7, 4, "Với ai", headerFormat);
        xlSheetWriteStr(sheet[i], 7, 5, "Ngày thực hiện", headerFormat);
        
        for (int j = 0; j < self.transactionArray.count; j++) {
            Transaction *newTransaction = (Transaction *)self.transactionArray[j];
            
            xlSheetWriteNum(sheet[i], j + 8, 0, j + 1, descriptionFormat);
            xlSheetWriteStr(sheet[i], j + 8, 1, [newTransaction.category.name UTF8String], descriptionFormat);
            xlSheetWriteNum(sheet[i], j + 8, 2, newTransaction.money, descriptionFormat);
            xlSheetWriteStr(sheet[i], j + 8, 3, [newTransaction.note UTF8String], descriptionFormat);
            xlSheetWriteStr(sheet[i], j + 8, 4, ([newTransaction.who isEqualToString:@"(null)"]) ? "" : [newTransaction.who UTF8String], descriptionFormat);
            xlSheetWriteStr(sheet[i], j + 8, 5, [newTransaction.date UTF8String], descriptionFormat);
        }
        
        int tmp = self.transactionArray.count;
        xlSheetWriteStr(sheet[i], tmp + 8, 0, "", tableBottomFormat);
        xlSheetWriteStr(sheet[i], tmp + 8, 1, "", tableBottomFormat);
        xlSheetWriteStr(sheet[i], tmp + 8, 2, "", tableBottomFormat);
        xlSheetWriteStr(sheet[i], tmp + 8, 3, "", tableBottomFormat);
        xlSheetWriteStr(sheet[i], tmp + 8, 4, "", tableBottomFormat);
        xlSheetWriteStr(sheet[i], tmp + 8, 5, "", tableBottomFormat);
    }
    
    NSString *documentPath =
    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    NSLog(@"%@", filePath);
    xlBookSave(book, [filePath UTF8String]);
    xlBookRelease(book);
    
    NSData* database = [NSData dataWithContentsOfFile: filePath];
    
    if (database != nil) {
        MFMailComposeViewController* picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        [picker setSubject:[NSString stringWithFormat: @"%@ %@", [[UIDevice currentDevice] model], [filePath lastPathComponent]]];
        [picker setSubject:@"Money Lover exports file"];
        
        NSString* filename = [filePath lastPathComponent];
        
        [picker addAttachmentData: database mimeType:@"application/octet-stream" fileName: filename];
        NSString* emailBody = @"Attached is the SQLite data from my iOS device.";
        [picker setMessageBody:emailBody isHTML:YES];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        UIAlertView* warning = [[UIAlertView alloc] initWithTitle: @"Error"
                                                          message: @"Unable to send attachment!"
                                                         delegate: self
                                                cancelButtonTitle: @"Ok"
                                                otherButtonTitles: nil];
        [warning show];
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
//	NSLog(@"[self dismissModalViewControllerAnimated:YES];");
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIAction

- (IBAction)chooseDuration:(id)sender {
	
	if(_isShowingByMonth){
		[self.pickerView selectRow:45 inComponent:1 animated:YES];
		[self.pickerView selectRow:0 inComponent:0 animated:YES];
	}else{
		[self.pickerView selectRow:45 inComponent:0 animated:YES];
	}
    
    NSDateComponents* today = [[NSCalendar currentCalendar]
                               components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                               fromDate:[NSDate date]];
    
	self.selectedMonth = today.month;
	self.selectedYear = today.year;
	
	self.selectTimeView.alpha = 0;
	self.selectTimeView.hidden = NO;
	
	[UIView animateWithDuration:0.5 animations:^{
		self.selectTimeView.alpha = 1;
	} completion:^(BOOL finished) {
		
	}];
	
}


- (IBAction)showSlideMenu:(id)sender {
	[self.delegate showSlideMenu];
}

- (IBAction)exportXLSFile:(id)sender {
	
	[self exportToExcelFile];
}

- (IBAction)exportXLSXFile:(id)sender {
	
	[self exportToExcelFile];
}


#pragma - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	
	if(_isShowingByMonth){
		return  2;
	}
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	if(_isShowingByMonth){
		switch (component) {
			case 0:{
				return [self.monthsArray count];
			}
				break;
			case 1:{
				return [self.yearsArray count];
			}
				break;
			default:
				break;
		}
		
	}else{
		switch (component) {
			case 0:{
				return [self.yearsArray count];
			}
				break;
			default:
				break;
		}
	}
	
	
	return 1;
}

#pragma - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
			forComponent:(NSInteger)component{
	if(_isShowingByMonth){
		
		switch (component) {
				// Theo tháng
			case 0:{
				return ((NSString*)self.monthsArray[row]);
			}
				break;
			case 1:{
				return ((NSString*)self.yearsArray[row]);
			}
				break;
			default:
				break;
		}
	}else{
		// Theo năm
		switch (component) {
			case 0:{
				return ((NSString*)self.yearsArray[row]);
			}
				break;
			default:
				break;
		}
		
	}
	return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	//	NSLog(@"row: %d component: %d", row, component);

	if(_isShowingByMonth){
		if(component == 0){
			self.selectedMonth = row + 1;
		}else{
			self.selectedYear = 1970 + row;
		}
	}else{
		self.selectedYear = 1970 + row;
	}
	
	//	NSLog(@"Month: %d Year: %d", self.selectedMonth, self.selectedYear);
}


#pragma - Setting selector for tap gesture recognizer
- (void)hidePickerView:(UITapGestureRecognizer *)pramaSender{
	[UIView animateWithDuration:0.5 animations:^{
		self.selectTimeView.alpha = 0;
	} completion:^(BOOL finished) {
		self.selectTimeView.hidden = YES;
	}];
	[self setTimeLabel];

}


- (void)setTimeLabel{
	if(_isShowingByMonth){
		[self.chooseDurationButton setTitle:[NSString stringWithFormat:@"Tháng %ld năm %ld", (long)self.selectedMonth, (long)self.selectedYear] forState:UIControlStateNormal];
	}else{
		[self.chooseDurationButton setTitle:[NSString stringWithFormat:@"Năm %ld", (long)self.selectedYear] forState:UIControlStateNormal];
	}
}

@end











