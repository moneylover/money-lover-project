//
//  SlideMenuViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 8/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BouncingViewController.h"

@protocol SlideMenuViewControllerDelegate <NSObject>

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
@end

@interface SlideMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UIDynamicAnimatorDelegate>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *reviewWalletsView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *upArrow;


@property (nonatomic, weak) id<SlideMenuViewControllerDelegate> delegate;
@property (nonatomic, strong) BouncingViewController *bouncingViewController;
@property BOOL showingBouncingView;
@property BOOL finishedBouncingAnimation;

- (void)setFrameForSubView;

@end
