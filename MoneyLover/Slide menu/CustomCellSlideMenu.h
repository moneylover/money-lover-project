//
//  CustomCellSlideMenu.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/6/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellSlideMenu : UITableViewCell

@property (nonatomic, strong) UIImage *selectedImage;
@end
