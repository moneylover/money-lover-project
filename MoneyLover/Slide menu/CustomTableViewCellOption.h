//
//  CustomTableViewCellOption.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/9/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomTableViewCellOptionDelegete <NSObject, UIGestureRecognizerDelegate>
- (void)moveSelectedCellToOriginalPosition;
@end

@interface CustomTableViewCellOption : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *walletName;

@property (weak, nonatomic) IBOutlet UILabel *amountOfMoney;

@property (weak, nonatomic) IBOutlet UIView *slideView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;

@property (strong, nonatomic) id <CustomTableViewCellOptionDelegete> delegate;

- (void)hideOption;
- (void)showOption;
@end
