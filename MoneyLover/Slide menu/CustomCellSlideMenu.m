//
//  CustomCellSlideMenu.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/6/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//



#import "CustomCellSlideMenu.h"

CGFloat const MY_ICON_SIZE = 32;

@implementation CustomCellSlideMenu

- (id)init{
  if(self = [super init]){
    
    
  }
  return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)layoutSubviews{
  [super layoutSubviews];
  
  self.imageView.frame = CGRectMake(15.0f, 5.0f, 32, 32);
  self.textLabel.frame = CGRectMake(60.0f, self.textLabel.frame.origin.y, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
  self.imageView.contentMode = UIViewContentModeScaleAspectFit;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
  // Configure the view for the selected state
  if(selected){
    self.textLabel.textColor = [UIColor whiteColor];
  }else{
//    self.textLabel.textColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0f];
    self.textLabel.textColor = [UIColor grayColor];
  }

}

@end
