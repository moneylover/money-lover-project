//
//  BoucingViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/7/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BouncingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *addWalletView;

@property (assign, nonatomic) NSInteger numberOfWallets;
@property (assign, nonatomic) NSInteger selectedRowIndex;


@end

