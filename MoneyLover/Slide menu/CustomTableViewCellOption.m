//
//  CustomTableViewCellOption.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/9/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "CustomTableViewCellOption.h"
#import "BouncingViewController.h"


@interface CustomTableViewCellOption ()

@property (nonatomic, assign) CGPoint preVelocity;
//@property (nonatomic, assign) CGPoint startPointGesture;

@end

@implementation CustomTableViewCellOption


- (void)awakeFromNib {
  UIPanGestureRecognizer *panGestureRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandle:)];
  panGestureRec.delegate = self;
  [self addGestureRecognizer:panGestureRec];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
 
}

- (void)swipeHandle:(id)sender{

  [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
  CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self];

   if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {

   }
  if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded){
    if(self.leftConstraint.constant > -120){
      if(self.selected == NO){
        [self.delegate moveSelectedCellToOriginalPosition];
      }
      [self showOption];
    }else{
      [self hideOption];
    }

  }
   if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {

     self.leftConstraint.constant += translatedPoint.x;
     self.rightConstraint.constant -= translatedPoint.x;
     
     if(self.leftConstraint.constant < -176){
       self.leftConstraint.constant = -176;
       self.rightConstraint.constant = 0;
     }else{
       if(self.leftConstraint.constant > 0){
         self.leftConstraint.constant = 0;
         self.rightConstraint.constant = -176;
       }
     }
     
     [self.contentView layoutIfNeeded];
   }
  
  [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:self];

}

// Just recognizing pan gesture in horizontal
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
  UIPanGestureRecognizer *panGestureRec = (UIPanGestureRecognizer*)gestureRecognizer;
  
  if([panGestureRec respondsToSelector:@selector(velocityInView:)]){
    CGPoint velocity = [panGestureRec velocityInView:self];
    return fabs(velocity.x) > fabs(velocity.y);
  }
  return YES;
}

- (IBAction)optionButtonAction:(id)sender {
  NSLog(@"option");
  
  [self.delegate moveSelectedCellToOriginalPosition];
  [self showOption];
}


- (IBAction)transferMoneyAction:(id)sender {
  NSLog(@"transer");
  
  [self hideOption];
}

- (IBAction)editWalletAction:(id)sender {
  NSLog(@"edit");
  
  [self hideOption];
}

- (IBAction)shareWalletAction:(id)sender {
  NSLog(@"share");
  
  [self hideOption];
}

- (IBAction)deleteWalletAction:(id)sender {
  NSLog(@"delete");
  
  [self hideOption];
}

- (void)showOption{
//  NSLog(@"show option");
  

  [self.contentView layoutIfNeeded];
  [UIView animateWithDuration:0.3
                   animations:^{
                     self.rightConstraint.constant = -176;
                     self.leftConstraint.constant = 0;
                     [self.contentView layoutIfNeeded];
                   }];
  self.selected = YES;
}

- (void)hideOption{
//  NSLog(@"hide option");

  [self.contentView layoutIfNeeded];
  [UIView animateWithDuration:0.3
                   animations:^{
                     
                     self.leftConstraint.constant = -176;
                     self.rightConstraint.constant = 0;
                     [self.contentView layoutIfNeeded];
                   }];
  self.selected = NO;
}

@end
