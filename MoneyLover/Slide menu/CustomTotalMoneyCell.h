//
//  SubviewOfCententViewCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/10/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTotalMoneyCell : UITableViewCell <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
