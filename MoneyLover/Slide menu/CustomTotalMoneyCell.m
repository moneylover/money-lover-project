//
//  SubviewOfCententViewCell.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/10/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "CustomTotalMoneyCell.h"

@implementation CustomTotalMoneyCell


- (void)awakeFromNib {

  [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CellIden"];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.backgroundColor = [UIColor clearColor];
  cell.imageView.image = [UIImage imageNamed:@"ic_icon_wallet@2x.png"];
  cell.textLabel.text = @"hihi";
  cell.textLabel.textColor = [UIColor blueColor];
  return cell;
}

@end
