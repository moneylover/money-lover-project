//
//  BoucingViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/7/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "BouncingViewController.h"
#import "CustomTableViewCellOption.h"
#import "CustomTotalMoneyCell.h"

@interface BouncingViewController () <CustomTableViewCellOptionDelegete>

@end

@implementation BouncingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  UITapGestureRecognizer *tapGestureRe = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
  [self.addWalletView addGestureRecognizer:tapGestureRe];
  
  
  UILongPressGestureRecognizer *longPressGestureRec = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
  [self.tableView addGestureRecognizer:longPressGestureRec];
  

  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  [self.tableView registerNib:[UINib nibWithNibName:@"CustomTableViewCellOption" bundle:nil] forCellReuseIdentifier:@"CellIdentifier"];
  [self.tableView registerNib:[UINib nibWithNibName:@"CustomTotalMoneyCell" bundle:nil] forCellReuseIdentifier:@"TotalMoneyCellIden"];
  
  self.numberOfWallets = 3;
}

- (void)viewWillLayoutSubviews{
  [super viewWillLayoutSubviews];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleTapGesture:(id)sender{
  NSLog(@"handleTapGesture");
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
  if(self.numberOfWallets == 1){
    return 1;
  }else{
    return self.numberOfWallets + 1;
  }
  
  return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  
  return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  UITableViewCell *cell;

  
  if(self.numberOfWallets == 1){
    cell = (CustomTableViewCellOption*)[tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    ((CustomTableViewCellOption*)cell).delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
  }else{
    if(indexPath.row == 0){
      cell = (CustomTotalMoneyCell*)[tableView dequeueReusableCellWithIdentifier:@"TotalMoneyCellIden" forIndexPath:indexPath];
    }else{
      cell = (CustomTableViewCellOption*)[tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
      ((CustomTableViewCellOption*)cell).delegate = self;
    }
  }
  cell.backgroundColor = [UIColor clearColor];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(self.numberOfWallets == 1){
    return 70.0f;
  }else{
    if(indexPath.row == 0){
      return self.numberOfWallets*30.0f + 10.0f;
    }else{
      return 70.0f;
    }
  }
  return 70.0f;
}


- (NSIndexPath*)tableView:(UITableView*)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
  [self moveSelectedCellToOriginalPosition];
  CustomTableViewCellOption *cell = (CustomTableViewCellOption*)[tableView cellForRowAtIndexPath:indexPath];
  cell.selected = YES;
  return indexPath;
}

- (void)moveSelectedCellToOriginalPosition{
  
  for(NSInteger i = 0; i < [self.tableView numberOfRowsInSection:0]; i ++){
    CustomTableViewCellOption *cell = (CustomTableViewCellOption*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    if(cell.selected){

      if([cell respondsToSelector:@selector(hideOption)]){
        [cell hideOption];
      }
      
    }
  }
}

- (void)handleLongPressGesture:(id)sender{
  
  UILongPressGestureRecognizer *longPressGestureRec = (UILongPressGestureRecognizer*)sender;
  UIGestureRecognizerState state = longPressGestureRec.state;

  CGPoint location= [longPressGestureRec locationInView:self.tableView];
  NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
  
  static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
  static NSIndexPath  *sourceIndexPath = nil;  ///< Initial index path, where gesture begins.
  static UITableViewCell  *firstCell = nil;

  switch (state) {
    case UIGestureRecognizerStateBegan:
    {
      if (indexPath) {
        firstCell = [self.tableView cellForRowAtIndexPath:indexPath];
        sourceIndexPath = indexPath;
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        // Take a snapshot of the selected row using helper method.
        snapshot = [self customSnapshotFromView:cell];
        
        // Add the snapshot as subview, centered at cell's center...
        __block CGPoint center = cell.center;
        snapshot.center = center;
        snapshot.alpha = 0.0;
        [self.tableView addSubview:snapshot];
        [UIView animateWithDuration:0.25
                         animations:^{
                           // Offset for gesture location.
                           center.y = location.y;
                           snapshot.center = center;
                           snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                           snapshot.alpha = 0.98;
                           // Fade out.
                           cell.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                           cell.hidden = YES;
                         }];
      }
      break;
    }
    case UIGestureRecognizerStateChanged:
    {
      CGPoint center = snapshot.center;
      center.y = location.y;
      snapshot.center = center;
      
      // Is destination valid and is it different from source?
      if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
        
        if(indexPath.row !=0 && sourceIndexPath.row != 0){
          // ... update data source.
          // [self.objects exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
          
          // ... move the rows.
          [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
          sourceIndexPath = indexPath;
        }

        // ... and update source so it is in sync with UI changes.
        sourceIndexPath = indexPath;
      }
      break;
    }
    default:
    {
      // Clean up.
      UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:sourceIndexPath];
      firstCell.hidden = NO;
      firstCell.alpha = 0.0;
      cell.hidden = NO;
      cell.alpha = 0.0;
      [UIView animateWithDuration:0.25
                       animations:^{
                         snapshot.center = cell.center;
                         snapshot.transform = CGAffineTransformIdentity;
                         snapshot.alpha = 0.0;
                         // Undo fade out.
                         cell.alpha = 1.0;
                         firstCell.alpha = 1.0;
                       }
                       completion:^(BOOL finished) {
                         sourceIndexPath = nil;
                         [snapshot removeFromSuperview];
                         snapshot = nil;
                       }];
      break;
    }
  }
  
  
}

- (UIView *)customSnapshotFromView:(UIView *)inputView {
  
  // Make an image from the input view.
  UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
  [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  // Create an image view.
  UIView *snapshot = [[UIImageView alloc] initWithImage:image];
  snapshot.layer.masksToBounds = NO;
  snapshot.layer.cornerRadius = 0.0;
  snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
  snapshot.layer.shadowRadius = 5.0;
  snapshot.layer.shadowOpacity = 0.4;
  
  return snapshot;
}
  
@end
