//
//  MainViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 8/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "MainViewController.h"
#import "TransactionViewController.h"
#import "SlideMenuViewController.h"
#import "UIImage+ImageEffects.h"
#import "CategoryTransactionViewController.h"
#import "TrendingViewController.h"
#import "BaseViewController.h"
#include "SavingsViewController.h"
#import "RecurringTransactionViewController.h"
#import "BudgetViewController.h"
#import "ExportFileViewController.h"
#import "DebtViewcontroller.h"


#define SLIDE_TIMING 0.25
#define PANEL_WIDTH 50

@interface MainViewController ()  <BaseViewControllerProtocol, SlideMenuViewControllerDelegate, UIGestureRecognizerDelegate>


@property (nonatomic, strong) BaseViewController *currentViewController;
@property (nonatomic, strong) SlideMenuViewController *slideMenuViewController;

@property (nonatomic, strong) UIGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign) BOOL showingSlideMenu;
@property (nonatomic, assign) BOOL showPanel;

@property (nonatomic, assign) CGPoint startPointGesture;


@property (nonatomic, strong) UIImageView *blurredBgImage;
@property (nonatomic, strong) UIView *blurMask;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//  NSLog(@"main viewDidLoad");
  self.navigationBarHidden = YES;
  [self setupView];
  
  UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(handlePanGesture:)];
  [panRecognizer setMinimumNumberOfTouches:1];
  [panRecognizer setMaximumNumberOfTouches:1];
  [panRecognizer setDelegate:self];
//  [self.view addGestureRecognizer:panRecognizer];
	
  self.tapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(handleTapGesture:)];
  [self.tapGestureRecognizer  setDelegate:self];
}

- (void)viewDidAppear:(BOOL)animated{
  [super viewDidAppear:animated];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  NSLog(@"main view controller didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
}

- (void)setupView {
  // setup center view
	TransactionViewController *transactionViewController = [[TransactionViewController alloc] initWithNibName:@"TransactionViewController" bundle:nil];
	transactionViewController.delegate = self;
	self.currentViewController = transactionViewController;
  self.currentViewController.view.frame = CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);

	
  [self setViewControllers:@[self.currentViewController] animated:YES];
  
  [self addChildViewController:_currentViewController];
  [_currentViewController didMoveToParentViewController:self];

}

- (void)setupSlideMenu{
  // init view if it doesn't already exist
  if (_slideMenuViewController == nil)
  {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.slideMenuViewController = [[SlideMenuViewController alloc] init];
    float slideMenuWidth = self.view.frame.size.width - PANEL_WIDTH;
    self.slideMenuViewController.view.frame = CGRectMake(-slideMenuWidth, 0.0f, slideMenuWidth, self.view.frame.size.height);
    [self.slideMenuViewController setFrameForSubView];
    self.slideMenuViewController.delegate = self;
    
    [self.view addSubview:self.slideMenuViewController.view];
		
//    [self addChildViewController:_slideMenuViewController];
//    [_slideMenuViewController didMoveToParentViewController:self];

    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handlePanGesture:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    [_slideMenuViewController.view addGestureRecognizer:panRecognizer];
  }

  self.showingSlideMenu = YES;
}

-(void)createBlurEffect{
	
	/*
	
  UIGraphicsBeginImageContext(CGSizeMake(self.view.frame.size.width, self.view.frame.size.height));
  [self.view drawViewHierarchyInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) afterScreenUpdates:NO];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  if(self.blurredBgImage == nil){
    self.blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.blurredBgImage setContentMode:UIViewContentModeScaleToFill];
  
    self.blurMask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.frame.size.height)];
    self.blurMask.backgroundColor = [UIColor whiteColor];
    self.blurredBgImage.layer.mask = self.blurMask.layer;
  }

  [((UIViewController*)self.viewControllers[0]).view addSubview:self.blurredBgImage];
//  NSLog(@"%@", [(UIViewController*)self.viewControllers[0] class]);
  self.blurredBgImage.image = [image applyBlurWithRadius:20 tintColor:[UIColor colorWithWhite:1 alpha:0.1] saturationDeltaFactor:1.5 maskImage:nil];
	
	*/
}

-(void)handleTapGesture:(id)sender{
//  NSLog(@"handle tap gesture");
  if(self.showingSlideMenu == YES){
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    [self moveSlideMenuToOriginalPosition];
  }
}

-(void)handlePanGesture:(id)sender{
//  NSLog(@"main vc handlePanGesture");

  [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
  
  CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
  CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];

  if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
		
    self.startPointGesture = [(UIPanGestureRecognizer*)sender locationInView:self.view];

//    UIView *childView = nil;
    if(velocity.x > 0) {
      if (!_showingSlideMenu && self.startPointGesture.x < 28.0f) {
        [self setupSlideMenu];
        [self createBlurEffect];
      }
    }
  }
  
  if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
//    NSLog(@"pan gesture end");
    
    if (!_showPanel) {
      [self moveSlideMenuToOriginalPosition];
    } else {
      if (_showingSlideMenu) {
        CGRect rect = CGRectMake(0.0f, 0.0f, (self.view.frame.size.width - PANEL_WIDTH), self.view.frame.size.height);
        if(self.slideMenuViewController.view.frame.size.width > rect.size.width){
          self.slideMenuViewController.view.frame = rect;
          self.blurMask.frame = rect;
        }else {
          [self showSlidemenuAnimation];
        }
      }
    }
    

  }
  
  if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
//    NSLog(@"pan gesture changed");
    if(self.startPointGesture.x < 28.0f || self.showingSlideMenu){
      
      _showPanel = _slideMenuViewController.view.center.x > -10.0f;
      
      float slideMenuFrameWidth = _slideMenuViewController.view.frame.size.width;
      float originalX = _slideMenuViewController.view.frame.origin.x + translatedPoint.x;
      
      if(originalX > 0.0f || (_slideMenuViewController.view.frame.origin.x == 0 && (_slideMenuViewController.view.frame.size.width > (self.view.frame.size.width - PANEL_WIDTH)))){
        originalX = 0.0f;
        slideMenuFrameWidth += translatedPoint.x;
      }
      
      if(slideMenuFrameWidth > self.view.frame.size.width){
        slideMenuFrameWidth = self.view.frame.size.width;
      }
      
      if(slideMenuFrameWidth < (self.view.frame.size.width - PANEL_WIDTH)){
        slideMenuFrameWidth = self.view.frame.size.width - PANEL_WIDTH;
      }
      
      //    if(self.startPointGesture.x < 28.0f || (_slideMenuViewController.view == ((SlideMenuViewController*)sender).view)){
      
      _slideMenuViewController.view.frame = CGRectMake(originalX, 0.0f, slideMenuFrameWidth, _slideMenuViewController.view.frame.size.height);
      
      if(originalX == 0.0f){
        self.blurMask.frame = CGRectMake(0.0f, 0.0f, slideMenuFrameWidth, self.view.frame.size.height);
      }else{
        self.blurMask.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width - PANEL_WIDTH + _slideMenuViewController.view.frame.origin.x, self.view.frame.size.height);
      }
      //    }
      
      [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:self.view];
    }
  }
}

- (void)showSlideMenu{
//	NSLog(@"Show slide menu");
  if(self.slideMenuViewController == nil){
    [self setupSlideMenu];
  }
  self.showingSlideMenu = YES;
  [self createBlurEffect];
  [self showSlidemenuAnimation];
}

- (void)showSlidemenuAnimation{
//  NSLog(@"Show slide menu animation");
  [UIView animateWithDuration:SLIDE_TIMING
                        delay:0
                      options:UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
                     CGRect rect = CGRectMake(0.0f, 0.0f, (self.view.frame.size.width - PANEL_WIDTH), self.view.frame.size.height);
                     self.slideMenuViewController.view.frame = rect;
                     self.blurMask.frame = rect;
                   }
                   completion:^(BOOL finished) {
                     if (finished) {
                       [((UIViewController*)self.viewControllers[0]).view addGestureRecognizer:self.tapGestureRecognizer];
                     }
                   }];
}

- (void)moveSlideMenuToOriginalPosition{
//  NSLog(@"move panel to original position");
 
  [UIView animateWithDuration:SLIDE_TIMING
                        delay:0
                      options:UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
                     _slideMenuViewController.view.frame = CGRectMake(-(self.view.frame.size.width - PANEL_WIDTH), 0.0f,_slideMenuViewController.view.frame.size.width, _slideMenuViewController.view.frame.size.height);
                     self.blurMask.frame = CGRectMake(0, 0, 0, self.view.frame.size.height);
                   }
                   completion:^(BOOL finished) {
                     if (finished) {
//                       [self resetMainView];
                       self.showingSlideMenu = NO;
                       [((UIViewController*)self.viewControllers[0]).view removeGestureRecognizer:self.tapGestureRecognizer];
                     }
                   }];
}

- (void)resetMainView{
  if(_slideMenuViewController != nil){
    [self.slideMenuViewController.view removeFromSuperview];
    self.slideMenuViewController = nil;
    self.showingSlideMenu = NO;
    self.blurMask = nil;
    
    [self.blurredBgImage removeFromSuperview];
    self.blurredBgImage = nil;
  }
}

// SlideMenuViewController delegate:
- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
  
  [self moveSlideMenuToOriginalPosition];
  
  switch (indexPath.section) {
		case 0:{
			switch (indexPath.row) {
				case 0:
				{
					TransactionViewController *center = [[TransactionViewController alloc] initWithNibName:@"TransactionViewController" bundle:nil];
					center.delegate = self;
					self.currentViewController = center;
				}
					break;
//				case 1: // Sổ nợ
//				{
//					DebtViewcontroller *debtViewController = [[DebtViewcontroller alloc] initWithNibName:@"DebtViewcontroller" bundle:nil];
//					debtViewController.delegate = self;
//					self.currentViewController = debtViewController;
//				}
//					break;
				case 1:{
					TrendingViewController *trendingViewController = [[TrendingViewController alloc] initWithNibName:@"TrendingViewController" bundle:nil];
					trendingViewController.delegate = self;
					self.currentViewController = trendingViewController;
				}
					
					break;
				case 2:
				{
					CategoryTransactionViewController *categoryTransactionVC = [[CategoryTransactionViewController alloc] init];
					categoryTransactionVC.delegate  = self;
					self.currentViewController = categoryTransactionVC;
				}
					break;
				default:
					break;
			}
		}

      break;
		case 1:{
			switch (indexPath.row) {
				case 0:{
					
					BudgetViewController *budgetViewController = [[BudgetViewController alloc] initWithNibName:@"BudgetViewController" bundle:nil];
					budgetViewController.delegate = self;
					
					self.currentViewController = budgetViewController;
					
				}
					break;
				case 1:{
					
					SavingsViewController *savingViewCotroller = [[SavingsViewController alloc] initWithNibName:@"SavingsViewController" bundle:nil];
					savingViewCotroller.delegate = self;
					
					self.currentViewController = savingViewCotroller;
					
				}
					break;

				case 2:{
					RecurringTransactionViewController *recurringVC = [[RecurringTransactionViewController alloc] initWithNibName:@"RecurringTransactionViewController" bundle:nil];
					recurringVC.delegate = self;
					
					self.currentViewController = recurringVC;
				}
					break;
				default:
					break;
			}
		}
			
      break;
		case 2:{
			if(indexPath.row ==0){
					// Xuất file ở đây
				ExportFileViewController *exportVC = [[ExportFileViewController alloc] initWithNibName:@"ExportFileViewController" bundle:nil];
				exportVC.delegate = self;
				self.currentViewController = exportVC;
			}
		}
			
      break;

    default:
      break;
  }

  
  
  [self setViewControllers:@[self.currentViewController]];
}


// UIGestureRecognizerDelegate method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
  
  if(self.slideMenuViewController != nil){
    if(self.slideMenuViewController.finishedBouncingAnimation == YES){
      return YES;
    }else{
      return NO;
    }
  }
  return YES;
}



@end
