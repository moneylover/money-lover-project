//
//  SlideMenuViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 8/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "SlideMenuViewController.h"
//#import "DeviceName.h"
#import "CustomCellSlideMenu.h"
#import <QuartzCore/QuartzCore.h>


@interface SlideMenuViewController ()

@property (nonatomic, strong) UIDynamicAnimator *dynamicAnimator;
@property (nonatomic, strong) UIPushBehavior *pushBehavior;
@property (nonatomic, strong) UIGravityBehavior *gravityBehavior;

@property (assign, nonatomic) CGFloat bouncingViewHeight;
@property (assign, nonatomic) CGFloat bouncingViewYPosition;

@end

@implementation SlideMenuViewController

- (id)init{
  if(self = [super init]){
  }
  return self;
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
//  NSLog(@"slide menu view will appear");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//	NSLog(@"slide menu viewDidLoad");
  self.showingBouncingView = NO;
  self.finishedBouncingAnimation = YES;
  
  self.reviewWalletsView.layer.masksToBounds = NO;
  self.reviewWalletsView.layer.shadowOffset = CGSizeMake(0, 5);
  self.reviewWalletsView.layer.shadowRadius = 3.0;
  self.reviewWalletsView.layer.shadowOpacity = 0.3;
  
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
  self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.92f];
  
  UITapGestureRecognizer *tapGestureForTopView = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:self
                                                          action:@selector(handleTapGesture:)];
  
  UITapGestureRecognizer *tapGestureForSecondView = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:self
                                                          action:@selector(handleTapGesture:)];
  
  UITapGestureRecognizer *tapGestureForBottomView = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:self
                                                          action:@selector(handleTapGesture:)];
//  tapGestureForTopView.delegate = self;
////  tapGestureForSecondView.delegate = self;
//  tapGestureForBottomView.delegate = self;
  
  [self.topView addGestureRecognizer:tapGestureForTopView];
  [self.reviewWalletsView addGestureRecognizer:tapGestureForSecondView];
  [self.bottomView addGestureRecognizer:tapGestureForBottomView];
  
}

- (void)viewWillLayoutSubviews{
  [super viewWillLayoutSubviews];
  
  self.bouncingViewController.view.frame = CGRectMake(0.0f, self.bouncingViewController.view.frame.origin.y, self.view.frame.size.width, self.bouncingViewController.view.frame.size.height);
  
}

- (void)handleTapGesture:(UITapGestureRecognizer*)tapGesture{
  
  if(tapGesture.view == self.reviewWalletsView){
    
    if(self.finishedBouncingAnimation == YES){
      self.view.userInteractionEnabled = NO;
      self.finishedBouncingAnimation = NO;
      if(self.showingBouncingView == NO){
        [self moveDownBouncingView];
//        self.bouncingViewController.view.frame = CGRectMake(0.0f, self.topView.frame.size.height + self.reviewWalletsView.frame.size.height,self.bouncingViewController.view.frame.size.width, self.bouncingViewHeight);
        [UIView animateWithDuration:0.3f
                         animations:^{
                           self.upArrow.transform = CGAffineTransformMakeRotation(M_PI);
                         }
                         completion:^(BOOL finished) {
                           
                         }];
      }else{

        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                           self.bouncingViewController.view.frame = CGRectMake(0.0f, self.bouncingViewYPosition,self.bouncingViewController.view.frame.size.width, self.bouncingViewHeight);
                           self.upArrow.transform = CGAffineTransformMakeRotation(0);
                         }
                         completion:^(BOOL finished) {
                           self.finishedBouncingAnimation = YES;
                           self.view.userInteractionEnabled = YES;
                         }];
      }
      
    self.showingBouncingView = !self.showingBouncingView;

    }
    
  }else{
    if(tapGesture.view == self.topView){
      
    }else{
      
      if(tapGesture.view == self.bottomView){
        
      }else{
        
      }
    }
  }
  
}

- (void)moveDownBouncingView{
	
	
  UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.bouncingViewController.view]];
  gravityBehavior.gravityDirection = CGVectorMake(0.0f, 7.0f);
  [self.dynamicAnimator addBehavior:gravityBehavior];
  
  UICollisionBehavior* collisionBehavior =
  [[UICollisionBehavior alloc] initWithItems:@[self.bouncingViewController.view]];
  [collisionBehavior addBoundaryWithIdentifier:@"bottomBoundary" fromPoint:CGPointMake(0.0f, self.view.frame.size.height + 1) toPoint:CGPointMake(self.view.frame.size.width, self.view.frame.size.height + 1)];
  [self.dynamicAnimator addBehavior:collisionBehavior];
  
  UIDynamicItemBehavior *elasticityBehavior =
  [[UIDynamicItemBehavior alloc] initWithItems:@[self.bouncingViewController.view]];
  elasticityBehavior.elasticity = 0.3f;
  [self.dynamicAnimator addBehavior:elasticityBehavior];
	
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator*)animator{
	
  [self.dynamicAnimator removeAllBehaviors];
  self.finishedBouncingAnimation = YES;
  self.view.userInteractionEnabled = YES;
	
}

- (void)setFrameForSubView{
  CGFloat positionY = self.topView.frame.size.height + self.reviewWalletsView.frame.size.height;
  
  self.tableView.frame = CGRectMake(0.0f, positionY, self.view.frame.size.width, self.view.frame.size.height - positionY - self.bottomView.frame.size.height);

  UIImageView *imageView = [[UIImageView alloc] init];
  imageView.frame = CGRectMake(self.tableView.frame.size.width/2.0f - 50,
                               -130.0f,
                               80.0f,
                               80.0f);
 
  imageView.image = [UIImage imageNamed:@"navigation_logo_subdued@2x.png"];
  imageView.backgroundColor = [UIColor clearColor];
  [self.tableView addSubview:imageView];
  
  
  self.bouncingViewController = [[BouncingViewController alloc] initWithNibName:@"BouncingViewController" bundle:nil];
  
  self.bouncingViewHeight = self.view.frame.size.height - self.topView.frame.size.height - self.reviewWalletsView.frame.size.height ;
  self.bouncingViewYPosition = -(self.bouncingViewHeight - self.topView.frame.size.height - self.reviewWalletsView.frame.size.height);
  
  self.bouncingViewController.view.frame = CGRectMake(0.0f, self.bouncingViewYPosition, self.view.frame.size.width, self.bouncingViewHeight);

  [self.view insertSubview:self.bouncingViewController.view aboveSubview:self.bottomView];
  
  [self addChildViewController:self.bouncingViewController];
  [self.bouncingViewController didMoveToParentViewController:self];
  
  
  self.dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
  self.dynamicAnimator.delegate = self;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  switch(section){
    case 0:
      return 3;
      break;
    case 1:
      return 3;
      break;
    case 2:
      return 2;
      break;
//    case 3:
//      return 5;
//      break;
  }
  return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
 
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  CustomCellSlideMenu *cell = [[CustomCellSlideMenu alloc] init];
  
  [cell setSelected:YES];
  [cell setSelected:YES animated:YES];
  cell.selectedBackgroundView = [UIView new];
  cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
  [cell.textLabel sizeToFit];
  
  switch(indexPath.section){
    case 0:
    {
      switch (indexPath.row){
        case 0:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_cashbook.png"];
          cell.textLabel.text = @"Sổ giao dịch";
          break;
//        case 1:
//          cell.imageView.image = [UIImage imageNamed:@"img_ic_debt_manager@2x.png"];
//          cell.textLabel.text = @"Sổ nợ";
//          break;
        case 1:
          cell.imageView.image = [UIImage imageNamed:@"ic_navigation_monthly_report@2x.png"];
          cell.textLabel.text = @"Xu hướng";
          break;
        case 2:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_category_manager.png"];
          cell.textLabel.text = @"Nhóm";
          break;
        default:
          break;
      }
      break;
    }
    case 1:
    {
      switch (indexPath.row){
        case 0:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_budget.png"];
          cell.textLabel.text = @"Ngân sách";
          break;
        case 1:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_navigation_saving.png"];
          cell.textLabel.text = @"Tiết kiệm";
          break;
//        case 2:
//          cell.imageView.image = [UIImage imageNamed:@"img_ic_navigation_event.png"];
//          cell.textLabel.text = @"Sự kiện";
//          break;
        case 2:
          cell.imageView.image = [UIImage imageNamed:@"ic_navigation_recurring_transactions@2x.png"];
          cell.textLabel.text = @"Giao dịch định kì";
          break;
        default:
          break;
      }
      break;
    }
    case 2:
    {
      switch (indexPath.row){
        case 0:
          cell.imageView.image = [UIImage imageNamed:@"ic_navigation_database@2x.png"];
          cell.textLabel.text = @"Xuất file";
          break;
        default:
          break;
      }
      break;
    }
			/*
    case 3:
    {
      switch (indexPath.row){
        case 0:
          cell.imageView.image = [UIImage imageNamed:@"ic_navigation_travel_mode@2x.png"];
          cell.textLabel.text = @"Chế độ du lịch";
          break;
        case 1:
          cell.imageView.image = [UIImage imageNamed:@"ic_navigation_shop@2x.png"];
          cell.textLabel.text = @"Cửa hàng biểu tượng";
          break;
        case 2:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_feedback.png"];
          cell.textLabel.text = @"Đóng góp";
          break;
        case 3:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_action_about.png"];
          cell.textLabel.text = @"Hướng dẫn";
          break;
        case 4:
          cell.imageView.image = [UIImage imageNamed:@"img_ic_setting@2x.png"];
          cell.textLabel.text = @"Cài đặt";
          break;
        default:
          break;
      }
      break;
    }
			*/
    default:
      break;
  }
  cell.backgroundColor = [UIColor clearColor];
  
  return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

  UILabel *labelForHeader = [[UILabel alloc] init];

  switch(section){
    case 0:
      labelForHeader.text = @"Quản lý giao dịch";
      break;
    case 1:
      labelForHeader.text = @"Lập kế hoạch";
      break;
    case 2:
      labelForHeader.text = @"Tiện ích";
      break;
//    case 3:
//      labelForHeader.text = @"Khác";
//      break;
    default:
      break;
  }
  labelForHeader.backgroundColor = [UIColor clearColor];
  labelForHeader.textColor = [UIColor grayColor];
  labelForHeader.frame = CGRectMake(20.0f, labelForHeader.frame.origin.y + 10.0, labelForHeader.frame.size.width, labelForHeader.frame.size.height);
  [labelForHeader sizeToFit];

  
  UIView *header = nil;
  CGRect resultFrame = CGRectMake(0.0f,
                                  0.0f,
                                  labelForHeader.frame.size.width ,
                                  labelForHeader.frame.size.height);
  header = [[UIView alloc] initWithFrame:resultFrame];
  
  
  [header addSubview:labelForHeader];
  header.backgroundColor = [UIColor clearColor];
  return header;
}

- (CGFloat)tableView:(UITableView*) tableView heightForHeaderInSection:(NSInteger)section{

  return 40.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

  [self.delegate didSelectRowAtIndexPath:indexPath];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
