//
//  BudgetViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "BudgetViewController.h"
#import "Budget.h"
#import "SQLiteManager.h"
#import "AddBudgetViewController.h"
#import "BudgetCell.h"
#import "FPPopoverController.h"
#import "OptionTableForSavingCellViewController.h"
#import "BudgetCell.h"
#import "CategoryTransaction.h"
#import "BudgetManager.h"


@interface BudgetViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, BudgetCellDelegate, AddBudgetDelegate,  OptionTableForSavingCellViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *runningButton;
@property (weak, nonatomic) IBOutlet UIButton *finishedButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UITableView *runningBudgetTableView;
@property (strong, nonatomic) UITableView *expriedBudgetTableView;

@property (strong, nonatomic) NSMutableArray *runningBudgetArray;
@property (strong, nonatomic) NSMutableArray *expriedBudgetArray;
//@property (strong, nonatomic) NSMutableArray *moneyArrayWhenSavingsExpried;

@property (strong, nonatomic) FPPopoverController *popoverViewController;

@property (strong, nonatomic) Budget *selectedBudget;
//@property (strong, nonatomic) FPPopoverController *showDetailPopoverViewController;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UILabel *emptyDataLabel_left;
@property (strong, nonatomic) UILabel *emptyDataLabel_right;

@end

@implementation BudgetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

//	self.runningBudgetArray = [[BudgetManager getInstance] getUnexpiredBudget];
//	self.expriedBudgetArray = [[BudgetManager getInstance] getExpiredBudget];
	
	CGFloat tableViewWidth = [[UIScreen mainScreen] bounds].size.width;
	CGFloat tableViewHeight = [UIScreen mainScreen].bounds.size.height - 108;
	
	[self.scrollView setContentSize:CGSizeMake(tableViewWidth*2, tableViewHeight)];
	self.scrollView.delegate = self;
	
	
	// Thiết lập UILabel khi không có dữ liệu cho self.runningBudgetTableView
	self.emptyDataLabel_left = [[UILabel alloc] initWithFrame:CGRectMake(70, tableViewHeight/2, 100, 50)];
	self.emptyDataLabel_left.text = @"Không có dữ liệu";
	[self.emptyDataLabel_left setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel_left setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel_left sizeToFit];
	[self.scrollView addSubview:self.emptyDataLabel_left];
	
	
	// Thiết lập UILabel khi không có dữ liệu cho self.expriedBudgetTableView
	self.emptyDataLabel_right = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width + 70, tableViewHeight/2, 100, 30)];
	self.emptyDataLabel_right.text = @"Không có dữ liệu";
	[self.emptyDataLabel_right setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel_right setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel_right sizeToFit];
	
	[self.scrollView addSubview:self.emptyDataLabel_right];
	
	
	//  Tạo bảng các ngân sách đang được thực hiện
	self.runningBudgetTableView = [[UITableView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, tableViewWidth - 20, tableViewHeight - 10) style:UITableViewStyleGrouped];
	
	self.runningBudgetTableView.delegate = self;
	self.runningBudgetTableView.dataSource = self;
	self.runningBudgetTableView.tag = 1;
	[self.runningBudgetTableView registerNib:[UINib nibWithNibName:@"BudgetCell" bundle:nil] forCellReuseIdentifier:@"BudgetCellIden"];
	self.runningBudgetTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.scrollView addSubview:_runningBudgetTableView];
	
	
	// Tạo bảng các ngân sách đã hết hạn
	self.expriedBudgetTableView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewWidth + 10, 10.0f, tableViewWidth - 20 , tableViewHeight - 10) style:UITableViewStyleGrouped];
	self.expriedBudgetTableView.delegate = self;
	self.expriedBudgetTableView.dataSource = self;
	self.expriedBudgetTableView.tag = 2;
	[self.expriedBudgetTableView registerNib:[UINib nibWithNibName:@"BudgetCell" bundle:nil] forCellReuseIdentifier:@"BudgetCellIden"];
	self.expriedBudgetTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.scrollView addSubview:_expriedBudgetTableView];
	

	
	
	self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tableViewHeight - 6, 0);
	self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
	self.scrollView.pagingEnabled = YES;
	
	[_runningButton addTarget:self action:@selector(runningButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	[_finishedButton addTarget:self action:@selector(finishedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	_runningButton.enabled = NO;
	_finishedButton.enabled = YES;
	
	
	[self reloadDataBudgetViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	
}



- (void)cancelAddingSaving{
	//	NSLog(@"Cancel");
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	
		if(tableView.tag == 1){
			return [self.runningBudgetArray count];
		}else{
			return [self.expriedBudgetArray count];
		}
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
		return 1;
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
		return 230.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	Budget *budget;
	BudgetCell *cell = (BudgetCell *)[tableView dequeueReusableCellWithIdentifier:@"BudgetCellIden"];
	
	if(tableView.tag == 1){
		
			budget = (Budget *)self.runningBudgetArray[indexPath.section];

	}else{
	
			budget = (Budget *)self.expriedBudgetArray[indexPath.section];

	}
	
	cell.icon_imageView.image = [UIImage imageNamed:budget.category.iconName];
	cell.nameLabel.text = budget.category.name;
	cell.budgetMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(budget.maximumAmount)];
	cell.spentMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(budget.currentAmount)];
	cell.overspentLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(llabs(budget.maximumAmount) - llabs(budget.currentAmount))];
	cell.progress_View.progress = fabs(budget.currentAmount*1.0/budget.maximumAmount);
	NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:budget.startDate];
	NSDate *endDate = [NSDate dateWithTimeIntervalSinceReferenceDate:budget.endDate];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat =@"dd/MM/YY";
	cell.noteLabel.text = [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:startDate], [dateFormatter stringFromDate:endDate] ];

	
	if(budget.state == 0){
		
			cell.budgetStateLabel.text = @"Đang thực hiện";
		
	}else{
		
			cell.budgetStateLabel.text = @"Đã kết thúc";
	}
	

	if((llabs(budget.maximumAmount) - llabs(budget.currentAmount)) < 0){
		cell.remainingLabel.text = @"Bội chi";
		cell.remainingLabel.textColor = [UIColor redColor];
		cell.overspentLabel.textColor = [UIColor redColor];
		cell.progress_View.progressTintColor = [UIColor redColor];
		
	}else{
		cell.remainingLabel.text = @"Còn lại";
		cell.remainingLabel.textColor = [UIColor darkGrayColor];
		cell.overspentLabel.textColor = [UIColor darkGrayColor];
		cell.progress_View.progressTintColor = [UIColor greenColor];
	}
	
	cell.budget = budget;
	cell.delegate = self;
	
	return cell;

}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	

	
//	[self.navigationController pushViewController: animated:YES];
	
}

#pragma mark  - Declaring selector for buttons

- (void)runningButtonAction:(UIButton*)button{
	_runningButton.enabled = NO;
	_finishedButton.enabled = YES;
	
	[_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)finishedButtonAction:(UIButton*)button{
	_finishedButton.enabled = NO;
	_runningButton.enabled = YES;
	
	[_scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
}

#pragma mark - IBAction

- (IBAction)addBudget:(id)sender {
	
	AddBudgetViewController *addBudgetVC  = [[AddBudgetViewController alloc] init];
	addBudgetVC.delegate = self;
	addBudgetVC.editedBudget = nil;
	
	[self.navigationController pushViewController:addBudgetVC animated:YES];
	
}

- (IBAction)showSlideMenu:(id)sender {
	
		[self.delegate showSlideMenu];
	
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	CGFloat contentOffSetX = scrollView.contentOffset.x;
	if(contentOffSetX == 0){
		_finishedButton.enabled = YES;
		_runningButton.enabled = NO;
	}else{
		if(scrollView.contentOffset.x == scrollView.frame.size.width){
			_finishedButton.enabled = NO;
			_runningButton.enabled = YES;
		}
	}
}

#pragma mark - BudgetCellDelegate

- (void)selectedOptionButtonInBudgetCell:(Budget *)budget andSender:(UIButton *)sender{
	
	self.selectedBudget = budget;
	NSInteger tableOption;
	
	if(self.selectedBudget.state == STATE_EXPIRED){
		tableOption = 1;
	}else{
		// self.selectedBudget.state == 2
		tableOption = 2;
	}
	
	OptionTableForSavingCellViewController *tableViewController = [[OptionTableForSavingCellViewController alloc] initWithNumberOfOption:tableOption];
	
	tableViewController.delegate = self;
	
	self.popoverViewController = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverViewController.contentSize = CGSizeMake(140,(tableOption == 1) ? 65 : 90);
	self.popoverViewController.border = NO;
	self.popoverViewController.arrowDirection = FPPopoverArrowDirectionVertical;
	self.popoverViewController.tint = FPPopoverLightGrayTint;
	
	[self.popoverViewController presentPopoverFromView:sender];
	
	
};

#pragma mark - OptionTableForSavingCellViewControllerDelegate

- (void)selectedOptionAtIndex:(NSInteger)index{
	//	NSLog(@"%d", index);
	[self.popoverViewController dismissPopoverAnimated:YES];
	
	if(self.selectedBudget.state == STATE_EXPIRED){
		if (index == 0) {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
																message:@"Bạn có chắc muốn xoá ngân sách này không?"
															   delegate:self
													  cancelButtonTitle:@"Không"
													  otherButtonTitles:@"Có", nil];
			[alertView show];
			
		}
	}else{
		
		if (index == 0) {
			AddBudgetViewController *addBudgetVC = [[AddBudgetViewController alloc] initWithNibName:@"AddBudgetViewController" bundle:nil];
			addBudgetVC.delegate = self;
			addBudgetVC.editedBudget = self.selectedBudget;
			
			[self.navigationController pushViewController:addBudgetVC animated:YES];
			
		}else{
			if(index == 1){
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
																	message:@"Bạn có chắc muốn xoá ngân sách này không?"
																   delegate:self
														  cancelButtonTitle:@"Không"
														  otherButtonTitles:@"Có", nil];
				[alertView show];
			}
		}
		
	}
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(buttonIndex == 0){
		
	}else{
		[[BudgetManager getInstance] deleteBudget:self.selectedBudget];
//		
//		self.runningBudgetArray = [[BudgetManager getInstance] getUnexpiredBudget];
//		self.expriedBudgetArray = [[BudgetManager getInstance] getExpiredBudget];
//		
//		[self.runningBudgetTableView reloadData];
//		[self.expriedBudgetTableView reloadData];
		
		[self reloadDataBudgetViewController];
		
		/*
		// Cancel local notification
		NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
		
		for (UILocalNotification *localNotification in localNotifications){
				NSDictionary *userInfo = localNotification.userInfo;
			
				NSString *localNotificationWithSavingName = [userInfo valueForKey:@"Start_Budget"];
			
				if([localNotificationWithSavingName isEqualToString:self.selectedBudget.category.name]){
						[[UIApplication sharedApplication] cancelLocalNotification:localNotification];
//						break;
				}
			
				NSString *endLocalNotificationWithBudgetName = [userInfo valueForKey:@"End_Budget"];
			
				if([endLocalNotificationWithBudgetName isEqualToString:self.selectedBudget.category.name]){
						[[UIApplication sharedApplication] cancelLocalNotification:localNotification];
//						break;
				}
			
		}
		 */
	}
}


#pragma mark -  AddBudgetDelegate

- (void) completeAddingBudget{
//		NSLog(@"Update");
	
	[self reloadDataBudgetViewController];
	
}

- (void)reloadDataBudgetViewController{
	
	self.runningBudgetArray = [[BudgetManager getInstance] getUnexpiredBudget];
	self.expriedBudgetArray = [[BudgetManager getInstance] getExpiredBudget];
	
	[self.runningBudgetTableView reloadData];
	[self.expriedBudgetTableView reloadData];
	
	// Ẩn hiện bảng và label tương ứng
	if([self.runningBudgetArray count] == 0){
		
		self.emptyDataLabel_left.hidden = NO;
		self.runningBudgetTableView.hidden = YES;
		
	}else{
		
		self.emptyDataLabel_left.hidden = YES;
		self.runningBudgetTableView.hidden = NO;
		
	}
	
	if([self.expriedBudgetArray count] == 0){
		
		self.emptyDataLabel_right.hidden= NO;
		self.expriedBudgetTableView.hidden = YES;
		
	}else{
		
		self.emptyDataLabel_right.hidden= YES;
		self.expriedBudgetTableView.hidden = NO;
		
	}
}

@end


