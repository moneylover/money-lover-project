//
//  BudgetCell.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "BudgetCell.h"

@implementation BudgetCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)optionAction:(id)sender {
	
	[self.delegate selectedOptionButtonInBudgetCell:self.budget andSender:sender];
	
}

@end
