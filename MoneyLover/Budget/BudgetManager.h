//
//  BudgetManager.h
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/12/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#ifndef MoneyLover_BudgetManager_h
#define MoneyLover_BudgetManager_h

@class Transaction;
@class CategoryTransaction;
@class Budget;

@interface BudgetManager : NSObject



+(id)getInstance;
-(BOOL)createBudgetForCategory:(CategoryTransaction*)category withStartDate:(NSInteger)startDate withEndDate:(NSInteger)endDate withMaximumAmount:(long long)maximumAmount;
-(void)checkTransaction:(Transaction*)transaction;
-(void)updateBudget:(Budget*) budget;
-(void)deleteBudget:(Budget*) budget;

-(void)updateBudgetList;

//-(void)detectNewlyStartedBudgets;
-(void)detectExpiredBudgets;

-(NSMutableArray*)getUnexpiredBudget;
-(NSMutableArray*)getExpiredBudget;

@end

#endif
