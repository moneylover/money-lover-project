//
//  BudgetCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Budget;

@protocol BudgetCellDelegate <NSObject>

@required
- (void)selectedOptionButtonInBudgetCell:(Budget *)budget andSender:(UIButton *)sender;

@end


@interface BudgetCell : UITableViewCell

@property (assign, nonatomic) NSInteger runningOrExpried_budget; // 0 1

@property (weak, nonatomic) id <BudgetCellDelegate> delegate;
@property (strong, nonatomic) Budget *budget;

@property (weak, nonatomic) IBOutlet UIImageView *icon_imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *spentMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *overspentLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progress_View;
@property (weak, nonatomic) IBOutlet UILabel *budgetStateLabel; // "Bội chi" hay "Còn lại"
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;

@end
