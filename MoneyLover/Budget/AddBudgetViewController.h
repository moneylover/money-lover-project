//
//  AddBudgetViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Budget.h"


@protocol AddBudgetDelegate <NSObject>

@required
- (void) completeAddingBudget;

@optional
- (void) cancelAddingBudget;

@end


@interface AddBudgetViewController : UIViewController

@property (weak, nonatomic) id <AddBudgetDelegate> delegate;
@property (strong, nonatomic) Budget *editedBudget;

@end
