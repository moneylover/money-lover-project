//
//  BudgetManager.m
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/12/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BudgetManager.h"
#import "Budget.h"
#import "Transaction.h"
#import "CategoryTransaction.h"
#import "SQLiteManager.h"
#import "NotificationCenterManager.h"

@interface BudgetManager ()

@property(nonatomic, strong) NSMutableArray* budgetList;

@end


@implementation BudgetManager

+(id)getInstance {
    static BudgetManager* instance = nil;
    static dispatch_once_t once = 0;
    dispatch_once(&once, ^{instance = [[self alloc] initPrivate];});
    return instance;
}

-(id)initPrivate {
    if ((self = [super init])) {
        [self updateBudgetList];
        [self detectNewlyStartedBudgets];
        [self detectExpiredBudgets];
    }
    return self;
}

-(id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(BOOL)createBudgetForCategory:(CategoryTransaction*)category withStartDate:(NSInteger)startDate withEndDate:(NSInteger)endDate withMaximumAmount:(long long)maximumAmount {
    BOOL check = YES;
	
	NSMutableArray *runningBudget = [self getUnexpiredBudget];
    for (int i = 0; i < runningBudget.count; i++) {
        Budget* budget = (Budget*)[runningBudget objectAtIndex:i];
        if (budget.category.Id == category.Id) {
            check = NO;
        }
    }
    
    if (check) {
        Budget* budget = [[Budget alloc] init];
        budget.category = category;
        budget.startDate = startDate;
        budget.endDate = endDate;
        budget.currentAmount = [[SQLiteManager database] getAmountOfMoneyForBudget:budget];
        budget.maximumAmount = maximumAmount;
        budget.state = STATE_NOT_BEGIN;
        [[SQLiteManager database] insertBudget:budget];
			
        [self updateBudgetList];
				[self detectExpiredBudgets];
//        [self.budgetList addObject:budget];
        return true;
    } else {
        return false;
    }
}

-(void)checkTransaction:(Transaction*)transaction {
    for (int i = 0; i < self.budgetList.count; i++) {
        Budget* budget = [self.budgetList objectAtIndex:i];
//        NSLog(@"@mark log: Budget cate id = %ld", (long)budget.category.Id);
//        NSLog(@"@mark log: transaction cate id = %ld", (long)transaction.category_id);
        if (transaction.category_id == budget.category.Id) {
            [self updateBudgetList];
            [self checkForBudgetWarning];
        }
    }
}

-(void)updateBudget:(Budget*) budget {
	
    [[SQLiteManager database] updateBudget:budget];
    [self updateBudgetList];
		[self detectExpiredBudgets];
}

-(void)deleteBudget:(Budget*) budget {
    [[SQLiteManager database] deleteBudget:budget];
    [self updateBudgetList];
//		[self detectExpiredBudgets];
}

-(void)updateBudgetList {
	
    self.budgetList = [[SQLiteManager database] getAllBudgets];
}


-(void)detectNewlyStartedBudgets {
	
    for (Budget* budget in self.budgetList) {
        if (budget.state == STATE_NOT_BEGIN) {
//            NSInteger currentInterval = [NSDate date].timeIntervalSinceReferenceDate;
//            NSInteger startInterval = budget.startDate;
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			dateFormatter.dateFormat = @"YYYY-MM-DD EEEE";
			NSString *startDateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.startDate]];
			NSString *todayString = [dateFormatter stringFromDate:[NSDate date]];
			
            if ([startDateString compare:todayString] == NSOrderedAscending) {
                budget.state = STATE_BEGAN;
                [[SQLiteManager database] updateBudget:budget];
                NSDateComponents* startDateComp = [[NSCalendar currentCalendar]
                                                   components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                                   fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.startDate]];
                NSDateComponents* endDateComp = [[NSCalendar currentCalendar]
                                                 components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                                 fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.endDate]];
                NSString* message = [NSString stringWithFormat:@"Ngày bắt đầu = %ld/%ld/%ld\nNgày kết thúc = %ld/%ld/%ld",
                                     (long)startDateComp.day, (long)startDateComp.month, (long)startDateComp.year,
                                     (long)endDateComp.day, (long)endDateComp.month, (long)endDateComp.year];
                
                NSString* title = [NSString stringWithFormat:@"Ngân sách bắt đầu: %@", budget.category.name];
                [[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
            }
        }
    }
}


-(void)detectExpiredBudgets {
    for (Budget* budget in self.budgetList) {
        if (budget.state != STATE_EXPIRED) {
//            NSInteger currentInterval = [NSDate date].timeIntervalSinceReferenceDate;
//            NSInteger expiredInterval = budget.endDate;
			
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			dateFormatter.dateFormat = @"YYYY-MM-DD EEEE";
			NSString *endDateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.endDate]];
			NSString *todayString = [dateFormatter stringFromDate:[NSDate date]];
			
            if ( [endDateString compare:todayString] == NSOrderedAscending) {
                budget.state = STATE_EXPIRED;
                [[SQLiteManager database] updateBudget:budget];
                NSDateComponents* startDateComp = [[NSCalendar currentCalendar]
                                                   components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                                   fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.startDate]];
                NSDateComponents* endDateComp = [[NSCalendar currentCalendar]
                                                   components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                                   fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:budget.endDate]];
                NSString* message = [NSString stringWithFormat:@"Ngày bắt đầu = %ld/%ld/%ld\nNgày kết thúc = %ld/%ld/%ld",
                                     (long)startDateComp.day, (long)startDateComp.month, (long)startDateComp.year,
                                     (long)endDateComp.day, (long)endDateComp.month, (long)endDateComp.year];
                
                NSString* title = [NSString stringWithFormat:@"Ngân sách kết thúc: %@", budget.category.name];
                [[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
            }
        }
    }

}

-(void)checkForBudgetWarning {
	
	NSMutableArray *runningBudget = [self getUnexpiredBudget];
	
    for (int i = 0; i < runningBudget.count; i++) {
        Budget* budget = (Budget*)[runningBudget objectAtIndex:i];
        if (llabs(budget.currentAmount) > llabs(budget.maximumAmount)) {
					NSString* message = [NSString stringWithFormat:@"Giới hạn ngân sách = %@\nĐã tiêu = %@", [MoneyStringFormatter stringMoneyWithLongLong:llabs(budget.maximumAmount)], [MoneyStringFormatter stringMoneyWithLongLong:llabs(budget.currentAmount)]];
            NSString* title = [NSString stringWithFormat:@"Vượt ngân sách: %@ ", budget.category.name];
            [[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
        }
    }
}

-(NSMutableArray*)getUnexpiredBudget {
    NSMutableArray* array = [[NSMutableArray alloc] init];
    for (Budget* budget in self.budgetList) {
        if (budget.state != STATE_EXPIRED ) {
            [array addObject:budget];
        }
    }
    return array;
}

-(NSMutableArray*)getExpiredBudget {
    NSMutableArray* array = [[NSMutableArray alloc] init];
    for (Budget* budget in self.budgetList) {
        if (budget.state == STATE_EXPIRED) {
            [array addObject:budget];
        }
    }
    return array;
}

@end