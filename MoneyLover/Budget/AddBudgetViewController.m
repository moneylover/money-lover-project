//
//  AddBudgetViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "AddBudgetViewController.h"
#import "Calendar.h"
#import "ChooseCategoryViewController.h"
#import "CategoryTransaction.h"
#import "BudgetManager.h"
#import "SQLiteManager.h"


@interface AddBudgetViewController () <ChooseCategoryProtocol, CalendarDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *icon_imageView;
@property (weak, nonatomic) IBOutlet UIButton *nameButton;
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;
@property (weak, nonatomic) IBOutlet UIButton *startDateButton;
@property (weak, nonatomic) IBOutlet UIButton *endDateButton;

@property (assign, nonatomic) NSInteger startDate;
@property (assign, nonatomic) NSInteger endDate;
//@property (assign, nonatomic) NSInteger selectedCategoryID;

@property (assign, nonatomic) NSInteger chooseDate_flag; // 0 1

@property (strong, nonatomic) CategoryTransaction *selectedCategory;

@end

@implementation AddBudgetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"EEEE dd-MM-YYYY";

	if(self.editedBudget != nil){
		
		self.icon_imageView.image = [UIImage imageNamed:self.editedBudget.category.iconName];
		self.moneyTextField.text = [NSString stringWithFormat:@"%lld", llabs(self.editedBudget.maximumAmount)];
		[self.nameButton setTitle:self.editedBudget.category.name forState:UIControlStateNormal];
		self.selectedCategory = self.editedBudget.category;
		
		[self.startDateButton setTitle:[dateFormatter
																		stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.editedBudget.startDate]]
													forState:UIControlStateNormal];
		self.startDate = self.editedBudget.startDate;
		
		
		[self.endDateButton setTitle:[dateFormatter
																	stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.editedBudget.endDate]]
												forState:UIControlStateNormal];
		
		self.endDate = self.editedBudget.endDate;
		
	}else{
		
		NSDate *date = [NSDate date];
		
		[self.startDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
		self.startDate = [date timeIntervalSinceReferenceDate];
		
		[self.endDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
		self.endDate = [date timeIntervalSinceReferenceDate];
		
	}
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction

- (IBAction)chooseCategoryTransaction:(id)sender {
	
	[self.moneyTextField resignFirstResponder];
	
	ChooseCategoryViewController *chooseCategoryVC = [[ChooseCategoryViewController alloc] initWithNibName:@"ChooseCategoryViewController" bundle:nil];
	chooseCategoryVC.delegate = self;
	
	[self.navigationController pushViewController:chooseCategoryVC animated:YES];
	
}

- (IBAction)chooseStartDate:(id)sender {
	
	[self.moneyTextField resignFirstResponder];
	
	[[Calendar getInstanceWithParentViewController:self] showCalendar];
	
	_chooseDate_flag = 0;
	
}

- (IBAction)chooseEndDate:(id)sender {
	
	[self.moneyTextField resignFirstResponder];
	
	[[Calendar getInstanceWithParentViewController:self] showCalendar];
	
	_chooseDate_flag = 1;
}

- (IBAction)cancel:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

- (IBAction)complete:(id)sender {
	
	if([self.nameButton.titleLabel.text isEqualToString:@"Chọn nhóm"]){
		UIAlertView *alertView = [[UIAlertView alloc]
															initWithTitle:nil
															message:@"Bạn cần chọn nhóm giao dịch"
															delegate:self
															cancelButtonTitle:nil
															otherButtonTitles:@"Đóng", nil];
		[alertView show];
		return;
	}
	

	if([self.moneyTextField.text longLongValue] == 0){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
																													message:@"Bạn cần nhập số tiền cho ngân sách này"
																												 delegate:self
																								cancelButtonTitle:@"Đóng"
																								otherButtonTitles:nil];
		[invalidName show];
		return;
	}
	
	/*
	NSTimeInterval toDay = (NSTimeInterval)[NSDate date].timeIntervalSinceReferenceDate;
	
	if(self.startDate < toDay){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
																													message:@"Ngày bắt đầu của bạn không hợp lệ"
																												 delegate:self
																								cancelButtonTitle:@"Đóng"
																								otherButtonTitles:nil];
		[invalidName show];
		return;
	}
	*/
	
	if(self.startDate > self.endDate){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
															  message:@"Ngày bắt đầu hoặc ngày kết thúc của bạn không hợp lệ"
															 delegate:self
													cancelButtonTitle:@"Đóng"
													otherButtonTitles:nil];
		[invalidName show];
		return;
	}
	
	if([[SQLiteManager database] categoryWithIdIsEarning:self.selectedCategory.Id]){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
															  message:@"Ngân sách chỉ áp dụng cho các khoản chi"
															 delegate:self
													cancelButtonTitle:@"Đóng"
													otherButtonTitles:nil];
		[invalidName show];
		return;
	}
	
	
	if(self.editedBudget == nil){
		
			[[BudgetManager getInstance] createBudgetForCategory:self.selectedCategory withStartDate:self.startDate withEndDate:self.endDate withMaximumAmount:[self.moneyTextField.text longLongValue]*-1];
		
//			[self createNotification];
		
	}else{
		
			self.editedBudget.category = self.selectedCategory;
			self.editedBudget.maximumAmount = [self.moneyTextField.text longLongValue]*-1;
			self.editedBudget.startDate = self.startDate;
			self.editedBudget.endDate = self.endDate;
//			self.editedBudget.endDate = self.endDate;
//			[self cancleNotification];
		
			[[BudgetManager getInstance] updateBudget:self.editedBudget];

	}

	[self.delegate completeAddingBudget];
	
	[self.navigationController popViewControllerAnimated:YES];
	
}


- (void)createNotification{
	
	// Tạo thông báo
	UILocalNotification *localNotification = [[UILocalNotification alloc] init];
	localNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.startDate + 28800];
	localNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
	NSString *message = [NSString stringWithFormat:@"Ngân sách %@ được bắt đầu vào ngày hôm nay", self.selectedCategory.name];
	localNotification.alertBody = NSLocalizedString(message, nil);
	localNotification.hasAction = YES;
	localNotification.alertAction = NSLocalizedString(@"View", nil);
	localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
	localNotification.userInfo = @{@"Start_Budget" : [NSString stringWithFormat:@"%@", self.selectedCategory.name]};
	localNotification.soundName = UILocalNotificationDefaultSoundName;
	/* Schedule the notification */
	[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
	
	
	UILocalNotification *endDocalNotification = [[UILocalNotification alloc] init];
	endDocalNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.endDate + 28800];
	endDocalNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
	NSString *endingMessage = [NSString stringWithFormat:@"Ngân sách %@ sẽ kết thúc trong ngày hôm nay", self.selectedCategory.name];
	endDocalNotification.alertBody = NSLocalizedString(endingMessage, nil);
	endDocalNotification.hasAction = YES;
	endDocalNotification.alertAction = NSLocalizedString(@"View", nil);
	endDocalNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
	endDocalNotification.userInfo = @{@"End_Budget" : [NSString stringWithFormat:@"%@", self.selectedCategory.name]};
	endDocalNotification.soundName = UILocalNotificationDefaultSoundName;
	/* Schedule the notification */
	[[UIApplication sharedApplication] scheduleLocalNotification:endDocalNotification];
}

- (void)cancleNotification{
	
	NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
	//		NSLog(@" updateSaving Notification array: %d", [localNotifications count]);
	
	for (UILocalNotification *localNotification in localNotifications){
		//			NSLog(@"Local Notification:\n%@", localNotification);
		NSDictionary *userInfo = localNotification.userInfo;
		
		NSString *localNotificationWithBudgetName = [userInfo valueForKey:@"Start_Budget"];
		
		if([localNotificationWithBudgetName isEqualToString:self.editedBudget.category.name]){
			localNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate: self.startDate];
			localNotification.userInfo  = @{@"Start_Budget": [NSString stringWithFormat:@"%@", self.editedBudget.category.name]};
//			break;
		}
		
		NSString *endLocalNotificationWithBudgetName = [userInfo valueForKey:@"End_Budget"];
		
		if([endLocalNotificationWithBudgetName isEqualToString:self.editedBudget.category.name]){
			localNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate: self.endDate];
			localNotification.userInfo  = @{@"End_Budget": [NSString stringWithFormat:@"%@", self.editedBudget.category.name]};
//			break;
		}
	}
	
}


#pragma mark - ChooseCategoryProtocol

- (void)selectedCategoryTransaction:(CategoryTransaction *)categoryTransaction{
	
	self.selectedCategory = categoryTransaction;
	
	self.icon_imageView.image = [UIImage imageNamed:categoryTransaction.iconName];
	
	[self.nameButton setTitle:categoryTransaction.name forState:UIControlStateNormal];
	
}

#pragma mark - CalendarDelegate

- (void)selectedDate:(NSDate *)date{
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"EEEE dd-MM-YYYY";
	
	if(_chooseDate_flag == 0){
		
		[self.startDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
		
		self.startDate = [date timeIntervalSinceReferenceDate];
		
	}else{
		
		if(_chooseDate_flag == 1){
			
			[self.endDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
			
			self.endDate = [date timeIntervalSinceReferenceDate];
		}
		
	}
	
}





@end










