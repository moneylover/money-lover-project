//
//  Budget.h
//  MoneyLover
//

//  Created by Ngo Huu Tuan on 11/14/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef MoneyLover_Budget_h
#define MoneyLover_Budget_h

enum BudgetState {
	STATE_NOT_BEGIN,
	STATE_BEGAN,
	STATE_EXPIRED
};

@class CategoryTransaction;


@interface Budget : NSObject

@property (assign, nonatomic) NSInteger budgetId;
@property (assign, nonatomic) NSInteger startDate;
@property (assign, nonatomic) NSInteger endDate;
@property (assign, nonatomic) long long currentAmount;
@property (assign, nonatomic) long long maximumAmount;
@property (strong, nonatomic) CategoryTransaction* category;
@property (assign, nonatomic) NSInteger state;

@end

#endif

