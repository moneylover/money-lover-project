//
//  OptionsCellCategoryTransaction.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/22/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "OptionsCellCategoryTransaction.h"

@implementation OptionsCellCategoryTransaction

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editAction:(id)sender {
	[self.delegate editCategoryTransaction];
}

- (IBAction)deleteAction:(id)sender {
	[self.delegate deleteCategoryTransaction];
}

@end
