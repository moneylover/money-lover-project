//
//  GroupViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/18/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface CategoryTransactionViewController : BaseViewController 
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@property (strong, nonatomic) NSMutableArray *earningCategoryTransactions;
@property (strong, nonatomic) NSMutableArray *spendingCategoryTransactions;
@end
