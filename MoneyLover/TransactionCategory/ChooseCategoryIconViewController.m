//
//  ChooseCategoryIconViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/21/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ChooseCategoryIconViewController.h"
#import "CustomCollectionViewCell.h"

@interface ChooseCategoryIconViewController ()

@end

@implementation ChooseCategoryIconViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.s
	UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
	flowLayout.minimumLineSpacing = 10;
	flowLayout.minimumInteritemSpacing = 10;
	flowLayout.itemSize = CGSizeMake(50.0f, 50.0f);
	flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
	flowLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 10.0f);
	
//	[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CollectionViewCellIdentification"];
	[self.collectionView registerNib:[UINib nibWithNibName:@"CustomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCellIdentifier"];
	self.collectionView.delegate = self;
	self.collectionView.dataSource = self;
	self.collectionView.collectionViewLayout = flowLayout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return 123;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	CustomCollectionViewCell *cell = (CustomCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCellIdentifier" forIndexPath:indexPath];

	cell.backgroundColor = [UIColor clearColor];
	cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%lu.png",(unsigned long)(indexPath.row + 1)]];
	return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	self.selectedIconName = [NSString stringWithFormat:@"icon_%ld.png",(long) (indexPath.row + 1)];
	[self.delegate getIconName:self.selectedIconName];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
	NSLog(@"Choose image dealloc");
}
@end
