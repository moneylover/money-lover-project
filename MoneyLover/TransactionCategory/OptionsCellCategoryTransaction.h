//
//  OptionsCellCategoryTransaction.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/22/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryTransaction;

@protocol OptionsCellCategoryTransactionProtocol
@required
- (void)editCategoryTransaction;
- (void)deleteCategoryTransaction;
@end

@interface OptionsCellCategoryTransaction : UITableViewCell

@property (weak, nonatomic) id <OptionsCellCategoryTransactionProtocol> delegate;

@end
