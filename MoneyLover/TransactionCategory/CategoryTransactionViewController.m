//
//  GroupViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/18/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "CategoryTransactionViewController.h"
#import "SQLiteManager.h"
#import "CategoryTransaction.h"
#import "AddCategoryViewController.h"
#import "OptionsCellCategoryTransaction.h"


@interface CategoryTransactionViewController () <UITableViewDataSource, UITableViewDelegate, AddCategoryVCProtocol, OptionsCellCategoryTransactionProtocol, UIAlertViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSIndexPath *showingOptionCellIndex;
//@property (assign, nonatomic) BOOL showingOptionCell;
@property (assign, nonatomic) CGFloat heightForCells;


@end

@implementation CategoryTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	[self.tableView registerNib:[UINib nibWithNibName:@"OptionsCellCategoryTransaction" bundle:nil]  forCellReuseIdentifier:@"OptionsCellCategoryIdentifier"];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
	self.tableView.sectionFooterHeight = 0;

	self.earningCategoryTransactions = [[SQLiteManager database] getCategorysTransactionIsEarningType:YES];
	self.spendingCategoryTransactions = [[SQLiteManager database] getCategorysTransactionIsEarningType:NO];
	
	self.headerView.hidden = YES;
	self.heightForCells = 50.0f;
	self.showingOptionCellIndex = nil;
//	self.showingOptionCell = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showSlideMenu:(id)sender {
  [self.delegate showSlideMenu];
}

- (IBAction)goToAddCategoryTransactionViewController:(id)sender {
	AddCategoryViewController *addCategoryVC = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
	addCategoryVC.delegate = self;
	[self.navigationController pushViewController:addCategoryVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(section == 0){
		return [self.spendingCategoryTransactions count];
	}else{
		return [self.earningCategoryTransactions count];
	}
  return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 2;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	UITableViewCell *cell;
	
	if(self.showingOptionCellIndex != nil && [self.showingOptionCellIndex isEqual:indexPath]){
		cell = [tableView dequeueReusableCellWithIdentifier:@"OptionsCellCategoryIdentifier"];
		((OptionsCellCategoryTransaction*)cell).delegate = self;
	}else{
		cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
		
		CategoryTransaction *aCategoryTransaction;
		if(indexPath.section == 0){
			aCategoryTransaction = (CategoryTransaction*)self.spendingCategoryTransactions[indexPath.row];
		}else{
			aCategoryTransaction = (CategoryTransaction*)self.earningCategoryTransactions[indexPath.row];
		}
		
		cell.imageView.image = [UIImage imageNamed:aCategoryTransaction.iconName];
		cell.textLabel.text = aCategoryTransaction.name;
		cell.textLabel.textColor = [UIColor grayColor];

		if([self isLockedCellIndexPathRow:aCategoryTransaction.Id]){

			UIImageView *lockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
			lockImageView.image = [UIImage imageNamed:@"ic_category_locked.png"];
			cell.accessoryView =  lockImageView;
		}else{
			cell.accessoryView = nil;
		}
	}
	
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return self.heightForCells;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return self.heightForCells;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

	UIView *header = [[UIView alloc] init];
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(6.0, 15.0, 100.0, 20.0f)];
	switch(section){
			case 0:
		{
			label.text = @"Chi tiêu";
			label.textColor = [UIColor redColor];
		}
			break;
			case 1:
		{
			label.text = @"Thu nhập";
			label.textColor = [UIColor blueColor];
		}
			break;
		default:
			break;
	}
	[label sizeToFit];
	[header addSubview:label];
	return header;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	CGFloat header_0_positionY = scrollView.contentOffset.y;
	if(header_0_positionY < 0){
		self.headerView.hidden = YES;
	}else{
		self.headerView.hidden = NO;
		if(header_0_positionY > (self.heightForCells*([self.spendingCategoryTransactions count] + 1))){
			self.headerLabel.text = @"Thu nhập";
			self.headerLabel.textColor = [UIColor blueColor];
		}else{
			self.headerLabel.text = @"Chi tiêu";
			self.headerLabel.textColor = [UIColor redColor];
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	NSInteger selectedCategoryId;
	if(indexPath.section == 1){
		selectedCategoryId = ((CategoryTransaction*)self.earningCategoryTransactions[indexPath.row]).Id;
	}else{
		selectedCategoryId = ((CategoryTransaction*)self.spendingCategoryTransactions[indexPath.row]).Id;
	}
	if([self isLockedCellIndexPathRow:selectedCategoryId]){
		return;
	}
	
	NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:indexPath.section];
	CategoryTransaction *categoryTransactionTemplate = [[CategoryTransaction alloc] init];

	if(self.showingOptionCellIndex == nil){
		if(indexPath.section == 1){
			[self.earningCategoryTransactions insertObject:categoryTransactionTemplate atIndex:newIndexPath.row];
		}else{
			[self.spendingCategoryTransactions insertObject:categoryTransactionTemplate atIndex:newIndexPath.row];
		}

		self.showingOptionCellIndex = newIndexPath;
		[tableView beginUpdates];
		[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
		[tableView endUpdates];
	}else{

		if(self.showingOptionCellIndex.section == 1){
			[self.earningCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
		}else{
			[self.spendingCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
		}
		[tableView beginUpdates];
		[tableView deleteRowsAtIndexPaths:@[self.showingOptionCellIndex] withRowAnimation:UITableViewRowAnimationBottom];
		[tableView endUpdates];
		
		if([self.showingOptionCellIndex isEqual:newIndexPath]){
			self.showingOptionCellIndex = nil;
		}else{
			if(self.showingOptionCellIndex.row < newIndexPath.row){
				newIndexPath = indexPath;
			}
			if(indexPath.section == 1){
				[self.earningCategoryTransactions insertObject:categoryTransactionTemplate atIndex:newIndexPath.row];
			}else{
				[self.spendingCategoryTransactions insertObject:categoryTransactionTemplate atIndex:newIndexPath.row];
			}
			self.showingOptionCellIndex = newIndexPath;
			[tableView beginUpdates];
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
			[tableView endUpdates];
		}
	}

}


// AddCategoryVCProtocol
- (void)addCategoryTransaction:(CategoryTransaction*)categoryTransaction{
	if(categoryTransaction.isEarning == YES){
		[self.earningCategoryTransactions addObject:categoryTransaction];
	}else{
		[self.spendingCategoryTransactions addObject:categoryTransaction];
	}
	[self.tableView reloadData];
}

- (void)updateCategoryTransaction:(CategoryTransaction*)categoryTransaction atIndex:(NSIndexPath*) indexPath{
	NSInteger  deletedCategoryTransactionId;
	
	if(indexPath.section == 0){
			deletedCategoryTransactionId = ((CategoryTransaction*)self.spendingCategoryTransactions[indexPath.row]).Id;
	}else{
			deletedCategoryTransactionId = ((CategoryTransaction*)self.earningCategoryTransactions[indexPath.row]).Id;
	}
	// Change type from Earning to Spending or otherwise
	if(categoryTransaction.isEarning == indexPath.section){
		if(indexPath.section == 0){
			[self.spendingCategoryTransactions replaceObjectAtIndex:indexPath.row withObject:categoryTransaction];
		}else{
			[self.earningCategoryTransactions	replaceObjectAtIndex:indexPath.row withObject:categoryTransaction];
		}
//		categoryTransaction.Id = deletedCategoryTransactionId;
//		[[SQLiteManager database] deleteCategoryTransactionWithId:deletedCategoryTransactionId];
//		[[SQLiteManager database] updateCategoryTransaction:categoryTransaction];
	}else{
		if(indexPath.section == 0){

			[self.spendingCategoryTransactions removeObjectAtIndex:indexPath.row];
			[self.earningCategoryTransactions	addObject:categoryTransaction];
			[self.tableView beginUpdates];
			[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
			[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:([self.earningCategoryTransactions count] - 1) inSection:1]] withRowAnimation:UITableViewRowAnimationBottom];
			[self.tableView endUpdates];

		}else{

			[self.earningCategoryTransactions	removeObjectAtIndex:indexPath.row];
			[self.spendingCategoryTransactions addObject:categoryTransaction];
			[self.tableView beginUpdates];
			[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
			[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:([self.spendingCategoryTransactions count] - 1) inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
			[self.tableView endUpdates];
		}
//		[[SQLiteManager database] deleteCategoryTransactionWithId:deletedCategoryTransactionId];
//		[[SQLiteManager database] insertCategoryTransaction:categoryTransaction];
//		[[SQLiteManager database] updateCategoryTransaction:categoryTransaction];
	}
	
	if([[SQLiteManager database] categoryWithIdIsEarning:categoryTransaction.Id] != categoryTransaction.isEarning){
		long long changeMoney = [[SQLiteManager database]
														 getTotalMoneyWithCategoryTransactionId:categoryTransaction.Id];
		long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		amountOfMoney += changeMoney*-2;
		[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:amountOfMoney] doubleValue] forKey:@"AmountOfMoney"];
	}
	
	
	
	[[SQLiteManager database] updateCategoryTransaction:categoryTransaction];
	
	[self.tableView reloadData];
}


// OptionsCellCategoryTransactionProtocol
- (void)editCategoryTransaction{

	// Hide cell option
	if(self.showingOptionCellIndex.section == 1){
		[self.earningCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
	}else{
		[self.spendingCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
	}
	
	[self.tableView beginUpdates];
	[self.tableView deleteRowsAtIndexPaths:@[self.showingOptionCellIndex] withRowAnimation:UITableViewRowAnimationBottom];
	[self.tableView endUpdates];

	
	NSIndexPath *updatedCellIndexPath = [NSIndexPath indexPathForRow:(self.showingOptionCellIndex.row - 1) inSection:self.showingOptionCellIndex.section];
	CategoryTransaction *aCategoryTransaction;
	if(self.showingOptionCellIndex.section == 0){
		aCategoryTransaction = (CategoryTransaction*)self.spendingCategoryTransactions[updatedCellIndexPath.row];
	}else{
		aCategoryTransaction = (CategoryTransaction*)self.earningCategoryTransactions[updatedCellIndexPath.row];
	}

	AddCategoryViewController *addCategoryVC = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
//	[addCategoryVC viewDidLoad];
	addCategoryVC.updatedCategoryTransaction = aCategoryTransaction;
	addCategoryVC.delegate = self;
	addCategoryVC.indexPathForUpdateCategory = updatedCellIndexPath;
	
	self.showingOptionCellIndex = nil;
	[self.navigationController pushViewController:addCategoryVC animated:YES];
}

- (void)deleteCategoryTransaction{

	UIAlertView *messageAlertDeletion = [[UIAlertView alloc]
																					initWithTitle:@""
																								message:@"Mọi giao dịch thuộc nhóm này sẽ bị xoá, bạn có chắc chắn xoá nhóm này không?"
																							 delegate:self
																			cancelButtonTitle:@"Cancel"
																			otherButtonTitles:@"Ok", nil];
	[messageAlertDeletion show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 0){
		NSLog(@"CategoryTransactionVC cancle delete categroy transaction");
	}else{
//		NSLog(@"CategoryTransactionVC delete categroy transaction");
		NSInteger categoryTransactionId;
		NSIndexPath *deletedCellIndexPath = [NSIndexPath indexPathForRow:(self.showingOptionCellIndex.row - 1) inSection:self.showingOptionCellIndex.section];
		if(self.showingOptionCellIndex.section == 1){
			categoryTransactionId = ((CategoryTransaction*)self.earningCategoryTransactions[deletedCellIndexPath.row]).Id;
			[self.earningCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
			[self.earningCategoryTransactions removeObjectAtIndex:deletedCellIndexPath.row];
			
		}else{
			categoryTransactionId = ((CategoryTransaction*)self.spendingCategoryTransactions[deletedCellIndexPath.row]).Id;
			[self.spendingCategoryTransactions removeObjectAtIndex:self.showingOptionCellIndex.row];
			[self.spendingCategoryTransactions removeObjectAtIndex:deletedCellIndexPath.row];
		}
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:@[self.showingOptionCellIndex] withRowAnimation:UITableViewRowAnimationBottom];
		[self.tableView deleteRowsAtIndexPaths:@[deletedCellIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
		[self.tableView endUpdates];
		self.showingOptionCellIndex = nil;
		
		[[SQLiteManager database] deleteCategoryTransactionWithId:categoryTransactionId];
	}
}

- (BOOL)isLockedCellIndexPathRow:(NSInteger)row{
	int lockedCell[5] = {11, 12, 16, 18, 19};
	for(int i = 0; i < 5; i++){
		if(row == lockedCell[i]){
			return YES;
		}
	}
	return NO;
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
//	NSLog(@"hihi");
//	return YES;
//}

@end
