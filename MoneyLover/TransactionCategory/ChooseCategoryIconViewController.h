//
//  ChooseCategoryIconViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/21/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseCategoryIconProtocol
- (void)getIconName:(NSString*)iconName;
@end

@interface ChooseCategoryIconViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) id <ChooseCategoryIconProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *theTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSString *selectedIconName;
@end
