//
//  AddCategoryViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/20/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "AddCategoryViewController.h"
#import "CategoryTransaction.h"
#import "ChooseCategoryIconViewController.h"
#import "SQLiteManager.h"

@interface AddCategoryViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate, ChooseCategoryIconProtocol>


@end

@implementation AddCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//	NSLog(@"viewDidLoad");
	self.countClickButton = 0;
//	self.iconName = nil;
	self.isUpdateCategoryTransaction = NO;
	
	self.textFieldForName.delegate = self;
	UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGes:)];
	tapGes.delegate = self;
//	[self.view addGestureRecognizer:tapGes];
	
	UITapGestureRecognizer *tapGes_iamgeview = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGes:)];
	tapGes_iamgeview.delegate = self;
	[self.categoryImageView addGestureRecognizer:tapGes_iamgeview];
	
	if(self.updatedCategoryTransaction != nil){
		self.categoryImageView.image = [UIImage imageNamed:self.updatedCategoryTransaction.iconName];
		self.textFieldForName.text = self.updatedCategoryTransaction.name;
		[self.categoryTypeButton setTitle:self.updatedCategoryTransaction.isEarning?@"Thu nhập":@"Chi tiêu" forState: UIControlStateNormal];
		[self.categoryTypeButton setTitleColor:(self.updatedCategoryTransaction.isEarning?[UIColor blueColor]:[UIColor redColor]) forState:UIControlStateNormal];
		self.countClickButton = self.updatedCategoryTransaction.isEarning?1:2;
		self.isUpdateCategoryTransaction = YES;
		self.iconName = self.updatedCategoryTransaction.iconName;
	}
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelAction:(id)sender {
	self.textFieldForName = nil;
	self.categoryImageView = nil;
	self.iconName = nil;
	self.delegate = nil;
	[self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkingValidityForInputValues{
	if(self.iconName == nil ){
		UIAlertView *missIconAlertView = [[UIAlertView alloc] initWithTitle:nil
																																message:@"Đừng quên hình minh hoạ"
																															 delegate:self
																											cancelButtonTitle:@"Đóng"
																											otherButtonTitles:nil];
		[missIconAlertView show];
		return NO;
	}

	NSString *removeWhiteSpaceString = [self.textFieldForName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if(self.isUpdateCategoryTransaction == YES){
		if([self.updatedCategoryTransaction.name isEqualToString:removeWhiteSpaceString]){
			return YES;
		}
	}

	if([removeWhiteSpaceString isEqualToString:@""]){
		UIAlertView *invalidIconName = [[UIAlertView alloc] initWithTitle:nil
																															message:@"Bạn cần nhập tên nhóm"
																														 delegate:self
																										cancelButtonTitle:@"Đóng"
																										otherButtonTitles:nil];
		[invalidIconName show];
		return NO;
	}
	BOOL isEarning = self.countClickButton%2;
	if([[SQLiteManager database] checkForExistingOfCategoryName:self.textFieldForName.text andTypeTransaction:isEarning]){
		UIAlertView *existingNameAlertView = [[UIAlertView alloc] initWithTitle:nil
																																		message:@"Tên nhóm này đã tồn tại"
																																	 delegate:self
																													cancelButtonTitle:@"Đóng"
																													otherButtonTitles:nil];
		[existingNameAlertView show];
		return NO;
	}
	
	if(self.countClickButton == 0){
		UIAlertView * notChooseCategoryMessage= [[UIAlertView alloc] initWithTitle:nil
																																			 message:@"Bạn cần chọn loại giao dịch"
																																			delegate:self
																														 cancelButtonTitle:@"Đóng"
																														 otherButtonTitles:nil];
		[notChooseCategoryMessage show];
		return NO;
	}
	
	return YES;
}

- (IBAction)completeAction:(id)sender {

	if([self checkingValidityForInputValues]){
		CategoryTransaction *categoryTransaction = [[CategoryTransaction alloc] init];
		categoryTransaction.name = [self.textFieldForName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
		categoryTransaction.isEarning = self.countClickButton%2;
		categoryTransaction.iconName = self.iconName;
		
//		NSLog(@"Name %@ %@", categoryTransaction.iconName, self.iconName);
		
		if(self.isUpdateCategoryTransaction == NO){
			[[SQLiteManager database] insertCategoryTransaction:categoryTransaction];
			[self.delegate addCategoryTransaction:categoryTransaction];
		}else{
			categoryTransaction.Id = self.updatedCategoryTransaction.Id;
//			[[SQLiteManager database] updateCategoryTransaction:categoryTransaction];
			[self.delegate updateCategoryTransaction:categoryTransaction atIndex:self.indexPathForUpdateCategory];
		}
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (IBAction)changeCategoryType:(id)sender {
	
	UIButton *button = ((UIButton*)sender);
	if((self.countClickButton % 2) == 1){
		[button setTitle:@"Chi tiêu" forState:UIControlStateNormal];
		[button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
	}else{
		[button setTitle:@"Thu nhập" forState:UIControlStateNormal];
		[button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
	}
	self.self.countClickButton++;
}

- (void)handleTapGes:(id)sender{
	
	if(((UITapGestureRecognizer*)sender).view == self.categoryImageView){
		ChooseCategoryIconViewController *chooseCategoryIconVC = [[ChooseCategoryIconViewController alloc] initWithNibName:@"ChooseCategoryIconViewController" bundle:nil];
		chooseCategoryIconVC.delegate = self;
		
		[self.navigationController pushViewController:chooseCategoryIconVC animated:YES];
	}else{
		[self.textFieldForName resignFirstResponder];
	}
}

// ChooseCategoryIconProtocol
- (void)getIconName:(NSString *)iconName{

	self.iconName = iconName;
	UIImage *newImage = [UIImage imageNamed:iconName];
	self.categoryImageView.image = newImage;
	[self.categoryImageView setImage:newImage];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}

- (void)dealloc{
	NSLog(@"add category view controller dealloc");
}
@end
