//
//  Group.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/20/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#ifndef MoneyLover_Group_h
#define MoneyLover_Group_h

#import "Foundation/Foundation.h"

@interface CategoryTransaction: NSObject

@property (assign, nonatomic) NSInteger Id;
@property (copy, nonatomic) NSString *name;
@property (assign, nonatomic) BOOL isEarning;
@property (copy, nonatomic) NSString *iconName;

@end

#endif
