//
//  AddCategoryViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/20/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryTransaction;

@protocol AddCategoryVCProtocol // <NSObject>

@required
- (void)addCategoryTransaction:(CategoryTransaction*)categoryTransaction;
- (void)updateCategoryTransaction:(CategoryTransaction*)categoryTransaction atIndex:(NSIndexPath*) indexPath;
@end


@interface AddCategoryViewController : UIViewController

@property (weak, nonatomic) id <AddCategoryVCProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldForName;
@property (weak, nonatomic) IBOutlet UIButton *categoryTypeButton;

@property (assign, nonatomic) NSInteger countClickButton;
@property (nonatomic, copy) NSString *iconName;
@property (assign, nonatomic) BOOL isUpdateCategoryTransaction;
@property (strong, nonatomic) NSIndexPath *indexPathForUpdateCategory;
@property (strong, nonatomic) CategoryTransaction *updatedCategoryTransaction;


@end
