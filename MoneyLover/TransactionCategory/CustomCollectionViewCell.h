//
//  CustomCollectionViewCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/21/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
