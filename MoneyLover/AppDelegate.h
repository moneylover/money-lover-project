//
//  AppDelegate.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/16/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

