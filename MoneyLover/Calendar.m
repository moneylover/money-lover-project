//
//  Calendar.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "Calendar.h"

@interface Calendar () <CKCalendarDelegate, UIGestureRecognizerDelegate>

//@property (strong, nonatomic) UIView *calendarViewBackground;

@end


@implementation Calendar


+ (Calendar*)getInstanceWithParentViewController:(id <CalendarDelegate>)parentViewController{
	static Calendar *_calendarInstance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_calendarInstance = [[Calendar alloc] initPrivate];
	});
//	_calendarInstance.view.hidden = YES;
	_calendarInstance.view.alpha = 0;
	_calendarInstance.delegate = parentViewController;
//	[((UIViewController*)parentViewController).view addSubview:_calendarInstance.view];
	[[[[UIApplication sharedApplication] delegate] window] addSubview:_calendarInstance.view];
	return _calendarInstance;
}

- (id)initPrivate{
	self = [super init];
	if(self){
		
//		self.calendarViewBackground = [[UIView alloc] initWithFrame:self.view.frame];
//		self.calendarViewBackground.backgroundColor = [UIColor clearColor];
//		self.calendarViewBackground.alpha = 0;
		self.view.backgroundColor = [UIColor clearColor];
		self.view.alpha = 0;
		
		UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCalendar:)];
		[self.view addGestureRecognizer:tapGes];
		
		UIView *opaqueView = [[UIView alloc] initWithFrame:self.view.frame];
		opaqueView.backgroundColor = [UIColor blackColor];
		opaqueView.alpha = 0.8;
		[self.view addSubview:opaqueView];
		
		self->_ckCalendarView = [[CKCalendarView alloc] initWithStartDay:startMonday];
		self->_ckCalendarView.delegate = self;
		self->_ckCalendarView.center = [self.view center];
		
		[self.view addSubview:self->_ckCalendarView];
	}
	return self;
}

- (id)init{
	[self doesNotRecognizeSelector:_cmd];
	return  nil;
}

- (void)viewDidLoad{
	[super viewDidLoad];
}

- (void)didReceiveMemoryWarning{
	 [super didReceiveMemoryWarning];
}

- (void)hideCalendar:(id)sender{
	[self hideCalendar];
}

- (void)showCalendar{
	[UIView animateWithDuration:0.5 animations:^{
		self.view.alpha = 1;
	} completion:^(BOOL finished) {
//		self.view.hidden = NO;
	}];
}

- (void)hideCalendar{
	[UIView animateWithDuration:0.5 animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
//		self.view.hidden = YES;
		dispatch_async(dispatch_get_main_queue(), ^{

			[self.view removeFromSuperview];
		});

	}];
}



- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date{
	if(date == nil){
		date = [NSDate date];
	}
	[self hideCalendar];
	[self.delegate selectedDate:date];
}

- (void)dealloc{
	// Never call because it is singleton class
	NSLog(@"Calendar delloc");
}
@end
