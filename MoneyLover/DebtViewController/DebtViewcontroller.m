//
//  DebtViewcontroller.m
//  MoneyLover
//
//  Created by Giap Nguyen on 11/16/15.
//  Copyright © 2015 Ngo Huu Tuan. All rights reserved.
//

#import "DebtViewcontroller.h"
#import "Saving.h"
#import "SavingCell.h"
#import "SQLiteManager.h"
#import "AddSavingViewController.h"
#import "ExpriedSavingCell.h"
#import "FPPopoverController.h"
#import "OptionTableForSavingCellViewController.h"
#import "OptionTableViewController.h"
#import "SavingManager.h"
#import "DetailSavingTableView.h"
#import "AddDebtViewController.h"
#import "DebtCell.h"
#import "DetailDebtViewController.h"
#import "DetailDebtCell.h"
#import "Transaction.h"
#import "DataSourceInASection.h"
#import "TransactionInterestRateManager.h"


@interface DebtViewcontroller ()<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, OptionTableForSavingCellViewControllerDelegate, UIAlertViewDelegate,AddDebtDelegate, DebtCellDelegate>


@property (weak, nonatomic) IBOutlet UIButton *runingButton;
@property (weak, nonatomic) IBOutlet UIButton *finishedButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UITableView *runningDebtTableView;
@property (strong, nonatomic) UITableView *expriedDebtTableView;

@property (strong, nonatomic) NSMutableArray *runningDebtArray;
@property (strong, nonatomic) NSMutableArray *expriedDebtArray;
@property (strong, nonatomic) NSMutableArray *moneyArrayWhenSavingsExpried;

@property (strong, nonatomic) FPPopoverController *popoverViewController;
@property (strong, nonatomic) Transaction *selectedDebt;
@property (strong, nonatomic) FPPopoverController *showDetailPopoverViewController;

@property (strong, nonatomic) UILabel *emptyDataLabel_left;
@property (strong, nonatomic) UILabel *emptyDataLabel_right;

@end

@implementation DebtViewcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    CGFloat tableViewWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat tableViewHeight = [UIScreen mainScreen].bounds.size.height - 108;
    
    [self.scrollView setContentSize:CGSizeMake(tableViewWidth*2, tableViewHeight)];
    self.scrollView.delegate = self;
    
	self.emptyDataLabel_right = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width + 70, tableViewHeight/2, 100, 30)];
	self.emptyDataLabel_right.text = @"Không có dữ liệu";
	[self.emptyDataLabel_right setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel_right setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel_right sizeToFit];
	[self.scrollView addSubview:self.emptyDataLabel_right];
	
	self.emptyDataLabel_left = [[UILabel alloc] initWithFrame:CGRectMake(70, tableViewHeight/2, 100, 50)];
	
	self.emptyDataLabel_left.text = @"Không có dữ liệu";
	[ self.emptyDataLabel_left setFont:[UIFont systemFontOfSize:23]];
	[ self.emptyDataLabel_left setTextColor:[UIColor lightGrayColor]];
	[ self.emptyDataLabel_left sizeToFit];
	
	[self.scrollView addSubview: self.emptyDataLabel_left];
	

	
	self.runningDebtTableView = [[UITableView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, tableViewWidth - 20, tableViewHeight - 10) style:UITableViewStyleGrouped];
	
	self.runningDebtTableView.delegate = self;
	self.runningDebtTableView.dataSource = self;
	self.runningDebtTableView.tag = 1;
	[self.runningDebtTableView registerNib:[UINib nibWithNibName:@"SavingCell" bundle:nil] forCellReuseIdentifier:@"CellIden"];
	self.runningDebtTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.scrollView addSubview:_runningDebtTableView];
	
	self.expriedDebtTableView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewWidth + 10, 10.0f, tableViewWidth - 20 , tableViewHeight - 10) style:UITableViewStyleGrouped];
	self.expriedDebtTableView.delegate = self;
	self.expriedDebtTableView.dataSource = self;
	self.expriedDebtTableView.tag = 2;
	[self.expriedDebtTableView registerNib:[UINib nibWithNibName:@"ExpriedSavingCell" bundle:nil] forCellReuseIdentifier:@"ExpriedSavingCellIden"];
	self.expriedDebtTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.scrollView addSubview:_expriedDebtTableView];

    
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tableViewHeight - 6, 0);
    self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    self.scrollView.pagingEnabled = YES;
    
    [_runingButton addTarget:self action:@selector(runingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_finishedButton addTarget:self action:@selector(finishedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    _runingButton.enabled = NO;
    _finishedButton.enabled = YES;
    
	
	[self reloadData];
}

-(void)updateData {
	
//    self.runningDebtArray = [[SQLiteManager database] getTransactionDebt];
//    [self.runningDebtTableView reloadData];
	
	[self  reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showDetailSaving:(id)sender {

}


#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.runningDebtArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 235;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailDebtCell *cellDebt = [[[NSBundle mainBundle] loadNibNamed:@"DetailDebtCell" owner:nil options:nil] objectAtIndex:0];
	
    Transaction *transactionDebt = [self.runningDebtArray objectAtIndex:indexPath.section];

    
    cellDebt.startDate.text = transactionDebt.date;
    cellDebt.laisuatLbl.text = [NSString stringWithFormat:@"%.1f %%",transactionDebt.interest_rate];
    cellDebt.withWhoLbl.text = transactionDebt.who;
    cellDebt.moneyStartLbl.text = [NSString stringWithFormat:@"%lld đ",transactionDebt.money];
    cellDebt.noteLbl.text = [NSString stringWithFormat:@"Ghi chú:%@",transactionDebt.note];
    
    long long totalMoney;

    NSDate *date1 = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:[self convertStringToTimeStamp:transactionDebt.date]];
    NSDate *date2 = [NSDate date];
    NSInteger month = [self getDistanceMonth:date1 date2:date2];
    totalMoney = [[TransactionInterestRateManager getInstance] getInterestForTransaction:transactionDebt afterNumberOfMonths:labs(month)];
    
    cellDebt.gopgocLbl.text = [NSString stringWithFormat:@"%ld thang",transactionDebt.monthsPerCompounding];
    cellDebt.totalLbl.text = [NSString stringWithFormat:@"%lld đ",totalMoney];
	cellDebt.delegate = self;
	cellDebt.debt = transactionDebt;

    
    
    return  cellDebt;
}

-(NSInteger)getDistanceMonth:(NSDate *)date1 date2:(NSDate *)date2 {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit
                                               fromDate:date1
                                                 toDate:date2
                                                options:NSCalendarWrapComponents];
    
//    NSLog(@"Difference in date components: %li/%li/%li", (long)components.day, (long)components.month, (long)components.year);
	
	
    return  components.month;
}

-(NSTimeInterval )convertStringToTimeStamp:(NSString *)stringDate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd EEEE"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *currentDate = [dateFormatter dateFromString:stringDate];
    NSTimeInterval interval = [currentDate timeIntervalSinceReferenceDate];
	
//	NSLog([[[NSDate alloc] initWithTimeIntervalSinceReferenceDate:interval] description]);
	
    return interval;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView	 deselectRowAtIndexPath:indexPath animated:YES];
	
	AddDebtViewController *addDebtVC = [[AddDebtViewController alloc] initWithNibName:@"AddDebtViewController" bundle:nil];
	addDebtVC.delegate = self;
	addDebtVC.editedDebt = (Transaction *)self.runningDebtArray[indexPath.section];
	[self.navigationController pushViewController:addDebtVC animated:YES];
	
}

- (void)runingButtonAction:(UIButton*)button{
    _runingButton.enabled = NO;
    _finishedButton.enabled = YES;
    
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)finishedButtonAction:(UIButton*)button{
    _finishedButton.enabled = NO;
    _runingButton.enabled = YES;
    
    [_scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffSetX = scrollView.contentOffset.x;
    if(contentOffSetX == 0){
        _finishedButton.enabled = YES;
        _runingButton.enabled = NO;
    }else{
        if(scrollView.contentOffset.x == scrollView.frame.size.width){
            _finishedButton.enabled = NO;
            _runingButton.enabled = YES;
        }
    }
}

#pragma - Slidemenu methods
- (IBAction)showSlideMenu:(id)sender {
    [self.delegate showSlideMenu];
}


#pragma - Add savings
- (IBAction)AddSavings:(id)sender {
	
    AddDebtViewController *addDebtVC = [[AddDebtViewController alloc] initWithNibName:@"AddDebtViewController" bundle:nil];
    addDebtVC.delegate = self;
	addDebtVC.editedDebt = nil;
    [self.navigationController pushViewController:addDebtVC animated:YES];
//    AddSavingViewController *addSavingVC = [[AddSavingViewController alloc] initWithNibName:@"AddSavingViewController" bundle:nil];
//        addSavingVC.delegate = self;
//    
//        [self.navigationController pushViewController:addSavingVC animated:YES];
}

#pragma - AddSavingelegate

- (void) completeAddingSaving{
	
	[self reloadData];
}

- (void) cancelAddingSaving{
    //	NSLog(@"Cancel");
}

#pragma - DebtCellDelegate
- (void)selectedOptionButton:(Transaction *)selectedTransaction andSender:(UIButton *)sender{
	
	self.selectedDebt = selectedTransaction;
	
	OptionTableForSavingCellViewController *tableViewController = [[OptionTableForSavingCellViewController alloc] initWithNumberOfOption:2];
	
	tableViewController.delegate = self;
	
	self.popoverViewController = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverViewController.contentSize = CGSizeMake(140,90);
	self.popoverViewController.border = NO;
	self.popoverViewController.arrowDirection = FPPopoverArrowDirectionUp;
	self.popoverViewController.tint = FPPopoverLightGrayTint;
	
	[self.popoverViewController presentPopoverFromView:sender];
	
}


- (void)selectedOptionAtIndex:(NSInteger)index{
    //	NSLog(@"%d", index);
    [self.popoverViewController dismissPopoverAnimated:YES];

	if (index == 0) {
		
		AddDebtViewController *addDebtVC = [[AddDebtViewController alloc] initWithNibName:@"AddDebtViewController" bundle:nil];
		addDebtVC.delegate = self;
		addDebtVC.editedDebt = self.selectedDebt;
		[self.navigationController pushViewController:addDebtVC animated:YES];
		
	}else{
		if(index == 1){
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
																message:@"Bạn có chắc muốn xoá giao dịch này không?"
															   delegate:self
													  cancelButtonTitle:@"Không"
													  otherButtonTitles:@"Có", nil];
			[alertView show];
		}
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
		// Do nothing
    }else{
		
        [[SQLiteManager database] deleteDebt:self.selectedDebt];
        
		[self reloadData];
		
		/*
        // Cancel local notification
        NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (UILocalNotification *localNotification in localNotifications){
            NSDictionary *userInfo = localNotification.userInfo;
            NSString *localNotificationWithSavingName = [userInfo valueForKey:@"SavingID"];
            if([localNotificationWithSavingName isEqualToString:self.selectedSaving.name]){
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
                break;
            }
        }
		 */
    }
}

- (void)reloadData{
	
	self.runningDebtArray = [[SQLiteManager database] getTransactionDebt];
	[self.runningDebtTableView reloadData];
	
//	self.runningDebtArray = [[SQLiteManager database] getSavingsIsExpried:NO];
//	self.expriedDebtArray = [[SQLiteManager database] getSavingsIsExpried:YES];
//	[self.runningDebtTableView reloadData];
//	[self.expriedDebtTableView reloadData];
	
	if([self.runningDebtArray count] == 0){
		
		self.emptyDataLabel_left.hidden = NO;
		self.runningDebtTableView.hidden = YES;
		
	}else{
		
		self.emptyDataLabel_left.hidden = YES;
		self.runningDebtTableView.hidden = NO;
	}
	
	if([self.expriedDebtArray count] == 0){
		
		self.emptyDataLabel_right.hidden = NO;
		self.expriedDebtTableView.hidden = YES;
		
	}else{
		
		self.emptyDataLabel_right.hidden = YES;
		self.expriedDebtTableView.hidden = NO;
	}
}

@end





