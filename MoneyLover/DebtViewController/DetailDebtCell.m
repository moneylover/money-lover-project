//
//  DetailDebtCell.m
//  MoneyLover
//
//  Created by Giap Nguyen on 11/17/15.
//  Copyright © 2015 Ngo Huu Tuan. All rights reserved.
//

#import "DetailDebtCell.h"

@implementation DetailDebtCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectedOption:(id)sender {
	
	[self.delegate selectedOptionButton:self.debt andSender:sender];
	NSLog(@"%ld", self.debt.Id);
}


@end
