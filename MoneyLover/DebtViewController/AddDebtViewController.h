//
//  AddDebtViewController.h
//  MoneyLover
//
//  Created by Giap Nguyen on 11/16/15.
//  Copyright © 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
#import "Transaction.h"


@protocol AddDebtDelegate
    -(void)updateData;
@end

@interface AddDebtViewController : UIViewController<UIKeyboardViewControllerDelegate>

@property(weak,nonatomic) id<AddDebtDelegate> delegate;
@property(strong, nonatomic) Transaction *editedDebt;

@property (strong, nonatomic) IBOutlet UISegmentedControl *chooseDebtType;
@property (strong, nonatomic) IBOutlet UITextField *moneyTextfield;
@property (strong, nonatomic) IBOutlet UITextField *noteTextfield;
@property (strong, nonatomic) IBOutlet UIButton *whoWithTextfield;
@property (strong, nonatomic) IBOutlet UITextField *amountTextfield;
@property (strong, nonatomic) IBOutlet UISwitch *enableDoubleAmount;
@property (strong, nonatomic) IBOutlet UIButton *chooseDateButton;
@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) IBOutlet UISwitch *chooseAmountType;
@property (strong, nonatomic) IBOutlet UILabel *gopgocLBL;
@property (strong, nonatomic) IBOutlet UILabel *thangLbl;
@property (strong, nonatomic) IBOutlet UITextField *monthTxt;


@end
