//
//  DetailDebtCell.h
//  MoneyLover
//
//  Created by Giap Nguyen on 11/17/15.
//  Copyright © 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

@protocol  DebtCellDelegate <NSObject>

@required
- (void)selectedOptionButton:(Transaction *)selectedTransaction andSender:(UIButton *)sender;

@end

@interface DetailDebtCell : UITableViewCell

@property (weak, nonatomic) id <DebtCellDelegate> delegate;
@property (strong, nonatomic) Transaction *debt;

@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UILabel *startDate;
@property (strong, nonatomic) IBOutlet UILabel *laisuatLbl;
@property (strong, nonatomic) IBOutlet UILabel *withWhoLbl;
@property (strong, nonatomic) IBOutlet UILabel *moneyStartLbl;
@property (strong, nonatomic) IBOutlet UILabel *noteLbl;
@property (strong, nonatomic) IBOutlet UILabel *gopgocLbl;


@end
