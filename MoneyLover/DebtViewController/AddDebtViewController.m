//
//  AddDebtViewController.m
//  MoneyLover
//
//  Created by Giap Nguyen on 11/16/15.
//  Copyright © 2015 Ngo Huu Tuan. All rights reserved.
//

#import "AddDebtViewController.h"
#import "ChoosePeopleInContactViewController.h"
#import "Calendar.h"
#import "Transaction.h"
#import "SQLiteManager.h"
#import "SavingManager.h"


@interface AddDebtViewController ()<ChoosePeopleInContactProtocol>{
    UIKeyboardViewController *keyboard;
}

@end

@implementation AddDebtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd EEEE"];
    [_chooseDateButton setTitle:[dateFormater stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    [self.chooseDateButton addTarget:self action:@selector(chooseDateAction:) forControlEvents:UIControlEventTouchUpInside];
    if (!self.chooseAmountType.isOn) {
        self.gopgocLBL.hidden = YES;
        self.thangLbl.hidden = YES;
        self.monthTxt.hidden = YES;
    }else {
        self.gopgocLBL.hidden = NO;
        self.thangLbl.hidden = NO;
        self.monthTxt.hidden = NO;
    }
	
	if(self.editedDebt != nil){
		
		// vay, cho vay
		self.moneyTextfield.text = [NSString stringWithFormat:@"%lld", self.editedDebt.money];
		self.noteTextfield.text = [NSString stringWithFormat:@"%@", self.editedDebt.note];
		[self.whoWithTextfield setTitle:self.editedDebt.who forState:UIControlStateNormal];
		self.amountTextfield.text = [NSString stringWithFormat:@"%f", self.editedDebt.interest_rate];
//		self.
	}
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    keyboard = [[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
}

- (IBAction)cancelAction:(id)sender {
    [_moneyTextfield resignFirstResponder];
    [_noteTextfield resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)chooseUser:(id)sender {
    [_moneyTextfield resignFirstResponder];
    [_noteTextfield resignFirstResponder];
	
    ChoosePeopleInContactViewController *choosePeopleInContact = [[ChoosePeopleInContactViewController alloc] initWithNibName:@"ChoosePeopleInContactViewController" bundle:nil];
    choosePeopleInContact.delegate = self;
	
    [self.navigationController pushViewController:choosePeopleInContact animated:YES];
}

- (void)choosePeoplesName:(NSMutableArray*)nameArray{
    NSMutableString *withPeopleString = [[NSMutableString alloc] init];
    if([nameArray count] != 0) {
        for(int i = 0;  i < [nameArray count]; i++){
            if( i == [nameArray count] - 1){
                [withPeopleString appendString:[NSString stringWithFormat:@"%@", (NSString*)nameArray[i] ]];
            }else{
                [withPeopleString appendString:[NSString stringWithFormat:@"%@ - ", (NSString*)nameArray[i] ]];
            }
        }
        [_whoWithTextfield setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [_whoWithTextfield setTitle:withPeopleString forState:UIControlStateNormal];
    }
}

- (void)chooseDateAction:(UIButton*)button{
    [_moneyTextfield resignFirstResponder];
    [_noteTextfield resignFirstResponder];
    
    [_chooseDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [[Calendar getInstanceWithParentViewController:self] showCalendar];
    
}

- (void)selectedDate:(NSDate*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
    self.selectedDate = date;
    [_chooseDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
    _chooseDateButton.titleLabel.textColor = [UIColor blackColor];
}

- (IBAction)completeAction:(id)sender {
	
//	NSLog(@"%f", [self.amountTextfield.text floatValue]);
	
	
    CGFloat money = [[_moneyTextfield text] floatValue];
    if(money == 0){
        UIAlertView *alertMoneyField= [[UIAlertView alloc]  initWithTitle:nil message:@"Bạn cần nhập số tiền" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Đóng", nil];
        [alertMoneyField show];
        return;
    }
    
    Transaction *transaction = [[Transaction alloc] init];
    //   transaction.category_id = _selectedCategory_id;
    //  transaction.category = [[SQLiteManager database] getCategoryWithId:transaction.category_id];
    //transaction.money = [[_moneyTextfield text] longLongValue]*(transaction.category.isEarning?1:-1);
    transaction.note = [_noteTextfield text];
    if([[_whoWithTextfield titleLabel].text isEqualToString:@"Với"]){
        transaction.who = @"";
    }else{
        transaction.who = [_whoWithTextfield titleLabel].text;
    }
    transaction.date = [_chooseDateButton titleLabel].text;
    long long currentAmountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue] ;
    
    if (self.chooseDebtType.selectedSegmentIndex == 0) {
        currentAmountOfMoney -= [self.moneyTextfield.text longLongValue];
        
    }else{
        currentAmountOfMoney += [self.moneyTextfield.text longLongValue];
    }
	
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:(currentAmountOfMoney)] doubleValue] forKey:@"AmountOfMoney"];
    
    transaction.compounding = self.chooseAmountType.isOn?1:0;
    transaction.money = [self.moneyTextfield.text longLongValue];
    transaction.interest_rate = [self.amountTextfield.text integerValue];
    transaction.monthsPerCompounding = [self.monthTxt.text integerValue];
    transaction.isDebt = self.chooseDebtType.selectedSegmentIndex == 0 ?@"0":@"1";
	
    [[SQLiteManager database] insertTransactionDebt:transaction];
    
//    if(_editedTransaction != nil) {
//        transaction.Id = _editedTransaction.Id;
//        [[SQLiteManager database] updateTransaction:transaction];
//        
//        //		NSLog(@"New money: %lld\nOld money %lld", transaction.money, self.editedTransaction.money);
//        //		NSLog(@"Chenh lech: %lld\nSau khi update %lld", amountOfMoney, (currentAmountOfMoney + amountOfMoney));
//        
//        [[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:(currentAmountOfMoney + amountOfMoney)] doubleValue] forKey:@"AmountOfMoney"];
//        
//    }else{
//        [[SQLiteManager database] insertTransaction:transaction];
//        [[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:(currentAmountOfMoney + amountOfMoney)] doubleValue] forKey:@"AmountOfMoney"];
//    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    [[SavingManager getInstance] cacutaleSaving];
	
    [self.delegate updateData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onChooseTypeAmount:(id)sender{
    if (!self.chooseAmountType.isOn) {
        self.gopgocLBL.hidden = YES;
        self.thangLbl.hidden = YES;
        self.monthTxt.hidden = YES;
    }else {
        self.gopgocLBL.hidden = NO;
        self.thangLbl.hidden = NO;
        self.monthTxt.hidden = NO;
    }
}
- (IBAction)onChooseDate:(id)sender {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
