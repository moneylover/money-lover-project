//
//  ContentAddRecurringTransactionVC.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/10/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RecurringTransaction;

@protocol RecurringTransactionDelegate <NSObject>


@end


@interface ContentAddRecurringTransactionVC : UIViewController

@property (strong, nonatomic) RecurringTransaction* editedRecurringTransaction;
@property (assign, nonatomic) BOOL isUpdateRecurringTransaction;


- (BOOL)completeAction;

@end
