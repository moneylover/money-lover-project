//
//  RecurringTransactionCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecurringTransaction.h"

@protocol RecurringCellDelegate <NSObject>

@required
- (void)selectedOptionButton:(RecurringTransaction *)recurringTransaction andSender:(UIButton *)sender;

@end


@interface RecurringTransactionCell : UITableViewCell

@property (weak, nonatomic) id <RecurringCellDelegate> delegate;
@property (strong, nonatomic) RecurringTransaction *recurringTransaction;

@property (weak, nonatomic) IBOutlet UIImageView *image_view;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextRepeatLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;

@end
