//
//  ContentAddRecurringTransactionVC.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/10/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ContentAddRecurringTransactionVC.h"
#import "Calendar.h"
#import "RecurringTransaction.h"
#import "ChooseCategoryViewController.h"
#import "CategoryTransaction.h"
#import "SQLiteManager.h"
#import "RecurringTransactionManager.h"

@interface ContentAddRecurringTransactionVC () <CalendarDelegate, ChooseCategoryProtocol, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIButton *chooseCategoryButton;
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;
//@property (weak, nonatomic) IBOutlet UILabel *nextRepeatLabel;

@property (weak, nonatomic) IBOutlet UIButton *durationMode_left_Button;
@property (weak, nonatomic) IBOutlet UIButton *durationMode_right_button;
@property (weak, nonatomic) IBOutlet UILabel *timeModeLabel;

@property (weak, nonatomic) IBOutlet UIButton *startDateButton;
@property (weak, nonatomic) IBOutlet UITextField *stepTextField;
@property (weak, nonatomic) IBOutlet UILabel *stepLabel;

@property (weak, nonatomic) IBOutlet UIButton *endDate_left_button;
@property (weak, nonatomic) IBOutlet UIButton *endDate_right_button;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (weak, nonatomic) IBOutlet UIButton *endDateButton;

@property (weak, nonatomic) IBOutlet UITextField *numberOfIterationTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *chooseRepeatDateSegmentedControl;


@property (assign, nonatomic) NSInteger timeMode_flag;				// 0 1 2
@property (assign, nonatomic) NSInteger durationMode_flag;		// 0 1 2

/// Nhận biết user đang chọn ngày bắt đầu hay ngày kết thúc
@property (assign, nonatomic) NSInteger chooseDate_flag;			// 0 1 2

@property (strong, nonatomic) RecurringTransaction *recurringTransaction;
@property (strong, nonatomic) CategoryTransaction *categoryTransaction;
@property (assign, nonatomic) NSInteger startDate;
@property (assign, nonatomic) NSInteger endDate;
@property (assign, nonatomic) NSInteger modeRepeatMonth;
@end

@implementation ContentAddRecurringTransactionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//	self.imageview.transform = CGAffineTransformMakeRotation(M_PI_2);
	self.noteTextField.delegate = self;
	self.moneyTextField.delegate = self;
	self.stepTextField.delegate  = self;
	self.numberOfIterationTextField.delegate = self;
	self.stepTextField.text = @"1";
	
	self.durationMode_right_button.transform = CGAffineTransformMakeRotation(M_PI);
	self.endDate_right_button.transform =CGAffineTransformMakeRotation(M_PI);
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"yyyy-MM-dd EEEE";
	
	[self.startDateButton setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
	[self.endDateButton setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
	
	if(self.editedRecurringTransaction == nil){
		_timeMode_flag = 0;
		_durationMode_flag = 0;
		_chooseDate_flag = 0;
		
		self.stepTextField.text = @"1";
		self.numberOfIterationTextField.text = @"1";
		self.endDate = [[NSDate date] timeIntervalSinceReferenceDate];
		self.startDate = self.endDate;
		self.modeRepeatMonth  = 1;
	}else{
		_timeMode_flag = self.editedRecurringTransaction.timeMode;
		_durationMode_flag = self.editedRecurringTransaction.durationMode;
		_chooseDate_flag = 0;
		
		self.endDate = self.editedRecurringTransaction.untilDate;
		self.startDate = self.editedRecurringTransaction.startDate;
		self.modeRepeatMonth  = self.editedRecurringTransaction.modeRepeatMonth;
		
		self.iconView.image =  [UIImage imageNamed:self.editedRecurringTransaction.categoryTransaction.iconName];
		[self.chooseCategoryButton setTitle:self.editedRecurringTransaction.categoryTransaction.name forState:UIControlStateNormal];
		self.moneyTextField.text = [NSString stringWithFormat:@"%lld", llabs(self.editedRecurringTransaction.amount)];
		self.noteTextField.text = self.editedRecurringTransaction.note;
		
		[self.startDateButton setTitle:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.startDate]] forState:UIControlStateNormal];
		[self.endDateButton setTitle:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.endDate]] forState:UIControlStateNormal];
		self.numberOfIterationTextField.text = [NSString stringWithFormat:@"%ld", (long)self.editedRecurringTransaction.numberOfEvents];
		
		self.stepTextField.text = [NSString stringWithFormat:@"%ld", (long)self.editedRecurringTransaction.step];
		self.chooseRepeatDateSegmentedControl.selectedSegmentIndex = self.editedRecurringTransaction.modeRepeatMonth;
		
		self.categoryTransaction = self.editedRecurringTransaction.categoryTransaction;
	}
	
	
	[self.chooseRepeatDateSegmentedControl addTarget:self action:@selector(chooseRepeatDateForMonthlyRecurringTransaction:) forControlEvents:UIControlEventValueChanged];
	
	self.recurringTransaction = [[RecurringTransaction alloc] init];
	
	[self updateComponentState];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];

}

- (void)updateComponentState{
	switch (_timeMode_flag) {
		case 0:{
			self.timeModeLabel.text = @"Lặp hàng ngày";
			self.stepLabel.text = @"ngày";
			self.chooseRepeatDateSegmentedControl.hidden = YES;
		}
			break;
		case 1:{
			self.timeModeLabel.text = @"Lặp hàng tháng";
			self.stepLabel.text = @"tháng";
			self.chooseRepeatDateSegmentedControl.hidden = NO;
		}
			break;
		case 2:{
			self.timeModeLabel.text = @"Lặp hàng năm";
			self.stepLabel.text = @"năm";
			self.chooseRepeatDateSegmentedControl.hidden = YES;
		}
			break;
  default:
			break;
	}
	
	switch (_durationMode_flag) {
		case 0:{
			self.endDateLabel.text = @"Mãi mãi";
			self.endDateButton.hidden = YES;
			self.numberOfIterationTextField.hidden = YES;
		}
			break;
		case 1:{
			self.endDateLabel.text = @"Cho đến ngày";
			self.endDateButton.hidden = NO;
			self.numberOfIterationTextField.hidden = YES;
		}
			break;
		case 2:{
			self.endDateLabel.text = @"Xảy ra một số lần nhất định";
//			self.e
			self.endDateButton.hidden = YES;
			self.numberOfIterationTextField.hidden = NO;
		}
			break;
  default:
			break;
	}
	
	[self myResignFirstResponder];
//	[self updateNextReapeatLabel];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self myResignFirstResponder];
}

- (void)myResignFirstResponder{
	
	[self.noteTextField resignFirstResponder];
	[self.moneyTextField resignFirstResponder];
	[self.stepTextField resignFirstResponder];
	[self.numberOfIterationTextField resignFirstResponder];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
		[textField resignFirstResponder];
//		NSLog(@"text field did end editing");
		return YES;
}


#pragma mark - IBAction

- (IBAction)chooseStartDateAction:(id)sender {
	[[Calendar getInstanceWithParentViewController:self] showCalendar];
	_chooseDate_flag = 1;
}

- (IBAction)chooseEndDateAction:(id)sender {
	[[Calendar getInstanceWithParentViewController:self] showCalendar];
	_chooseDate_flag = 2;
}

- (IBAction)chooseCategoryTransactionAction:(id)sender {
	ChooseCategoryViewController *chooseCategoryTransactionVC = [[ChooseCategoryViewController alloc] initWithNibName:@"ChooseCategoryViewController" bundle:nil];
	chooseCategoryTransactionVC.delegate = self;
	
	[self.navigationController pushViewController:chooseCategoryTransactionVC animated:YES];
}

- (IBAction)back_timeMode:(id)sender {
	_timeMode_flag = ((_timeMode_flag - 1) < 0) ? 2:(--_timeMode_flag);
	
//	NSLog(@"%ld", (long)_timeMode_flag);
	
	[self updateComponentState];
}


- (IBAction)next_timeMode:(id)sender {
	_timeMode_flag = (_timeMode_flag + 1)%3;
	
//	NSLog(@"%ld", (long)_timeMode_flag);
	
	[self updateComponentState];
	// [self updateNextReapeatLabel];
//	NSLog(@"%@", self.nextRepeatLabel.text);
}


- (IBAction)back_durationMode:(id)sender {
	_durationMode_flag = ((_durationMode_flag - 1) < 0) ? 2:(--_durationMode_flag);
//	NSLog(@"%ld", (long)_durationMode_flag);
	
	[self updateComponentState];
}

- (IBAction)next_durationMode:(id)sender {
	_durationMode_flag = (_durationMode_flag + 1)%3;
//	NSLog(@"%ld", (long)_durationMode_flag);
	
	[self updateComponentState];
}


#pragma mark - CalendarDelegate

- (void)selectedDate:(NSDate *)date{
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"yyyy-MM-dd EEEE";
	if(_chooseDate_flag == 1){
		
			[self.startDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
		self.startDate = [date timeIntervalSinceReferenceDate];
		
		/*
		NSDateComponents* startDateComps = [[NSCalendar currentCalendar]
																				components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
																				fromDate:date];
		
		NSDateComponents* nextDayComps = [[RecurringTransactionManager getInstance] getNextDayComponents:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];
		
		self.nextRepeatLabel.text =  [NSString stringWithFormat:@"%ld-%ld-%ld", nextDayComps.year, nextDayComps.month, nextDayComps.day];
		*/
		
	}else{
		
		if(_chooseDate_flag == 2){
			[self.endDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
			self.endDate = [date timeIntervalSinceReferenceDate];
		}
		
	}
	
	[self myResignFirstResponder];
}


#pragma mark - ChooseCategoryProtocol

//- (void)getIconName:(NSString *)iconName{
//	self.iconView.image = [UIImage imageNamed:iconName];
//}

- (void)selectedCategoryTransaction:(CategoryTransaction*)categoryTransaction{
	self.iconView.image = [UIImage imageNamed:categoryTransaction.iconName];
	self.categoryTransaction = categoryTransaction;
	[self.chooseCategoryButton setTitle:categoryTransaction.name forState:UIControlStateNormal];
	[self myResignFirstResponder];
}


#pragma mark - UISegmentedControl changes value

- (void)chooseRepeatDateForMonthlyRecurringTransaction:(UISegmentedControl *)paramSender{
	self.modeRepeatMonth =  paramSender.selectedSegmentIndex;
	
//	[self updateNextReapeatLabel];
}


- (BOOL)completeAction{
	if([self checkInput] == YES){
		if(self.editedRecurringTransaction != nil){
			// Cập nhật
			
			long long money = llabs([self.moneyTextField.text longLongValue]);
			
			self.editedRecurringTransaction.categoryTransaction	= self.categoryTransaction;
			self.editedRecurringTransaction.amount = (self.categoryTransaction.isEarning) ? money : (money * -1);
			self.editedRecurringTransaction.note = self.noteTextField.text;
			self.editedRecurringTransaction.timeMode = self.timeMode_flag;
			self.editedRecurringTransaction.step = [self.stepTextField.text integerValue];
			self.editedRecurringTransaction.numberOfEvents = [self.numberOfIterationTextField.text integerValue];
			self.editedRecurringTransaction.durationMode = self.durationMode_flag;
			self.editedRecurringTransaction.untilDate = self.endDate;
			self.editedRecurringTransaction.modeRepeatMonth = self.modeRepeatMonth;
			self.editedRecurringTransaction.startDate = self.startDate;
			
			NSDateComponents* startDateComps = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.startDate]];
			startDateComps = [[RecurringTransactionManager getInstance] getStartDateComponents:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];

			NSDateComponents* lastRepeatComps = [[RecurringTransactionManager getInstance] getLastRepeatForTransactionWithStartingDate:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];
			
//			NSLog(@"@ lastRepeatComps: %ld/%ld/%ld", lastRepeatComps.day, lastRepeatComps.month, lastRepeatComps.year);

			self.editedRecurringTransaction.lastRepeat = [[NSCalendar currentCalendar] dateFromComponents:lastRepeatComps].timeIntervalSinceReferenceDate;
			
			[[SQLiteManager database] updateRecurringTransaction:self.editedRecurringTransaction];
			
			return YES;
			
		}else{
			// Tạo mới
			long long money = llabs([self.moneyTextField.text longLongValue]);
			
			self.recurringTransaction = [[RecurringTransaction alloc] init];

			self.recurringTransaction.categoryTransaction = self.categoryTransaction;
			self.recurringTransaction.amount = (self.categoryTransaction.isEarning) ? money : (money * -1);
			self.recurringTransaction.note = self.noteTextField.text;
			self.recurringTransaction.timeMode = self.timeMode_flag;
			self.recurringTransaction.step = [self.stepTextField.text integerValue];
			self.recurringTransaction.numberOfEvents = [self.numberOfIterationTextField.text integerValue];
			self.recurringTransaction.durationMode = self.durationMode_flag;
			self.recurringTransaction.untilDate = self.endDate;
			self.recurringTransaction.modeRepeatMonth = self.modeRepeatMonth;
			self.recurringTransaction.startDate = self.startDate;
			
			NSDateComponents* startDateComps = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.startDate]];

			startDateComps = [[RecurringTransactionManager getInstance] getStartDateComponents:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];

//			NSLog(@"Test startDateComps =  %ld/%ld/%ld", startDateComps.day, startDateComps.month, startDateComps.year);

			NSDateComponents* lastRepeatComps = [[RecurringTransactionManager getInstance] getLastRepeatForTransactionWithStartingDate:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];

			self.recurringTransaction.lastRepeat = [[NSCalendar currentCalendar] dateFromComponents:lastRepeatComps].timeIntervalSinceReferenceDate;

			[[SQLiteManager database] insertRecurringTransaction:self.recurringTransaction];
			
			return YES;
			
		}
	}else{
		return NO;
	}
	return NO;
}

// - (void)updateNextReapeatLabel{
	
// 	NSDateComponents* startDateComps = [[NSCalendar currentCalendar]
// 																			components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
// 																			fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.startDate]];
	
// 	NSDateComponents* nextDayComps = [[RecurringTransactionManager getInstance] getStartDateComponents:startDateComps withTimeMode:self.timeMode_flag withStep:[self.stepTextField.text intValue] withModeRepeatMonth:self.modeRepeatMonth];
	
// 	NSLog(@"%ld", (long)[self.stepTextField.text intValue]);
// 	self.nextRepeatLabel.text =  [NSString stringWithFormat:@"%ld-%ld-%ld", (long)nextDayComps.year, (long)nextDayComps.month, (long)nextDayComps.day];
	
// }

- (BOOL)checkInput{
	
	if(self.categoryTransaction == nil){
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn cần chọn loại giao dịch" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
		[alertView show];
		return NO;
	}
	
	if([self.moneyTextField.text longLongValue]  == 0){
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn cần chọn loại giao dịch" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
		[alertView show];
		return NO;
	}
	
	/*
	NSTimeInterval toDay = (NSTimeInterval)[NSDate date].timeIntervalSinceReferenceDate;
	
	if(self.startDate < toDay){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:@"Thông báo"
																													message:@"Giao dịch định kì không được bắt đầu trong quá khứ"
																												 delegate:self
																								cancelButtonTitle:@"Đóng"
																								otherButtonTitles:nil];
		[invalidName show];
		return NO;
	}
	*/
	
	if(_durationMode_flag == 1){
		if(self.startDate > self.endDate){
			UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
																														message:@"Ngày bắt đầu hoặc ngày kết thúc của bạn không hợp lệ"
																													 delegate:self
																									cancelButtonTitle:@"Đóng"
																									otherButtonTitles:nil];
			[invalidName show];
			return NO;
		}
	}


	return YES;
}


@end











