

#import "AddRecurringTransactionController.h"
#import "SQLiteManager.h"
#import "ContentAddRecurringTransactionVC.h"


@interface AddRecurringTransactionController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) ContentAddRecurringTransactionVC *contentVC;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end


@implementation AddRecurringTransactionController


- (void)viewDidLoad{
	[super viewDidLoad];
		self.tableView.delegate = self;
		self.tableView.dataSource = self;
		[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	
		if(self.editedRecurringTransaction == nil){
			self.deleteButton.enabled = NO;
		}else{
			self.deleteButton.enabled = YES;
		}
	
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];

}


#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 680;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
	
	self.contentVC = [[ContentAddRecurringTransactionVC alloc] init];
	self.contentVC.editedRecurringTransaction = self.editedRecurringTransaction;
	[self addChildViewController:self.contentVC];
	[self.contentVC didMoveToParentViewController:self];
	
	
	[cell.contentView addSubview:self.contentVC.view];
	self.contentVC.view.translatesAutoresizingMaskIntoConstraints = NO;

	NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																	 attribute:NSLayoutAttributeTop
																																	 relatedBy:NSLayoutRelationEqual
																																			toItem:cell
																																	 attribute:NSLayoutAttributeTop
																																	multiplier:1.0f
																																		constant:0.0f];
	
	NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																			attribute:NSLayoutAttributeBottom
																																			relatedBy:NSLayoutRelationEqual
																																					toItem:cell
																																			attribute:NSLayoutAttributeBottom
																																			multiplier:1.0f
																																				constant:0.0f];
	NSLayoutConstraint *horizontalCenterConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																								attribute:NSLayoutAttributeCenterX
																																								relatedBy:NSLayoutRelationEqual
																																									 toItem:cell
																																								attribute:NSLayoutAttributeCenterX
																																							 multiplier:1.0f
																																								 constant:0.0f];
	NSLayoutConstraint *verticalCenterConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																							attribute:NSLayoutAttributeCenterY
																																							relatedBy:NSLayoutRelationEqual
																																								 toItem:cell
																																							attribute:NSLayoutAttributeCenterY
																																						 multiplier:1.0f
																																							 constant:0.0f];
	
	
	NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																			 attribute:NSLayoutAttributeLeading
																																			 relatedBy:NSLayoutRelationEqual
																																					toItem:cell
																																			 attribute:NSLayoutAttributeLeft
																																			multiplier:1.0f
																																				constant:0.0f];
	
	NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:self.contentVC.view
																																				attribute:NSLayoutAttributeTrailing
																																				relatedBy:NSLayoutRelationEqual
																																					 toItem:cell
																																				attribute:NSLayoutAttributeLeft
																																			 multiplier:1.0f
																																				 constant:0.0f];
	[cell addConstraints:@[topConstraint,bottomConstraint, horizontalCenterConstraint, verticalCenterConstraint, leadingConstraint, trailingConstraint]];

	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - IBAction

- (IBAction)completeAction:(id)sender {
	
	if([self.contentVC completeAction] == YES){
		
			[self.delegate updateTableView];
		
			[self.navigationController popViewControllerAnimated:YES];
		
	}
}


- (IBAction)cancelAction:(id)sender {
	
		[self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)deleteRecurringTransactionAction:(id)sender {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo"
            message:@"Bạn có chắc muốn xoá giao dịch này"
            delegate:self cancelButtonTitle:@"Cancel"
            otherButtonTitles:@"Ok", nil];
		[alertView show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(buttonIndex == 1){ // Clicked OK
		
			[[SQLiteManager database] deleteRecurringTransaction:self.editedRecurringTransaction];
			
			[self.delegate updateTableView];
			
			[self.navigationController popViewControllerAnimated:YES];
		
	}
}

@end




















