//#import <UI/UIKit.h>
#import "RecurringTransaction.h"

@protocol AddRecurringTransactionDelegate <NSObject>

@required
- (void)updateTableView;

@end

@interface AddRecurringTransactionController : UIViewController

@property (weak, nonatomic)  id <AddRecurringTransactionDelegate> delegate;
@property (strong, nonatomic) RecurringTransaction* editedRecurringTransaction;

@end