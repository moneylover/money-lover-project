

#import "RecurringTransactionManager.h"
#import "RecurringTransaction.h"
#import "Transaction.h"
#import "SQLiteManager.h"
#import "NotificationCenterManager.h"

#define MONTHS_PER_YEAR 12
#define SECONDS_PER_DAY 86400

enum TimeMode {
	MODE_DAY,
	MODE_MONTH,
	MODE_YEAR
};

enum DurationMode {
	MODE_FOREVER,
	MODE_EXPIRED,
	MODE_TIMES
};

enum ModeRepeatMonth {
	MODE_1ST,
	MODE_15TH
};

@interface RecurringTransactionManager ()
@property (strong, nonatomic) NSMutableArray* transactions;
@end

@implementation RecurringTransactionManager


+(id) getInstance {
	static RecurringTransactionManager* instance = nil;
	static dispatch_once_t once = 0;
	dispatch_once(&once, ^{instance = [[self alloc] initPrivate];});
	return instance;
}

-(id)initPrivate {
	if ((self = [super init])) {

	}
	return self;
}

-(id)init {
	[self doesNotRecognizeSelector:_cmd];
	return nil;
}

-(void)doScheduledTransactions {
//	NSLog(@"@mark log: doScheduledTransactions");
	self.transactions = [[SQLiteManager database] getScheduledTransactions];
	for (int i = 0; i < self.transactions.count; ++i) {
		NSInteger transactionTimes = [self countTimesOfTransaction:[self.transactions objectAtIndex:i]];
		if (transactionTimes > 0) {
//			NSLog(@"@mark log: A transaction occured %ld times", (long)transactionTimes);
			[self doRecurringTransaction:[self.transactions objectAtIndex:i] withTimes:transactionTimes];
		}
	}
}

-(void)doRecurringTransaction:(RecurringTransaction*)transaction withTimes:(NSInteger)times {
	if (times < 1) return;
    BOOL cleanup = NO;
	Transaction* thisTransaction = [[Transaction alloc] init];
	RecurringTransaction* thisRecurringTransaction = transaction;

	NSDateComponents* lastRepeat = [[NSCalendar currentCalendar] 
		components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
		fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:transaction.lastRepeat]];

    NSDateComponents* expiredDate = [[NSCalendar currentCalendar]
        components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
        fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:transaction.untilDate]];
    
    long long expiredDateToInterval = [[NSCalendar currentCalendar] dateFromComponents:expiredDate].timeIntervalSinceReferenceDate;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd EEEE";
    
	thisTransaction.category_id = thisRecurringTransaction.categoryTransaction.Id;
	thisTransaction.money = thisRecurringTransaction.amount;
	thisTransaction.note = thisRecurringTransaction.note;
	thisTransaction.who = nil;

	for (int i = 0; i < times; i++) {
        
        if (transaction.durationMode == MODE_TIMES) {
            thisRecurringTransaction.numberOfEvents -= 1;
            if (thisRecurringTransaction.numberOfEvents < 0) {
//                NSLog(@"@mark log: [ERROR] Recurring transaction in TIMES MODE - numberOfEvents < 1. It will be deleted now.");
                [[SQLiteManager database] deleteRecurringTransaction:thisRecurringTransaction];
                return;
            };
            if (thisRecurringTransaction.numberOfEvents == 0) {
//                NSLog(@"@mark log: [WARNING] Recurring transaction in TIMES MODE - transaction will expired. Its cleanup has been scheduled.");
                cleanup = YES;
            }
        }
        
        if (transaction.durationMode == MODE_EXPIRED) {
            long long lastRepeatToInterval = [[NSCalendar currentCalendar] dateFromComponents:lastRepeat].timeIntervalSinceReferenceDate;
            if (lastRepeatToInterval > expiredDateToInterval) {
//                NSLog(@"@mark log: [ERROR] Recurring transaction in EXPIRED MODE. It will be deleted now.");
                [[SQLiteManager database] deleteRecurringTransaction:thisRecurringTransaction];
                return;
            }
        }
        
		if (transaction.timeMode == MODE_DAY) {
            long long lastRepeatToInterval = [[NSCalendar currentCalendar] dateFromComponents:lastRepeat].timeIntervalSinceReferenceDate;
            lastRepeat = [[NSCalendar currentCalendar]
                          components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                            fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:lastRepeatToInterval + transaction.step * SECONDS_PER_DAY]];
		} else if (transaction.timeMode == MODE_MONTH) {
			lastRepeat.month += transaction.step;
            NSInteger elapsedYear = (lastRepeat.month - 1) / (MONTHS_PER_YEAR);
            lastRepeat.month %= MONTHS_PER_YEAR;
            if (lastRepeat.month == 0) lastRepeat.month = 12;
            lastRepeat.year += elapsedYear;
            if (transaction.modeRepeatMonth == MODE_15TH) {
                lastRepeat.day = 15;
            }
		} else if (transaction.timeMode == MODE_YEAR) {
			lastRepeat.year += transaction.step;
		} else return;
        
        if (transaction.durationMode == MODE_EXPIRED) {
            long long lastRepeatToInterval = [[NSCalendar currentCalendar] dateFromComponents:lastRepeat].timeIntervalSinceReferenceDate;
            if (lastRepeatToInterval > expiredDateToInterval) {
//                NSLog(@"@mark log: [WARNING] Recurring transaction in EXPIRED MODE - transaction will expired. Its cleanup has been scheduled.");
                cleanup = YES;
            }
        }
        
        transaction.lastRepeat = [[NSCalendar currentCalendar] dateFromComponents:lastRepeat].timeIntervalSinceReferenceDate;
        
		NSString *dateString = [dateFormatter
			stringFromDate: [NSDate dateWithTimeIntervalSinceReferenceDate:transaction.lastRepeat]];
		thisTransaction.date = dateString;
		[[SQLiteManager database] insertTransaction:thisTransaction];

        BOOL isEarning = thisRecurringTransaction.amount > 0 ? true : false;
        long long amount = thisRecurringTransaction.amount > 0 ? (long long)thisRecurringTransaction.amount : (long long)(-thisRecurringTransaction.amount);
        
        NSString* message = [NSString stringWithFormat:@"Tên giao dịch: %@\nNgày thực hiện %ld/%ld/%ld\nSố tiền = %lld đ", thisRecurringTransaction.categoryTransaction.name, (long)lastRepeat.day,(long)lastRepeat.month, (long)lastRepeat.year, amount];
		
        NSString* title = nil;
		
        if (isEarning) {
            title = [NSString stringWithFormat:@"Giao dịch định kỳ - THU"];
        } else {
            title = [NSString stringWithFormat:@"Giao dịch định kỳ - CHI"];
        }
        
        [[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];

		long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		[[NSUserDefaults standardUserDefaults] 
			setDouble:[[NSNumber numberWithLongLong:(amountOfMoney + thisRecurringTransaction.amount)] doubleValue]
			forKey:@"AmountOfMoney"];
        
        if (cleanup) {
            [[SQLiteManager database] deleteRecurringTransaction:thisRecurringTransaction];
            return;
        }
	}
    [[SQLiteManager database] updateRecurringTransaction:thisRecurringTransaction];
}

-(NSInteger)countTimesOfTransaction:(RecurringTransaction*)transaction {
//	NSLog(@"@mark log: countTimesOfTransaction");
    if (transaction.step < 1) return 0;
	NSDateComponents* lastRepeat = [[NSCalendar currentCalendar] 
		components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) 
		fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:transaction.lastRepeat]];

	NSDateComponents* today = [[NSCalendar currentCalendar] 
		components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) 
		fromDate:[NSDate date]];

	NSInteger elapsedYear = today.year - lastRepeat.year;
	NSInteger elapsedMonth = (today.month - lastRepeat.month) + elapsedYear * MONTHS_PER_YEAR;
	
	NSDate* lastRepeatWithBeginningOfDay = [[NSCalendar currentCalendar] dateFromComponents:lastRepeat];
	NSInteger elapsedDay = (int)([[NSCalendar currentCalendar] dateFromComponents:today].timeIntervalSinceReferenceDate -
                                 lastRepeatWithBeginningOfDay.timeIntervalSinceReferenceDate) / SECONDS_PER_DAY;
	NSInteger timesOfTransactions = 0;
    
	if (transaction.timeMode == MODE_DAY) {
		timesOfTransactions = elapsedDay / transaction.step;
	} else if (transaction.timeMode == MODE_MONTH) {
        if (transaction.modeRepeatMonth == MODE_15TH && today.day < 15)
            elapsedMonth -= 1;
        timesOfTransactions = elapsedMonth / transaction.step;
	} else if (transaction.timeMode == MODE_YEAR) {
        timesOfTransactions = elapsedYear / transaction.step;
	} 
    return timesOfTransactions > 0 ? timesOfTransactions : 0;
}

-(NSInteger)compareDateComponents:(NSDateComponents*) dateComps1 with:(NSDateComponents*) dateComps2 {
    if (dateComps2.year > dateComps1.year) {
        return 2;
    }
    if (dateComps2.year < dateComps1.year) {
        return 1;
    }
    if (dateComps2.month > dateComps1.month) {
        return 2;
    }
    if (dateComps2.month < dateComps1.month) {
        return 1;
    }
    if (dateComps2.day > dateComps1.day) {
        return 2;
    }
    if (dateComps2.day < dateComps1.day) {
        return 1;
    }
    return 0;
}

-(NSInteger)calculateElapsedMonthsFromDateComponents:(NSDateComponents*)components1 toDateComponents:(NSDateComponents*)components2 {
    return (components2.year - components1.year) * 12 + (components2.month - components1.month);
}

-(NSDateComponents*)increaseDateComponents:(NSDateComponents*)components withNumberOfMonths:(NSInteger)months {
    NSDateComponents* retVal = [[NSDateComponents alloc] init];
    retVal.day = components.day;
    retVal.month = components.month;
    retVal.year = components.year;
    components.month += months;
    NSInteger elapsedYear = (retVal.month - 1) / (MONTHS_PER_YEAR);
    retVal.month %= MONTHS_PER_YEAR;
    if (retVal.month == 0) retVal.month = 12;
    retVal.year += elapsedYear;
    return retVal;
}

-(NSDateComponents*)getLastRepeatForTransactionWithStartingDate:(NSDateComponents*)startingDateComps withTimeMode:(NSInteger)timeMode  withStep:(NSInteger)step withModeRepeatMonth:(NSInteger)modeRepeatMonth {
    NSDateComponents* initDayComponents = [self getStartDateComponents:startingDateComps withTimeMode:timeMode withStep:step withModeRepeatMonth:modeRepeatMonth];
    if (timeMode == MODE_DAY) {
        NSInteger beginningPickedDayInterval = [[NSCalendar currentCalendar] dateFromComponents:startingDateComps].timeIntervalSinceReferenceDate;

        beginningPickedDayInterval -= step * SECONDS_PER_DAY;
        initDayComponents = [[NSCalendar currentCalendar]
                    components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                    fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:beginningPickedDayInterval]];

        return initDayComponents;
    } else if (timeMode == MODE_MONTH) {
        initDayComponents = [self increaseDateComponents:initDayComponents withNumberOfMonths:-step];
			return initDayComponents;
    } else if (timeMode == MODE_YEAR) {
        initDayComponents.year -= step;
			return initDayComponents;
    }
    return nil;
}

-(NSDateComponents*)getNextDayComponents:(NSDateComponents*)lastDateComps withTimeMode:(NSInteger)timeMode withStep:(NSInteger)step withModeRepeatMonth:(NSInteger)modeRepeatMonth {
    NSDateComponents* nextDayComps = [[NSDateComponents alloc] init];
    nextDayComps.day = lastDateComps.day;
    nextDayComps.month = lastDateComps.month;
    nextDayComps.year = lastDateComps.year;
    if (timeMode == MODE_DAY) {
        NSInteger beginninglastDateInterval = [[NSCalendar currentCalendar] dateFromComponents:lastDateComps].timeIntervalSinceReferenceDate;
        
        beginninglastDateInterval += step * SECONDS_PER_DAY;
        nextDayComps = [[NSCalendar currentCalendar]
                             components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                             fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:beginninglastDateInterval]];
        
        return nextDayComps;
    } else if (timeMode == MODE_MONTH) {
        nextDayComps = [self increaseDateComponents:nextDayComps withNumberOfMonths:step];
        return nextDayComps;
    } else if (timeMode == MODE_YEAR) {
        nextDayComps.year += step;
        return nextDayComps;
    }
    return nil;
}

-(NSDateComponents*)getStartDateComponents:(NSDateComponents*)pickedDayComps withTimeMode:(NSInteger)timeMode  withStep:(NSInteger)step withModeRepeatMonth:(NSInteger)modeRepeatMonth {
    NSDateComponents* todayComps = [[NSCalendar currentCalendar]
                                    components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                                    fromDate:[NSDate date]];
    
    NSDateComponents* retVal = nil;
    NSInteger check = [self compareDateComponents:pickedDayComps with:todayComps];
    
    if (timeMode == MODE_DAY) {
        if (check == 1) {
            retVal = [[NSDateComponents alloc] init];
            retVal.day = pickedDayComps.day;
            retVal.month = pickedDayComps.month;
            retVal.year = pickedDayComps.year;
        } else if (check == 2) {
            NSInteger beginningTodayInterval = [[NSCalendar currentCalendar] dateFromComponents:todayComps].timeIntervalSinceReferenceDate;
            NSInteger beginningPickedDayInterval = [[NSCalendar currentCalendar] dateFromComponents:pickedDayComps].timeIntervalSinceReferenceDate;
            NSInteger elaspsedSteps = ceil((beginningTodayInterval - beginningPickedDayInterval) / (1.0 * SECONDS_PER_DAY * step));

            retVal = [[NSCalendar currentCalendar]
                    components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                    fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:beginningPickedDayInterval + elaspsedSteps * step * SECONDS_PER_DAY]];
            if ([self compareDateComponents:todayComps with:retVal] == 0) {

                retVal.day += step;
            }
        } else if (check == 0) {
            NSInteger beginningTodayInterval = [[NSCalendar currentCalendar] dateFromComponents:todayComps].timeIntervalSinceReferenceDate;
            retVal = [[NSCalendar currentCalendar]
                      components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear)
                      fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:beginningTodayInterval +  step * SECONDS_PER_DAY]];
        }
    }
    
    else if (timeMode == MODE_MONTH) {
        if (check == 0 || check == 1) {
            retVal = [[NSDateComponents alloc] init];
            retVal.day = pickedDayComps.day;
            retVal.month = pickedDayComps.month;
            retVal.year = pickedDayComps.year;
        } else if (check == 2) {
            NSInteger elapsedMonths = [self calculateElapsedMonthsFromDateComponents:pickedDayComps toDateComponents:todayComps];
            NSInteger elapsedSteps = fmax(ceil(elapsedMonths * 1.0 / step), 1);
            retVal = [self increaseDateComponents:pickedDayComps withNumberOfMonths:elapsedSteps * step];
        }
        
        if (retVal && retVal.month == todayComps.month && retVal.year == todayComps.year) {
            if (modeRepeatMonth == MODE_1ST) {
                retVal.month += 1;
                retVal.day = 1;
            } else if (modeRepeatMonth == MODE_15TH) {
                retVal.day = 15;
                if (todayComps.day >= 15) {
                    retVal.month += 1;
                }
            }
        }
    }
    
    else if (timeMode == MODE_YEAR) {
//        NSLog(@"Mode năm nhé");
        if (check == 1) {
            retVal = [[NSDateComponents alloc] init];
            retVal.day = pickedDayComps.day;
            retVal.month = pickedDayComps.month;
            retVal.year = pickedDayComps.year;

        } else if (check == 0) {
					retVal = [[NSDateComponents alloc] init];
            retVal.day = pickedDayComps.day;
            retVal.month = pickedDayComps.month;
            retVal.year = pickedDayComps.year + step;

        } else if (check == 2) {
					retVal = [[NSDateComponents alloc] init];
            NSInteger elapsedSteps = fmax(ceil((todayComps.year - pickedDayComps.year) * 1.0 / step), 1);
            retVal.day = pickedDayComps.day;
            retVal.month = pickedDayComps.month;
            retVal.year = pickedDayComps.year + elapsedSteps * step;
            if ([self compareDateComponents:todayComps with:retVal] == 0) {
                retVal.year += step;
            }
        }
    }
    
    return retVal;
}


// -(void)printScheduledTransactions {
// 	NSLog(@"@mark log: printScheduledTransaction");
// 	self.transactions = [[SQLiteManager database] getScheduledTransactions];
// 	for (int i = 0; i < [self.transactions count]; i++) {
// 		NSLog(@"Transactions number %d = %f, category = %ld", i, 
// 			((RecurringTransaction*)[self.transactions objectAtIndex:i]).amount,
// 			(long)((RecurringTransaction*)[self.transactions objectAtIndex:i]).cateId);
// 	}
// }


@end
