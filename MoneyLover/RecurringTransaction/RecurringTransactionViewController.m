//
//  RecurringTransactionViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/10/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "RecurringTransactionViewController.h"
#import "AddRecurringTransactionController.h"
#import "ContentAddRecurringTransactionVC.h"
#import "RecurringTransactionCell.h"
#import "SQLiteManager.h"
#import "RecurringTransactionManager.h"
#import "OptionTableForSavingCellViewController.h"
#import "FPPopoverController.h"


@interface RecurringTransactionViewController () <UITableViewDataSource, UITableViewDelegate, AddRecurringTransactionDelegate, OptionTableForSavingCellViewControllerDelegate, RecurringCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *recurringTransactionArray;
@property (strong, nonatomic) FPPopoverController *popoverViewController;
@property (strong, nonatomic) RecurringTransaction *selectedRecurringTrans;


@property (strong, nonatomic) UILabel *emptyDataLabel;


@end


@implementation RecurringTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	[self.tableView registerNib:[UINib nibWithNibName:@"RecurringTransactionCell" bundle:nil] forCellReuseIdentifier:@"RecurringTransactionCellIden"];

	self.recurringTransactionArray = [[SQLiteManager database] getScheduledTransactions];
	
	
	
	self.emptyDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, self.tableView.frame.size.height/2, 100, 50)];
	self.emptyDataLabel.text = @"Không có dữ liệu";
	[self.emptyDataLabel setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel sizeToFit];
	[self.view addSubview:self.emptyDataLabel];
	
	
	[self updateTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction

- (IBAction)showSlideMenu:(id)sender {
		[self.delegate showSlideMenu];
}

- (IBAction)addRecurringTransaction:(id)sender {

		AddRecurringTransactionController *addRecurringVC = [[AddRecurringTransactionController alloc] initWithNibName:@"AddRecurringTransactionController" bundle:nil];
	
		addRecurringVC.editedRecurringTransaction = nil;
		addRecurringVC.delegate = self;
	
		[self.navigationController pushViewController:addRecurringVC animated:YES];

}


#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	
		return self.recurringTransactionArray.count;
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
		return 1;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	return 186;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
		RecurringTransactionCell *cell = (RecurringTransactionCell *)[tableView dequeueReusableCellWithIdentifier:@"RecurringTransactionCellIden"];
	
		RecurringTransaction *recurringTransaction = (RecurringTransaction *)self.recurringTransactionArray[indexPath.section];
		cell.image_view.image = [UIImage imageNamed:recurringTransaction.categoryTransaction.iconName];
		cell.nameLabel.text = recurringTransaction.categoryTransaction.name;
		cell.moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(recurringTransaction.amount)];
		cell.noteLabel.text = recurringTransaction.note;
		cell.delegate = self;
		cell.recurringTransaction = recurringTransaction;
	
		NSDateComponents* lastRepeatComps = [[NSCalendar currentCalendar]
												components:NSCalendarUnitYear|	NSCalendarUnitMonth|NSCalendarUnitDay
												fromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:recurringTransaction.lastRepeat]];
	
//		NSLog(@"@mark đang test đây: %ld/%ld/%ld", lastRepeatComps.day, lastRepeatComps.month, lastRepeatComps.year);

		NSDateComponents* nextDayComps = [[RecurringTransactionManager getInstance] getNextDayComponents:lastRepeatComps withTimeMode:recurringTransaction.timeMode withStep:recurringTransaction.step withModeRepeatMonth:recurringTransaction.modeRepeatMonth];
	
		cell.nextRepeatLabel.text =  [NSString stringWithFormat:@"Lần kế tiếp: %ld-%ld-%ld", (long)nextDayComps.year, (long)nextDayComps.month, (long)nextDayComps.day];
	
		return cell;
	
}


#pragma mark - UITableViewDelegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
		// Chỉnh sửa giao dịch định kì vừa chọn
		AddRecurringTransactionController *addRecurringVC = [[AddRecurringTransactionController alloc] initWithNibName:@"AddRecurringTransactionController" bundle:nil];
		addRecurringVC.editedRecurringTransaction = (RecurringTransaction *)self.recurringTransactionArray[indexPath.section];
		addRecurringVC.delegate = self;
	
		[self.navigationController pushViewController:addRecurringVC animated:YES];
	
}

#pragma mark - AddRecurringTransactionDelegate

- (void)updateTableView{
	
	self.recurringTransactionArray = [[SQLiteManager database] getScheduledTransactions];
	
	[self.tableView reloadData];
	
	if(self.recurringTransactionArray.count == 0){
		
		self.tableView.hidden = YES;
		self.emptyDataLabel.hidden = NO;
		
	}else{
		
		self.tableView.hidden = NO;
		self.emptyDataLabel.hidden = YES;
		
	}
}


#pragma - RecurringCellDelegate

- (void)selectedOptionButton:(RecurringTransaction *)recurringTransaction andSender:(UIButton *)sender{
	
	self.selectedRecurringTrans = recurringTransaction;
//	NSLog(@"%@ %d", recurringTransaction.categoryTransaction.name, recurringTransaction.Id);
	OptionTableForSavingCellViewController *tableViewController = [[OptionTableForSavingCellViewController alloc] initWithNumberOfOption:2];
	
	tableViewController.delegate = self;
	
	self.popoverViewController = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverViewController.contentSize = CGSizeMake(140,90);
	self.popoverViewController.border = NO;
	self.popoverViewController.arrowDirection = FPPopoverArrowDirectionVertical;
	self.popoverViewController.tint = FPPopoverLightGrayTint;
	
	[self.popoverViewController presentPopoverFromView:sender];
	
}

#pragma mark - OptionTableForSavingCellViewControllerDelegate

- (void)selectedOptionAtIndex:(NSInteger)index{
		//	NSLog(@"%d", index);
	[self.popoverViewController dismissPopoverAnimated:YES];
	
		
	if (index == 0) {
		
		AddRecurringTransactionController *addRecuringTrans = [[AddRecurringTransactionController alloc] initWithNibName:@"AddRecurringTransactionController" bundle:nil];
		addRecuringTrans.delegate = self;
		addRecuringTrans.editedRecurringTransaction = self.selectedRecurringTrans;
		
		[self.navigationController pushViewController:addRecuringTrans animated:YES];
		
	}else{
		
		if(index == 1){
			
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo" message:@"Bạn có chắc muốn xoá giao dịch định kì này không?" delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có", nil];
			[alertView show];
			
		}
		
	}
		
	
	
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(buttonIndex == 1){

		[[SQLiteManager database] deleteRecurringTransaction:self.selectedRecurringTrans];
		
//		self.recurringTransactionArray = [[SQLiteManager database] getScheduledTransactions];
		
		[self updateTableView];

	}
	
}









@end


