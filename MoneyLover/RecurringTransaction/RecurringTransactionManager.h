


#import <Foundation/Foundation.h>
@class RecurringTransaction;
@interface RecurringTransactionManager: NSObject

+(id)getInstance;
-(void)doScheduledTransactions;
//-(NSDateComponents*)getDateOfTransaction:(RecurringTransaction*)transaction withFactor:(NSInteger)factor;
-(NSDateComponents*)getNextDayComponents:(NSDateComponents*)lastDateComps
                            withTimeMode:(NSInteger)timeMode
                                withStep:(NSInteger)step
                     withModeRepeatMonth:(NSInteger)modeRepeatMonth;

-(NSDateComponents*)getStartDateComponents:(NSDateComponents*)pickedDayComps
                            withTimeMode:(NSInteger)timeMode
                                withStep:(NSInteger)step
                     withModeRepeatMonth:(NSInteger)modeRepeatMonth;

-(NSDateComponents*)getLastRepeatForTransactionWithStartingDate:(NSDateComponents*)startingDateComps
								withTimeMode:(NSInteger)timeMode  
								withStep:(NSInteger)step 
								withModeRepeatMonth:(NSInteger)modeRepeatMonth;
@end