#import <Foundation/Foundation.h>
#import "CategoryTransaction.h"


@interface RecurringTransaction : NSObject

@property (assign, nonatomic) NSInteger Id;
@property (strong, nonatomic) CategoryTransaction *categoryTransaction;
@property (assign, nonatomic) long long amount;
@property (copy, nonatomic) NSString *note;

@property (assign, nonatomic) NSInteger timeMode;
@property (assign, nonatomic) NSInteger step;
@property (assign, nonatomic) NSInteger numberOfEvents;

@property (assign, nonatomic) NSInteger durationMode;

@property (assign, nonatomic) NSInteger untilDate;
@property (assign, nonatomic) NSInteger startDate;
//@property (copy, nonatomic) NSString* checkedWeekDay;
@property (assign, nonatomic) NSInteger modeRepeatMonth;
@property (assign, nonatomic) NSInteger lastRepeat;
@property (strong, nonatomic) NSDateComponents* nextTime;
@end