//
//  RecurringTransactionCell.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/13/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "RecurringTransactionCell.h"

@implementation RecurringTransactionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)selectOption:(id)sender {
	
	[self.delegate selectedOptionButton:self.recurringTransaction andSender:sender];
	
}

@end
