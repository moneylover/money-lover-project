//
//  Calendar.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CKCalendarView.h"
#import <UIKit/UIKit.h>

@protocol CalendarDelegate <NSObject>
@required
- (void)selectedDate:(NSDate*)date;

@end

@interface Calendar : UIViewController{
	CKCalendarView *_ckCalendarView;
}

@property (weak, nonatomic) id <CalendarDelegate> delegate;

+ (Calendar*)getInstanceWithParentViewController:(UIViewController*)parentViewController;
- (void)showCalendar;
- (void)hideCalendar;

@end
