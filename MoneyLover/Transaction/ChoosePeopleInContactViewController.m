//
//  ChoosePeopleInContactViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ChoosePeopleInContactViewController.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>



@interface ChoosePeopleInContactViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation ChoosePeopleInContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_tableNameView.delegate = self;
	_tableNameView.dataSource = self;
//	_tableViewDataSource = [[NSMutableArray alloc] init];
	[_tableNameView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	
	
	ABAddressBookRef addressBook = ABAddressBookCreate();
	
	__block BOOL accessGranted = NO;
	
	if (ABAddressBookRequestAccessWithCompletion != NULL) { // We are on iOS 6
		dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
		
		ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
			accessGranted = granted;
			dispatch_semaphore_signal(semaphore);
		});
		
		dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//		dispatch_release(semaphore);
	}else { // We are on iOS 5 or Older
		accessGranted = YES;
		[self getContactsWithAddressBook:addressBook];
	}
	
	if (accessGranted) {
		[self getContactsWithAddressBook:addressBook];
	}
}

- (void)getContactsWithAddressBook:(ABAddressBookRef )addressBook {
	
	_tableViewDataSource = [[NSMutableArray alloc] init];
	
	CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
	CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
	
	for (int i = 0; i < nPeople; i++) {
		
		ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
//		CFStringRef firstName = nil;
//		CFStringRef  lastName = nil;
//		firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
//		lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
		[_tableViewDataSource addObject:[NSString stringWithFormat:@"%@ %@", ABRecordCopyValue(ref, kABPersonFirstNameProperty), ABRecordCopyValue(ref, kABPersonLastNameProperty)]];
//		firstName = nil;
//		lastName = nil;
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_tableViewDataSource count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
	cell.textLabel.text = (NSString*)_tableViewDataSource[indexPath.row];
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
		cell.accessoryType = UITableViewCellAccessoryNone;
	}else{
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
}

- (IBAction)doneAction:(id)sender {
	NSMutableArray *selectedNameArray = [[NSMutableArray alloc] init];
	
	for(int i = 0; i < [_tableViewDataSource count]; i++){
		NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
		UITableViewCell *cell = [_tableNameView cellForRowAtIndexPath:newIndexPath];
		
		if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
			[selectedNameArray addObject:cell.textLabel.text];
		}
	}
//	NSLog(@"%@", selectedNameArray);
	[self.delegate choosePeoplesName:selectedNameArray];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backAction:(id)sender {

	[self.navigationController popViewControllerAnimated:YES];
}

@end
