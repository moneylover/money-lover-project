//
//  ChooseCategoryViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ChooseCategoryViewController.h"
#import "SQLiteManager.h"
#import "CategoryTransaction.h"



@interface ChooseCategoryViewController () <UITableViewDataSource, UITableViewDelegate , UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *incomeCategoryArray;
@property (strong, nonatomic) NSMutableArray *expenseCategoryArray;

@property (strong, nonatomic) UITableView *expenseCategoryTableView;
@property (strong, nonatomic) UITableView *incomeCategoryTableView;

@end

@implementation ChooseCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_incomeCategoryArray = [[NSMutableArray alloc] init];
	_expenseCategoryArray = [[NSMutableArray alloc] init];
	_incomeCategoryArray = [[SQLiteManager database] getCategorysTransactionIsEarningType:YES];
	_expenseCategoryArray = [[SQLiteManager database] getCategorysTransactionIsEarningType:NO];
	
	
	CGFloat tableViewWidth = [[UIScreen mainScreen] bounds].size.width;
	CGFloat tableViewHeight = [UIScreen mainScreen].bounds.size.height - 109;
	
	[self.scrollView setContentSize:CGSizeMake(tableViewWidth*2, tableViewHeight)];
	self.scrollView.delegate = self;
	
	self.expenseCategoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableViewWidth, tableViewHeight) style:UITableViewStylePlain];
	self.incomeCategoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewWidth, 0.0f, tableViewWidth, tableViewHeight) style:UITableViewStylePlain];
	self.expenseCategoryTableView.delegate = self;
	self.incomeCategoryTableView.delegate = self;
	self.expenseCategoryTableView.dataSource = self;
	self.incomeCategoryTableView.dataSource = self;
	self.expenseCategoryTableView.tag = 1;
	self.incomeCategoryTableView.tag = 2;
	[self.expenseCategoryTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	[self.incomeCategoryTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	self.expenseCategoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.incomeCategoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
//	_expenseCategoryTableView.backgroundColor = [UIColor redColor];
	
	[self.scrollView addSubview:_incomeCategoryTableView];
	[self.scrollView addSubview:_expenseCategoryTableView];
//	self.scrollView.backgroundColor = [UIColor greenColor];
	self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tableViewHeight - 5, 0);
	
	[_incomeButton addTarget:self action:@selector(incomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	[_expenseButton addTarget:self action:@selector(expenseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	
}

- (void)incomeButtonAction:(UIButton*)button{
	[_scrollView setContentOffset:CGPointMake(self.incomeCategoryTableView.frame.size.width, 0) animated:YES];
}

- (void)expenseButtonAction:(UIButton*)button{
	[_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(tableView.tag == 1){
		return [_expenseCategoryArray count];
	}else{
		return [_incomeCategoryArray count];
	}
	return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell;
	cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
	CategoryTransaction *categoryTransaction;
	if(tableView.tag == 1){
		categoryTransaction = (CategoryTransaction*)_expenseCategoryArray[indexPath.row];
	}else{
		categoryTransaction = (CategoryTransaction*)_incomeCategoryArray[indexPath.row];
	}

	cell.imageView.image = [UIImage imageNamed:categoryTransaction.iconName];
	cell.textLabel.text = categoryTransaction.name;
	
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	if(tableView.tag == 1){
		[self.delegate selectedCategoryTransaction:(CategoryTransaction*)_expenseCategoryArray[indexPath.row]];
	}else{
		[self.delegate selectedCategoryTransaction:(CategoryTransaction*)_incomeCategoryArray[indexPath.row]];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	if(scrollView.contentOffset.x == 0){
		_incomeButton.titleLabel.textColor = [UIColor grayColor];
		_expenseButton.titleLabel.textColor = [UIColor redColor];
	}else{
		if(scrollView.contentOffset.x == scrollView.contentSize.width/2){
			_incomeButton.titleLabel.textColor = [UIColor blueColor];
			_expenseButton.titleLabel.textColor = [UIColor grayColor];
		}
	}
}



- (IBAction)backAction:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}




@end
