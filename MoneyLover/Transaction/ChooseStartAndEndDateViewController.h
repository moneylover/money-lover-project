//
//  ChooseStartAndEndDateViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/3/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ChooseCustomDateProtocol

@required
- (void)chooseStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate;

@end

@interface ChooseStartAndEndDateViewController : UIViewController

@property (weak, nonatomic) id <ChooseCustomDateProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;


@end
