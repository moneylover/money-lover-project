//
//  TransactionInterestRateManager.h
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/12/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#ifndef MoneyLover_TransactionInterestRateManager_h
#define MoneyLover_TransactionInterestRateManager_h
@class Transaction;

@interface TransactionInterestRateManager : NSObject
+(id)getInstance;
-(long long)getInterestForTransaction:(Transaction*)transaction afterNumberOfMonths:(NSInteger)months;
@end

#endif
