//
//  ChooseStartAndEndDateViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/3/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ChooseStartAndEndDateViewController.h"
//#import "MDDatePickerDialog.h"
#import "CKCalendarView.h"


static BOOL isChooseStartDateLabel = NO;

@interface ChooseStartAndEndDateViewController () <CKCalendarDelegate>
@property (strong, nonatomic) CKCalendarView *calendarView;
@property (strong, nonatomic) UIView *calendarViewBackground;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@end

@implementation ChooseStartAndEndDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	self.startDateLabel.tag = 1;
	self.endDateLabel.tag = 2;
	
	self.cancelButton.layer.cornerRadius = 6;
	self.cancelButton.layer.borderWidth = 1;
	self.cancelButton.layer.borderColor = [UIColor blueColor].CGColor;
	[self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
	
	self.completeButton.layer.cornerRadius = 6;
	self.completeButton.layer.borderWidth = 1;
	self.completeButton.layer.borderColor = [UIColor blueColor].CGColor;
	[self.completeButton addTarget:self action:@selector(completeAction:) forControlEvents:UIControlEventTouchUpInside];

	
	UITapGestureRecognizer *tapGes_1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesForStartDate:)];
	
	UITapGestureRecognizer *tapGes_2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesForEndDate:)];
	[self.startDateLabel addGestureRecognizer:tapGes_1];
	[self.endDateLabel addGestureRecognizer:tapGes_2];

	
	self.startDate = [NSDate date];
	self.endDate   = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd/MM/yyy EEEE"];
	self.startDateLabel.text = [dateFormatter stringFromDate:_startDate];
	self.endDateLabel.text = [dateFormatter stringFromDate:_endDate];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	self.calendarViewBackground = [[UIView alloc] initWithFrame:self.view.frame];
	self.calendarViewBackground.backgroundColor = [UIColor clearColor];
	self.calendarViewBackground.alpha = 0;

	UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCalendar:)];
	[self.calendarViewBackground addGestureRecognizer:tapGes];
	
	UIView *tempt = [[UIView alloc] initWithFrame:self.view.frame];
	tempt.backgroundColor = [UIColor blackColor];
	tempt.alpha = 0.8;
	[self.calendarViewBackground addSubview:tempt];
	
	self.calendarView = [[CKCalendarView alloc] initWithStartDay:startMonday];
	self.calendarView.delegate = self;
	self.calendarView.center = [self.calendarViewBackground center];
	
	[self.calendarViewBackground addSubview:self.calendarView];
	[self.view addSubview:self.calendarViewBackground];
}

- (void)hideCalendar:(id)sender{
	[UIView animateWithDuration:0.5 animations:^{
		self.calendarViewBackground.alpha = 0;
	}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleTapGesForStartDate:(UITapGestureRecognizer*)tapGes{

	isChooseStartDateLabel = YES;
	[UIView animateWithDuration:0.5 animations:^{
		self.calendarViewBackground.alpha = 1;
	}];

	NSLog(@"handle tapGes for startdate");
}

- (void)handleTapGesForEndDate:(UITapGestureRecognizer*)tapGes{

	isChooseStartDateLabel = NO;
	[UIView animateWithDuration:0.5 animations:^{
		self.calendarViewBackground.alpha = 1;
	}];
	NSLog(@"handle tapGes for enddate");
}

- (void)cancelAction:(UIButton*)button{
	[self.navigationController popViewControllerAnimated:YES];

}

- (void)completeAction:(UIButton*)button{
	
	if([_startDate compare:_endDate] == NSOrderedDescending){
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo" message:@"Ngày bắt đầu phải trước ngày kết thúc" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Đóng", nil];
		[alertView show];
		return;
	}
	[self.delegate chooseStartDate:_startDate andEndDate:_endDate];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date{
	
//	NSDate *newDate = [date dateByAddingTimeInterval:60*60*24];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setDateFormat:@"dd/MM/yyy EEEE"];

	if(isChooseStartDateLabel == YES){
		self.startDate = date;
		self.startDateLabel.text = [dateFormatter stringFromDate:date];
	}else{
		self.endDateLabel.text = [dateFormatter stringFromDate:date];
		self.endDate = date;
	}
	[UIView animateWithDuration:0.5 animations:^{
		self.calendarViewBackground.alpha = 0;
	}];
	

}



- (void)dealloc{
	NSLog(@"Choose start date and end date view controller dealloc");
}

@end
