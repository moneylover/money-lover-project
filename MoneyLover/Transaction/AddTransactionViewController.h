//
//  AddTransactionViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

@protocol AddTransactionProtocol

@required
- (void)updatateData;

@end

@interface AddTransactionViewController : UIViewController

@property (weak, nonatomic) id <AddTransactionProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIButton *chooseCategoryButton;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;
@property (weak, nonatomic) IBOutlet UIButton *withWhoButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseDateButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) Transaction *editedTransaction;
@property (weak, nonatomic) IBOutlet UIButton *deleteTransactionButton;

@end
