//
//  OptionTableViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/8/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol OptionTableViewControllerProtocol

@required
- (void)selectRowAtIndex:(NSInteger)index;

@end


@interface OptionTableViewController : UITableViewController

@property (weak, nonatomic) id <OptionTableViewControllerProtocol> delegate;

@end
