//
//  CustomScrollView.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/26/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "CustomScrollView.h"

@implementation CustomScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:
(UIGestureRecognizer *)otherGestureRecognizer{
//	NSLog(@"shouldRecognizeSimultaneouslyWithGestureRecognizer");
	UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
	CGPoint startPoint;
	if([panGesture respondsToSelector:@selector(locationInView:)]){
		startPoint = [panGesture locationInView:self];
		if(startPoint.x > ([[UIScreen mainScreen] bounds].size.width)*2.0 - 50){
			return	NO;
		}else{
			return YES;
		}
	}
	return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
//	NSLog(@"gestureRecognizerShouldBegin");
	UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
	CGPoint startPoint;
	BOOL swipeHorizontal = NO;
	
	if([panGesture respondsToSelector:@selector(velocityInView:)]){
		CGPoint velocity = [panGesture velocityInView:self];
		swipeHorizontal = fabs(velocity.x) > fabs(2*velocity.y);
	}
	if([panGesture respondsToSelector:@selector(locationInView:)]){
		startPoint = [panGesture locationInView:self];

		if((swipeHorizontal == YES)&&(startPoint.x > [[UIScreen mainScreen] bounds].size.width + 30)){
			return YES;
		}else{
			return NO;
		}
	}
	return YES;
}
@end
