//
//  ShowTransactionViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/24/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ShowTransactionViewController.h"
#import "SQLiteManager.h"
#import "DataSourceInASection.h"
#import "Transaction.h"
#import "OverviewCell.h"
#import "DisplayTimeCell.h"
#import "TransactionCell.h"
#import "DisplayTimeCell_ViewByTransaction.h"
#import "TransactionCell_ViewByTransaction.h"
#import "OverviewViewController.h"



@interface ShowTransactionViewController () <UITableViewDataSource, UITableViewDelegate>


@end

@implementation ShowTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//	NSLog(@"Showing transaction view did load");
//	self.activityIndicator.hidesWhenStopped = YES;
	self.transactionsArray = [[NSMutableArray alloc] init];
	
	if([[NSUserDefaults standardUserDefaults] integerForKey:@"DisplayModeByTransaction"] == 0){
		[[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"DisplayModeByTransaction"];
	}
	self.displayMode = (DisplayMode)[[NSUserDefaults standardUserDefaults] integerForKey:@"DisplayModeByTransaction"];
//	self.displayMode = SHOWINGBY_CATEGORY;
	
	[self.tableView registerNib:[UINib nibWithNibName:@"OverviewCell" bundle:nil] forCellReuseIdentifier:@"OverviewCellIden"];
	
	[self.tableView registerNib:[UINib nibWithNibName:@"TransactionCell_ViewByTransaction" bundle:nil] forCellReuseIdentifier:@"TransactionCell_ViewByTransactionIden"];
	[self.tableView registerNib:[UINib nibWithNibName:@"DisplayTimeCell_ViewByTransaction" bundle:nil] forCellReuseIdentifier:@"DisplayTimeCell_ViewByTransactionIden"];
	
	[self.tableView registerNib:[UINib nibWithNibName:@"DisplayTimeCell" bundle:nil] forCellReuseIdentifier:@"DisplayTimeCellIden"];
	[self.tableView registerNib:[UINib nibWithNibName:@"TransactionCell" bundle:nil] forCellReuseIdentifier:@"TransactionCellIden"];
	
	self.emptyDataView.hidden = YES;
	self.tableView.hidden = YES;
//	[self loadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

	self = [super initWithNibName:nibNameOrNil bundle:nil];
	if (self) {

	}
	return self;
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
//	[self loadData];
//	NSLog(@"showing transaction View Will Appear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataWithStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate{
//	NSLog(@"load data by showingVC");
	dispatch_sync(dispatch_get_main_queue(), ^{
		[self.delegate updateTotalMoneyInWallet];
		self.activityIndicatorParentView.hidden = NO;
		self.activityIndicatorParentView.alpha = 1.0;
	});
	
	self.startDate = startDate;
	self.endDate = endDate;
	self.transactionsArray = [[SQLiteManager database] getTransactionsArrayWithDisplayMode:self.displayMode startDate:startDate andEndDate:endDate];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
	[dateFormatter setDateFormat:@"dd/MM/yyy"];
	NSString *startDateString = [dateFormatter stringFromDate:self.startDate];
	NSString *endDateString   = [dateFormatter stringFromDate:self.endDate];
	NSString *timeString;
	
	
	if(startDate != nil){
		timeString = [NSString stringWithFormat:@"%@ - %@", startDateString, endDateString];
	}else{
		timeString = @"Tất cả";
	}

	dispatch_sync(dispatch_get_main_queue(), ^{
			[self.timeLabel setText:timeString];
			if([self.transactionsArray count] == 0){
				self.tableView.hidden = YES;
				self.emptyDataView.hidden = NO;
			}else{
				self.emptyDataView.hidden = YES;
				self.tableView.hidden = NO;
				
				self.tableView.delegate = self;
				self.tableView.dataSource = self;
				[_tableView reloadData];
			}
//			[_tableView reloadData];

			[UIView animateWithDuration:0.5
														delay:0.2
													options:UIViewAnimationOptionBeginFromCurrentState
											 animations:^{

				self.activityIndicatorParentView.alpha = 0.0;
			}
											 completion:^(BOOL finished) {
				self.activityIndicatorParentView.hidden = YES;
			}];
			
		});
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{

	return ([self.transactionsArray count] + 1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(_displayMode == SHOWINGBY_CATEGORY){
		if(section == 0){
			return 1;
		}else{
			NSInteger tempt = ((DataSourceInASection*)self.transactionsArray[section - 1]).numberOfRowForSection + 1;
			return tempt;
		}
	}else{
		// Showing by transaction
		if(section == 0){
			return 1;
		}else{
			NSInteger tempt = ((DataSourceInASection*)self.transactionsArray[section - 1]).numberOfRowForSection + 1;

			return tempt;
		}
	}
	return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	UITableViewCell *cell;
	
	if(indexPath.section == 0){
		cell = [tableView dequeueReusableCellWithIdentifier:@"OverviewCellIden" ];
		long long income = 0;
		long long expense = 0;
		for(int i = 0; i < [self.transactionsArray count]; i ++){
			DataSourceInASection *dataSourceInASection = (DataSourceInASection*)self.transactionsArray[i];
			for(int j = 0; j < [dataSourceInASection.transactionsArray count]; j++){
				if(((Transaction*)dataSourceInASection.transactionsArray[j]).category.isEarning){
					income += ((Transaction*)dataSourceInASection.transactionsArray[j]).money;
				}else{
					expense += ((Transaction*)dataSourceInASection.transactionsArray[j]).money;
				}
			}
		}
		((OverviewCell*)cell).incomeMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:income];
		((OverviewCell*)cell).expenseMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(expense)];
		((OverviewCell*)cell).remainingMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(income + expense)];
//		NSLog(@"\n%f\n%f\n%f\n%f",income, expense, income + expense, fabs(income + expense));
		if(income + expense < 0){
			((OverviewCell*)cell).remainingMoneyLabel.textColor = [UIColor redColor];
		}else{
			((OverviewCell*)cell).remainingMoneyLabel.textColor = [UIColor blueColor];
		}
	}else{
		
		DataSourceInASection *dataSourceInASection = (DataSourceInASection*)self.transactionsArray[indexPath.section -1];
		
		if(_displayMode == SHOWINGBY_CATEGORY){
			
			if(indexPath.row == 0){
				cell = (TransactionCell*) [tableView dequeueReusableCellWithIdentifier:@"TransactionCellIden" ];

				((TransactionCell*)cell).icon.image = [UIImage imageNamed:((Transaction*)dataSourceInASection.transactionsArray[0]).category.iconName];
				((TransactionCell*)cell).categoryLabel.text = ((Transaction*)dataSourceInASection.transactionsArray[0]).category.name;
				((TransactionCell*)cell).moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(dataSourceInASection.totalMoneyInSection)];
				((TransactionCell*)cell).rightNotesLabel.text = [NSString stringWithFormat:@"%ld giao dịch", (long)[dataSourceInASection.transactionsArray count]];
				if(dataSourceInASection.totalMoneyInSection > 0){
					((TransactionCell*)cell).moneyLabel.textColor = [UIColor blueColor];
				}else{
					((TransactionCell*)cell).moneyLabel.textColor = [UIColor redColor];
				}
				
			}else{
				Transaction *transactionForThisRow = (Transaction*)(dataSourceInASection.transactionsArray[indexPath.row - 1]);
				
				cell = (DisplayTimeCell*)[tableView dequeueReusableCellWithIdentifier:@"DisplayTimeCellIden"];
//				NSLog(@"%@", transactionForThisRow.date);
				NSString *transactionDate = transactionForThisRow.date;
				NSString *dayString = [NSString stringWithFormat:@"%@, %@",[transactionDate substringToIndex:7], [transactionDate substringFromIndex:11]];
				
				((DisplayTimeCell*)cell).dateLabel.text = [transactionDate substringWithRange:NSMakeRange(8, 2)];
				((DisplayTimeCell*)cell).dayLabel.text = dayString;
				if(![transactionForThisRow.who isEqualToString:@"(null)"]){
					((DisplayTimeCell*)cell).notesLeftLabel.text = [NSString stringWithFormat:@"Với %@", transactionForThisRow.who];
				}else{
						((DisplayTimeCell*)cell).notesLeftLabel.text = @"";
				}
				((DisplayTimeCell*)cell).moneyLabel.text =  [MoneyStringFormatter stringMoneyWithLongLong:llabs(transactionForThisRow.money)];
				((DisplayTimeCell*)cell).notesRightLabel.text = transactionForThisRow.note;
				if(transactionForThisRow.category.isEarning){
					[((DisplayTimeCell*)cell).moneyLabel setTextColor:[UIColor blueColor]];
				}else{
					[((DisplayTimeCell*)cell).moneyLabel setTextColor:[UIColor redColor]];
				}
			}
			
		}else{
			//   SHOWINGBY_TRANSACTION
			
			if(indexPath.row == 0){
				Transaction *transaction = ((Transaction*)dataSourceInASection.transactionsArray[0]);
				
				cell = (DisplayTimeCell_ViewByTransaction*) [tableView dequeueReusableCellWithIdentifier:@"DisplayTimeCell_ViewByTransactionIden"];
//				NSLog(@"Transaction date: %@", transaction.date);
				((DisplayTimeCell_ViewByTransaction*)cell).dateLabel.text = [NSString stringWithFormat:@"%@", [transaction.date substringWithRange:NSMakeRange(8, 2)]];
				((DisplayTimeCell_ViewByTransaction*)cell).dayLabel.text = [transaction.date substringFromIndex:11];
				((DisplayTimeCell_ViewByTransaction*)cell).monthLabel.text = [NSString stringWithFormat:@"%@", [transaction.date substringWithRange:NSMakeRange(0, 7)]];
				((DisplayTimeCell_ViewByTransaction*)cell).moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs(dataSourceInASection.totalMoneyInSection)];
				
				if(dataSourceInASection.totalMoneyInSection > 0){
					((DisplayTimeCell_ViewByTransaction*)cell).moneyLabel.textColor = [UIColor blueColor];
				}else{
					((DisplayTimeCell_ViewByTransaction*)cell).moneyLabel.textColor = [UIColor redColor];
				}
				
			}else{
				
				Transaction *transaction = (Transaction*)(dataSourceInASection.transactionsArray[indexPath.row - 1]);
				
				cell = (TransactionCell_ViewByTransaction*)[tableView dequeueReusableCellWithIdentifier:@"TransactionCell_ViewByTransactionIden"];
				
				((TransactionCell_ViewByTransaction*)cell).iconView.image = [UIImage imageNamed:transaction.category.iconName];
				((TransactionCell_ViewByTransaction*)cell).categoryLabel.text = transaction.category.name;
				if(![transaction.who isEqualToString:@"(null)"]){
					((TransactionCell_ViewByTransaction*)cell).rightNotesLabel.text = [NSString stringWithFormat:@"Với %@", transaction.who];
				}else{
					((TransactionCell_ViewByTransaction*)cell).rightNotesLabel.text = @"";
				}

				((TransactionCell_ViewByTransaction*)cell).moneyLabel.text =  [MoneyStringFormatter stringMoneyWithLongLong:llabs(transaction.money)];
				((TransactionCell_ViewByTransaction*)cell).leftNotesLabel.text = transaction.note;
				
				if(transaction.category.isEarning){
					[((TransactionCell_ViewByTransaction*)cell).moneyLabel setTextColor:[UIColor blueColor]];
				}else{
					[((TransactionCell_ViewByTransaction*)cell).moneyLabel setTextColor:[UIColor redColor]];
				}
			}
		}
	}

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if(indexPath.section == 0){
		return 195.0;
	}else{
		return 50;
	}
	return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if(indexPath.section == 0){

		OverviewViewController *overviewVC =  [[OverviewViewController alloc] initWithNibName:@"OverviewViewController" bundle:nil transactionData:self.transactionsArray timeLabelString:self.timeLabel.text andStartDate:self.startDate];
		
		
		[self.navigationController pushViewController:overviewVC animated:YES];
		
	}else{
		if(indexPath.row == 0){
			// Thu nho cac cell
			DataSourceInASection *dataSourceInASection = (DataSourceInASection*)self.transactionsArray[indexPath.section - 1];
			if(dataSourceInASection.isShowingCellContent == YES){
				dataSourceInASection.numberOfRowForSection = 0;
				dataSourceInASection.isShowingCellContent = NO;
				[self thumbnailCell:[dataSourceInASection.transactionsArray count] inSectionIndexPath:indexPath];

			}else{
				dataSourceInASection.numberOfRowForSection = [dataSourceInASection.transactionsArray count];
				dataSourceInASection.isShowingCellContent = YES;
				[self extendCell:[dataSourceInASection.transactionsArray count] inSectionIndexPath:indexPath];
			}
		}else{
			// Hien thi chi tiet
			[self showTransactionDetailInIndexPath:indexPath];
		}
	}

}

- (void)thumbnailCell:(NSInteger)numberOfCell inSectionIndexPath:(NSIndexPath*)indexPath{
	
	NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
	for(int i = 0; i < numberOfCell; i++){
		NSIndexPath *indexPathCellThumbnailed = [NSIndexPath indexPathForRow:(i + 1) inSection:indexPath.section];
		[indexPathArray addObject:indexPathCellThumbnailed];
	}
	
	[_tableView beginUpdates];
	[_tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
	[_tableView endUpdates];
}

- (void)extendCell:(NSInteger)numberOfCell inSectionIndexPath:(NSIndexPath*)indexPath{
	
	NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
	for(int i = 0; i < numberOfCell; i++){
		NSIndexPath *indexPathCellExtended = [NSIndexPath indexPathForRow:(i + 1) inSection:indexPath.section];
		[indexPathArray addObject:indexPathCellExtended];
	}
	
	[_tableView beginUpdates];
	[_tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationBottom];
	[_tableView endUpdates];

}

- (void)changeDisplayMode{
	_displayMode = (_displayMode == 1)?2:1;

	[[NSUserDefaults standardUserDefaults] setInteger:_displayMode forKey:@"DisplayModeByTransaction"];
	self.transactionsArray = [[SQLiteManager database]
														getTransactionsArrayWithDisplayMode:self.displayMode
																											startDate:_startDate
																										 andEndDate:_endDate];
	
	[_tableView reloadData];
}

- (void)showTransactionDetailInIndexPath:(NSIndexPath*)indexPath{
	DataSourceInASection *dataSourceInASection = (DataSourceInASection*)self.transactionsArray[indexPath.section - 1];
	Transaction *showingDetailTransaction = (Transaction*)dataSourceInASection.transactionsArray[indexPath.row - 1];
	AddTransactionViewController *addTransactionVC = [[AddTransactionViewController alloc] initWithNibName:@"AddTransactionViewController" bundle:nil];
	addTransactionVC.editedTransaction = showingDetailTransaction;
	addTransactionVC.delegate = self;
	
	[self.navigationController pushViewController:addTransactionVC animated:YES];
}

- (void)updateTransactionsTable{
//	NSLog(@"showing transaction vc update table");
	[self loadDataWithStartDate:self.startDate andEndDate:self.endDate];
	
//	[_tableView reloadData];
//	dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.LoadSQLIteDataqueue", DISPATCH_QUEUE_SERIAL);
//	dispatch_async(serialQueue, ^{
//
//	});
}

- (void)updatateData{
//	NSLog(@"Showing transaction update date protocol");
		dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.LoadSQLIteDataqueue", DISPATCH_QUEUE_SERIAL);
		dispatch_async(serialQueue, ^{
			[self updateTransactionsTable];
		});

}

- (void)dealloc{
	NSLog(@"Showing transaction dealloc");
}

@end
