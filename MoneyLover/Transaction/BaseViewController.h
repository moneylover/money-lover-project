//
//  BaseViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/18/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "Foundation/Foundation.h"
#import "UIKit/UIKit.h"

@protocol BaseViewControllerProtocol <NSObject>

@required
- (void) showSlideMenu;

@optional
- (void) hideSlideMenu;
- (void) rightBarButtonAction;

@end

@interface BaseViewController: UIViewController

@property (weak, nonatomic) id <BaseViewControllerProtocol> delegate;

@end