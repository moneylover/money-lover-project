//
//  ChooseDurationTableViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/8/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ChooseDurationTableViewController.h"

@interface ChooseDurationTableViewController ()

@end

@implementation ChooseDurationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ChooseDurationCellIden"];
	self.tableView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChooseDurationCellIden" forIndexPath:indexPath];
	NSInteger currentDuration = [[NSUserDefaults standardUserDefaults] integerForKey:@"Duration"];

	switch (indexPath.row) {
  case 0:{
		cell.textLabel.textAlignment = NSTextAlignmentCenter;
		cell.textLabel.textColor = [UIColor blueColor];
		cell.textLabel.text = @"Chọn khoảng thời gian";
	}
			break;
  case 1:{
			cell.textLabel.text = @"Ngày";
	}
			break;
  case 2:{
			cell.textLabel.text = @"Tuần";
	}
			break;
  case 3:{
			cell.textLabel.text = @"Tháng";
	}
			break;
  case 4:{
			cell.textLabel.text = @"Quý";
	}
			break;
  case 5:{
			cell.textLabel.text = @"Năm";
	}
			break;
  case 6:{
			cell.textLabel.text = @"Tất cả";
	}
			break;
  case 7:{
		cell.textLabel.text = @"Tuỳ chỉnh";
	}
			break;
  default:
			break;
	}
	
	if(currentDuration != 0){
		if(indexPath.row == currentDuration){
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		}
	}
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return  35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if(indexPath.row == 0){
		return;
	}else{
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
		[self.delegate selectedDurationWithIndex:indexPath.row];
	}
}


@end
