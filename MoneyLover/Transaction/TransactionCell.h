//
//  TransactionCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/27/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightNotesLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
