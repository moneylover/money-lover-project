//
//  DisplayTimeCell_ViewByTransaction.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/27/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayTimeCell_ViewByTransaction : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
