//
//  TransactionInterestRateManager.m
//  MoneyLover
//
//  Created by Tran Hoang Duong on 11/12/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionInterestRateManager.h"
#import "Transaction.h"

@implementation TransactionInterestRateManager

+(id) getInstance {
    static TransactionInterestRateManager* instance = nil;
    static dispatch_once_t once = 0;
    dispatch_once(&once, ^{instance = [[self alloc] initPrivate];});
    return instance;
}

-(id)initPrivate {
    if ((self = [super init])) {
        
    }
    return self;
}

-(id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

/*
 A = P * (1 + r/n)^(n*t)
 A: Future amount
 P: starting amount
 r: annual nominal interest rate
 n: number of compounding times every year
 t: number of the years
 */

-(long long)getInterestForTransaction:(Transaction*)transaction afterNumberOfMonths:(NSInteger)months {
    long long P = transaction.money;
    float r = transaction.interest_rate/100.0;
    long long n = transaction.monthsPerCompounding;
    NSInteger t = months;
	long long retVal;
	
	if(transaction.compounding == 0){
		
		retVal = (long long)(P*(t*r + 1));
		NSLog(@"%lld %ld", retVal, t);
	}else{
		
		retVal = (long long)(P * pow((1 + r/n), (n * t)));
		
	}
	
	
    return retVal;
}

@end