//
//  ShowTransactionViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/24/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddTransactionViewController.h"

typedef enum {
	SHOWINGBY_NONE,
	SHOWINGBY_TRANSACTION,
	SHOWINGBY_CATEGORY
}DisplayMode;

@protocol ShowingTransactionDelegate <NSObject>

@required
- (void)updateTotalMoneyInWallet;

@end

@interface ShowTransactionViewController : UIViewController <AddTransactionProtocol>

@property (weak, nonatomic) id <ShowingTransactionDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *transactionsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorParentView;

@property (assign, nonatomic) DisplayMode displayMode;

// Empty data to display
@property (weak, nonatomic) IBOutlet UIView *emptyDataView;
@property (weak, nonatomic) IBOutlet UILabel *faceLabel;
@property (weak, nonatomic) IBOutlet UILabel *midleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;

- (void)loadDataWithStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate;
- (void)changeDisplayMode;
- (void)updateTransactionsTable;
@end
