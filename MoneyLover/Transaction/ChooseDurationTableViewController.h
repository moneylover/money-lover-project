//
//  ChooseDurationTableViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/8/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseDurationTableVCProtocol

@required
- (void)selectedDurationWithIndex:(NSInteger)index;

@end

@interface ChooseDurationTableViewController : UITableViewController

@property (weak, nonatomic) id <ChooseDurationTableVCProtocol> delegate;

@end
