//
//  ChoosePeopleInContactViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChoosePeopleInContactProtocol

@required
- (void)choosePeoplesName:(NSMutableArray*)nameArray;

@end


@interface ChoosePeopleInContactViewController : UIViewController

@property (weak, nonatomic) id <ChoosePeopleInContactProtocol> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableNameView;
@property (strong, nonatomic) NSMutableArray *tableViewDataSource;

@end
