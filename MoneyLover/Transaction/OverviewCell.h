//
//  OverviewCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/27/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverviewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *incomeMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingMoneyLabel;

@end
