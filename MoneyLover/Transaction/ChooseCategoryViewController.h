//
//  ChooseCategoryViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomScrollView.h"
//#import "CategoryTransaction.h"

@class  CategoryTransaction;

@protocol ChooseCategoryProtocol


@required
- (void)selectedCategoryTransaction:(CategoryTransaction*)categoryTransaction;

@end


@interface ChooseCategoryViewController : UIViewController

@property (weak, nonatomic) id <ChooseCategoryProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *expenseButton;
@property (weak, nonatomic) IBOutlet UIButton *incomeButton;
@property (weak, nonatomic) IBOutlet UILabel *horizontalLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
