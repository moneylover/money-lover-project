//
//  OverviewViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/7/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "OverviewViewController.h"
#import "DataSourceInASection.h"
#import "Transaction.h"
#import "PNChart.h"
#import "SQLiteManager.h"


@interface OverviewViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) PNPieChart *incomingPieChart;
@property (strong, nonatomic) PNPieChart *expensingPieChart;
@property (assign, nonatomic) long long openingBalance;
@property (assign, nonatomic) long long netIncoming;



@end

@implementation OverviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil transactionData:(NSMutableArray*)transactionsArray timeLabelString:(NSString*)timeLabelString andStartDate:(NSDate *)startDate{

	self = [super initWithNibName:nibNameOrNil bundle:nil];
	if (self) {
		self.openingBalance = 0;
		self.netIncoming = 0;
		self.expenseArray = [[NSMutableArray alloc] init];
		self.incomeArray  = [[NSMutableArray alloc] init];
		self.timeString = timeLabelString;
		self.openingBalance = [[SQLiteManager database] getAmountOfMoneyToDate:startDate];

		for(int i = 0; i < [transactionsArray count]; i ++){
			DataSourceInASection *dataSourceInASection = (DataSourceInASection*)transactionsArray[i];
			for(int j = 0; j < [dataSourceInASection.transactionsArray count]; j++){
				Transaction *transaction = ((Transaction*)dataSourceInASection.transactionsArray[j]);

				if(transaction.category.isEarning){
					
					if([self.incomeArray count] == 0){
						[self.incomeArray addObject:transaction];
					}else{
						NSInteger k;
						for(k = 0; k < [self.incomeArray count]; k++){
							Transaction *transactionToCompare = [(Transaction*)self.incomeArray[k] makeACopy];
							if(transaction.category_id == transactionToCompare.category_id){
								transactionToCompare.money += transaction.money;
								break;
							}
						}
						if(k == [self.incomeArray count]){
							[self.incomeArray addObject:transaction];
						}
					}
					
				}else{
					
					if([self.expenseArray count] == 0){
						[self.expenseArray addObject:transaction];
					}else{
						NSInteger l;
						for(l = 0; l < [self.expenseArray count]; l++){
							Transaction *transactionToCompare = [(Transaction*)self.expenseArray[l] makeACopy];
							if(transaction.category_id == transactionToCompare.category_id){
								transactionToCompare.money += transaction.money;
								break;
							}
						}
						if(l == [self.expenseArray count]){
							[self.expenseArray addObject:transaction];
						}
					}
				}
				self.netIncoming += transaction.money;
//				NSLog(@"%lld + %lld = %lld ", self.netIncoming, transaction.money, self.netIncoming += transaction.money);
			}
		}
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//	NSLog(@"Overview did load");
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	_timeLabel.text = _timeString;
	
	NSMutableArray *itemsForExpensingChart = [[NSMutableArray alloc] init];
	for(int i = 0; i < [_expenseArray count]; i++){
		Transaction *transaction = (Transaction*)self.expenseArray[i];
		[itemsForExpensingChart addObject:[PNPieChartDataItem dataItemWithValue:fabs(transaction.money) color:[PNColor getColorAtIndex:i] description:transaction.category.name]];
	}
	self.expensingPieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(10.0, 40.0, 200.0, 200.0) items:itemsForExpensingChart];
	self.expensingPieChart.showOnlyValues = YES;
	self.expensingPieChart.legendStyle = PNLegendItemStyleStacked;

	UIView *legend = [self.expensingPieChart getLegendWithMaxWidth:200];
	[legend setFrame:CGRectMake(206, 10, legend.frame.size.width, legend.frame.size.height)];
	[self.expensingPieChart addSubview:legend];


	NSMutableArray *itemsForIncomingChart = [[NSMutableArray alloc] init];
	for(int i = 0; i < [_incomeArray count]; i++){
		Transaction *transaction = (Transaction*)self.incomeArray[i];
				[itemsForIncomingChart addObject:[PNPieChartDataItem dataItemWithValue:fabs(transaction.money) color:[PNColor getColorAtIndex:26 - i] description:transaction.category.name]];
	}
	self.incomingPieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(10.0, 40.0, 200.0, 200.0) items:itemsForIncomingChart];
	self.incomingPieChart.showOnlyValues = YES;
	self.incomingPieChart.legendStyle = PNLegendItemStyleStacked;
	
	UIView *legendIncomingChart = [self.incomingPieChart getLegendWithMaxWidth:200];
	[legendIncomingChart setFrame:CGRectMake(206, 10, legendIncomingChart.frame.size.width, legendIncomingChart.frame.size.height)];
	[self.incomingPieChart addSubview:legendIncomingChart];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	NSInteger numberOfRow = 1;
	
	switch (section) {
  case 0:{
		numberOfRow = 2;
	}
			break;
  case 1:{
		numberOfRow = 1;
	}
			break;
  case 2:{
		numberOfRow = 1 + [_expenseArray count];
	}
			break;
  case 3:{
		numberOfRow = 1 + [_incomeArray count];
	}
			break;

  default:
			break;
	}

	return numberOfRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	CGFloat height = 50;

	if(indexPath.section == 2 && ([_expenseArray count] != 0)){
		if(indexPath.row == 0 ){
			height = 250;
		}
	}
	if(indexPath.section == 3 && ([_incomeArray count] != 0)){
		if(indexPath.row == 0 ){
			height = 250;
		}
	}
	return height;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
	
	UITableViewCell *cell =  [[UITableViewCell alloc] init];

	UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 150, 23)];
	moneyLabel.textAlignment = NSTextAlignmentRight;
	cell.accessoryView = moneyLabel;
	long long moneyValue = 0;
	switch (indexPath.section) {
  case 0:{

		if(indexPath.row == 0){
			cell.textLabel.text = @"Số dư đầu";
			moneyValue  = self.openingBalance;
		}else{
			cell.textLabel.text = @"Số dư cuối";
			moneyValue = self.openingBalance + self.netIncoming;
		}
	}
			break;
  case 1:{
		cell.textLabel.text = @"Thu nhập ròng";
		moneyValue = self.netIncoming;

	}
			break;
  case 2:{

		if([self.expenseArray count] > 0){
			if(indexPath.row == 0){
				
				long long totalMoneyInArray = 0;
				for(id interator in self.incomeArray){
					totalMoneyInArray += ((Transaction*)interator).money;
				}
				UILabel *expenseLabel = [[UILabel alloc] init];
				expenseLabel.text = @"Chi tiêu";
				
				UILabel *expenseMoneyLabel = [[UILabel alloc] init];
				expenseMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:totalMoneyInArray];
				expenseMoneyLabel.textColor = [UIColor redColor];
				[cell addSubview:expenseLabel];
				[cell addSubview:expenseMoneyLabel];
				
				expenseLabel.translatesAutoresizingMaskIntoConstraints = NO;
				NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(expenseLabel);
				
				NSMutableArray *constraintArray = [[NSMutableArray alloc] init];
				[constraintArray addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[expenseLabel]" options:0 metrics:nil views:viewDictionary]];
				[constraintArray addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[expenseLabel]" options:0 metrics:nil views:viewDictionary]];
				[cell addConstraints:constraintArray];
				
				expenseMoneyLabel.translatesAutoresizingMaskIntoConstraints = NO;
				NSDictionary *viewDictionary_2 = NSDictionaryOfVariableBindings(expenseMoneyLabel);
				
				NSMutableArray *constraintArray_2 = [[NSMutableArray alloc] init];
				[constraintArray_2 addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[expenseMoneyLabel]-10-|" options:0 metrics:nil views:viewDictionary_2]];
				[constraintArray_2 addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[expenseMoneyLabel]" options:0 metrics:nil views:viewDictionary_2]];
				[cell addConstraints:constraintArray_2];
				
				
				[self.expensingPieChart removeFromSuperview];
				[cell addSubview:self.expensingPieChart];
				[self.expensingPieChart strokeChart];
				cell.accessoryView = nil;
			}else{
				cell.textLabel.text = ((Transaction*)self.expenseArray[indexPath.row - 1]).category.name;
				moneyValue = ((Transaction*)self.expenseArray[indexPath.row - 1]).money;
				cell.imageView.image =  [UIImage imageNamed:((Transaction*)self.expenseArray[indexPath.row - 1]).category.iconName];
			}
			
		}else{
			
			cell.textLabel.text = @"Chi tiêu";
			moneyValue = 0;
			moneyLabel.textColor = [UIColor redColor];
			moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:moneyValue*(moneyValue<0?-1:1)];
			return cell;
			
		}
	}
			break;
  case 3:{
		if([self.incomeArray count] > 0){
			if(indexPath.row == 0){
				//For Line Chart
				long long totalMoneyInArray = 0;
				for(id interator in self.incomeArray){
					totalMoneyInArray += ((Transaction*)interator).money;
				}
				UILabel *incomingLabel = [[UILabel alloc] init];
				incomingLabel.text = @"Thu nhập";
				
				UILabel *incomingMoneyLabel = [[UILabel alloc] init];
				incomingMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:totalMoneyInArray];
				incomingMoneyLabel.textColor = [UIColor blueColor];
				[cell addSubview:incomingLabel];
				[cell addSubview:incomingMoneyLabel];
				
				incomingLabel.translatesAutoresizingMaskIntoConstraints = NO;
				NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(incomingLabel);
				
				NSMutableArray *constraintArray = [[NSMutableArray alloc] init];
				[constraintArray addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[incomingLabel]" options:0 metrics:nil views:viewDictionary]];
				[constraintArray addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[incomingLabel]" options:0 metrics:nil views:viewDictionary]];
				[cell addConstraints:constraintArray];
			
				incomingMoneyLabel.translatesAutoresizingMaskIntoConstraints = NO;
				NSDictionary *viewDictionary_2 = NSDictionaryOfVariableBindings(incomingMoneyLabel);
				
				NSMutableArray *constraintArray_2 = [[NSMutableArray alloc] init];
				[constraintArray_2 addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[incomingMoneyLabel]-10-|" options:0 metrics:nil views:viewDictionary_2]];
				[constraintArray_2 addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[incomingMoneyLabel]" options:0 metrics:nil views:viewDictionary_2]];
				[cell addConstraints:constraintArray_2];
				
				
				[self.incomingPieChart removeFromSuperview];
				[cell addSubview:self.incomingPieChart];
				[self.incomingPieChart strokeChart];
				cell.accessoryView = nil;
			}else{
				cell.textLabel.text = ((Transaction*)self.incomeArray[indexPath.row - 1]).category.name;
				moneyValue = ((Transaction*)self.incomeArray[indexPath.row - 1]).money;
				cell.imageView.image =  [UIImage imageNamed:((Transaction*)self.incomeArray[indexPath.row - 1]).category.iconName];
				
			}
		}else{
			cell.textLabel.text = @"Thu nhập";
			moneyValue = 0;
			moneyLabel.textColor = [UIColor blueColor];
			moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:moneyValue*(moneyValue<0?-1:1)];
			return cell;
		}


	}
		break;
  default:
			break;
	}
	
	if(moneyValue < 0.0){
		moneyLabel.textColor = [UIColor redColor];
	}else{
		moneyLabel.textColor = [UIColor blueColor];
	}
	moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:moneyValue*(moneyValue<0?-1:1)];
	[cell.textLabel sizeToFit];
	return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
}

- (void)dealloc{
	NSLog(@"Overview dealloc");
}

@end
