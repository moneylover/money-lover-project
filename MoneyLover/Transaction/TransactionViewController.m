//
//  CenterViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 8/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "TransactionViewController.h"
#import "SQLiteManager.h"
#import "Transaction.h"
#import "TransactionCell.h"
#import "ShowTransactionViewController.h"
#import "AddTransactionViewController.h"
#import "FPPopoverController.h"
#import "OptionTableViewController.h"
#import "ChooseDurationTableViewController.h"
#import "ChooseStartAndEndDateViewController.h"
#import "NotificationCenter.h"


enum {
	NONE,
	DAY,
	WEEK,
	MONTH,
	QUARTER,
	YEAR,
	ALL,
	CUSTOM
 
}TimeDistance;


@interface TransactionViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate, OptionTableViewControllerProtocol, ChooseCustomDateProtocol,FPPopoverControllerDelegate, ChooseDurationTableVCProtocol, AddTransactionProtocol, ShowingTransactionDelegate, UIAlertViewDelegate >
@property (weak, nonatomic) IBOutlet UIButton *buttonNotification;
@property (weak, nonatomic) IBOutlet UILabel *label_badge_noti;

@property (weak, nonatomic) IBOutlet UIButton *addTransactionButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *walletButton;

@property (assign, nonatomic) CGFloat showTransactionViewWidth;
@property (assign, nonatomic) CGFloat showTransactionViewHeight;

@property (strong, nonatomic) NSMutableArray *contentViewsArray;
@property (strong, nonatomic) dispatch_queue_t serialQueue;

@property (strong, nonatomic) NSDate *startDate_leftView;
@property (strong, nonatomic) NSDate *startDate_centerView;
@property (strong, nonatomic) NSDate *startDate_rightView;
//@property (strong, nonatomic) NSDate *custom_EndDate;

@property (strong, nonatomic) NSCalendar *calendar;
@property (assign, nonatomic) NSInteger timeDistance;
@property (assign, nonatomic) NSTimeInterval timeIntervalForOneDay;

@property (strong, nonatomic) FPPopoverController *popoverRightOption;
@property (strong, nonatomic) FPPopoverController *popoverChooseDuration;

@end

@implementation TransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//	NSLog(@"Transaction view did load");
	
	[self.addTransactionButton addTarget:self action:@selector(addTransaction:) forControlEvents:UIControlEventTouchUpInside];
	
	self.addTransactionButton.layer.cornerRadius = self.addTransactionButton.frame.size.width/2.0;
	self.addTransactionButton.layer.shadowOffset = CGSizeMake( 0, 0);
	self.addTransactionButton.layer.shadowOpacity = 2.0;
	self.addTransactionButton.layer.shadowRadius = 3.0;
	[self.view bringSubviewToFront:self.addTransactionButton];
	
	
	if([[NSUserDefaults standardUserDefaults] objectForKey:@"AmountOfMoney"] == nil){
		
		[self.walletButton setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
			
		UIAlertView *adjustBalance = [[UIAlertView alloc] initWithTitle:@"Thông báo"
																														message:@"Bạn hãy nhập số dư ban đầu"
																													 delegate:self
																									cancelButtonTitle:@"Cancel"
																									otherButtonTitles:@"Ok", nil];
		[adjustBalance setAlertViewStyle:UIAlertViewStylePlainTextInput];
		UITextField *textField = [adjustBalance textFieldAtIndex:0];
		textField.keyboardType = UIKeyboardTypeNumberPad;
		adjustBalance.tag = 1;
		[adjustBalance show];
		
	}else{
		long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		[self.walletButton setTitle:[MoneyStringFormatter stringMoneyWithLongLong: amountOfMoney] forState:UIControlStateNormal];
	}
	
	
	self.showTransactionViewWidth = [[UIScreen mainScreen] bounds].size.width;
	self.showTransactionViewHeight = [[UIScreen mainScreen] bounds].size.height - 68.0;
	
	self.scrollView.frame = CGRectMake( 0.0f, 68.f, _showTransactionViewWidth, _showTransactionViewHeight);
	[self.scrollView setContentSize:CGSizeMake(_showTransactionViewWidth*3, _showTransactionViewHeight)];
	[_scrollView setContentOffset:CGPointMake(_scrollView.bounds.size.width, _scrollView.contentOffset.y) animated:NO];
	_scrollView.showsHorizontalScrollIndicator = NO;
	self.scrollView.pagingEnabled = YES;
	self.scrollView.bounces = NO;
	self.scrollView.delegate = self;

	self.contentViewsArray = [[NSMutableArray alloc] init];
	

	if([[NSUserDefaults standardUserDefaults] integerForKey:@"Duration"] == 0){
		_timeDistance = MONTH;
		[[NSUserDefaults standardUserDefaults] setInteger:_timeDistance forKey:@"Duration"];
	}else{
		_timeDistance = [[NSUserDefaults standardUserDefaults] integerForKey:@"Duration"];
	}
	
	self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

	_timeIntervalForOneDay = 60*60*24;
	
	[self caculateStartDateForLeftCenterAndRightView];
	
	_serialQueue = dispatch_queue_create("com.unique.LoadSQLIteDataqueue", DISPATCH_QUEUE_SERIAL);
	
	NSDate *startDate;
	
	for(int i = 0; i < 3; i ++){
		CGRect frame = CGRectMake(i*_showTransactionViewWidth, 0, _showTransactionViewWidth, _showTransactionViewHeight);
		ShowTransactionViewController	*showTransactionVC = [[ShowTransactionViewController alloc] initWithNibName:@"ShowTransactionViewController" bundle:nil ];
		showTransactionVC.delegate = self;
		showTransactionVC.view.frame = frame;
		//		showTransactionVC.title = [NSString stringWithFormat:@"%ld", (long)i];
		switch (i) {
			case 0:{
				startDate = _startDate_leftView;
			}
    break;
			case 1:{
				startDate = _startDate_centerView;
			}
    break;
			case 2:{
				startDate = _startDate_rightView;
			}
    break;
			default:
    break;
		}
		dispatch_async(_serialQueue, ^{
			[showTransactionVC loadDataWithStartDate:startDate andEndDate:[self  caculateEndDateForStartDate:startDate]];
		});
		
		[self.contentViewsArray addObject:showTransactionVC];
		[self.scrollView addSubview:((ShowTransactionViewController*)self.contentViewsArray[i]).view];
		[self addChildViewController:self.contentViewsArray[i]];
		[((ShowTransactionViewController*)self.contentViewsArray[i]) didMoveToParentViewController:self];
	}

	self.self.label_badge_noti.layer.cornerRadius = 8;
	self.self.label_badge_noti.layer.masksToBounds = YES;
	self.self.label_badge_noti.font = [UIFont fontWithName:@"ArialMT" size:12];
	[self updateBadgeForNotificationButton];
}

- (void)printfResult{
	/*
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd:MM:yyyy HH:mm:ss EEEE ZZZZ"];
	
	NSLog(@"******************************************************************");
	NSLog(@"left view:\n");
	NSLog(@"Start: %@", [dateFormatter stringFromDate:_startDate_leftView]);
	[self caculateEndDateForStartDate:_startDate_leftView];

	NSLog(@"******************************************************************");
	NSLog(@"center view:\n");
	NSLog(@"Start: %@", [dateFormatter stringFromDate:_startDate_centerView]);
	[self caculateEndDateForStartDate:_startDate_centerView];

	NSLog(@"******************************************************************");
	NSLog(@"right view:\n");
	NSLog(@"Start: %@", [dateFormatter stringFromDate:_startDate_rightView]);
	[self caculateEndDateForStartDate:_startDate_rightView];
	 */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
//	NSLog(@"view will appear");
	[self updateBadgeForNotificationButton];
}

- (IBAction)showSlideMenu:(id)sender {
//  NSLog(@"show slide menu");
  [self.delegate  showSlideMenu];
}

- (void)addTransaction:(UIButton*)sender{
//	NSLog(@"Add transaction");
	AddTransactionViewController *addTransactionVC = [[AddTransactionViewController alloc] initWithNibName:@"AddTransactionViewController" bundle:nil];
	addTransactionVC.delegate = self;
	[self.navigationController pushViewController:addTransactionVC animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	if(_scrollView.contentOffset.x == 0.0){
//		NSLog(@"swipe right");
		[_scrollView setContentOffset:CGPointMake(_scrollView.bounds.size.width, _scrollView.contentOffset.y) animated:NO];
		ShowTransactionViewController *rightViewController = (ShowTransactionViewController*)[_contentViewsArray lastObject];
		[_contentViewsArray removeLastObject];
		[_contentViewsArray insertObject:rightViewController atIndex:0];
		[self setShowingTransactionViewFrames];
		
		
		dispatch_async(_serialQueue, ^{
			NSDate *newStartDate = [self caculateStartDateForNewPageWithSwipeLeft:NO];
			[rightViewController loadDataWithStartDate:newStartDate andEndDate:[self caculateEndDateForStartDate:newStartDate]];
			
		});
		
	}else{
		if(_scrollView.contentOffset.x == _scrollView.bounds.size.width*([_contentViewsArray count] - 1)){
//			NSLog(@"Swipe left");
			
			[_scrollView setContentOffset:CGPointMake(_scrollView.bounds.size.width, _scrollView.contentOffset.y) animated:NO];
			ShowTransactionViewController *leftViewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:0];
			[_contentViewsArray removeObjectAtIndex:0];
			[_contentViewsArray addObject:leftViewController];
			[self setShowingTransactionViewFrames];
			
			dispatch_async(_serialQueue, ^{
				NSDate *newStartDate = [self caculateStartDateForNewPageWithSwipeLeft:YES];
				[leftViewController loadDataWithStartDate:newStartDate andEndDate:[self caculateEndDateForStartDate:newStartDate]];
				
			});
		}
	}
}

- (void)setShowingTransactionViewFrames{
//	NSLog(@"set showingtransaction Frame");
	for(int i = 0; i < [_contentViewsArray count]; i++){
		ShowTransactionViewController *temptViewController = (ShowTransactionViewController*)_contentViewsArray[i];
		temptViewController.view.frame = CGRectMake(_showTransactionViewWidth*i, 0.0, _showTransactionViewWidth, _showTransactionViewHeight);
	}
}


// Caculate data time methods
- (NSDate*)caculateEndDateForStartDate:(NSDate*)startDate{
	
	NSDate *result;
	NSDateComponents *dateComp = [[NSDateComponents alloc] init];
	NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute;
	
	switch (_timeDistance) {
  case DAY:{
		result = startDate;
	}
			break;
  case WEEK:{
		result = [startDate dateByAddingTimeInterval:6*_timeIntervalForOneDay];
	}
			break;
  case MONTH:{
		NSRange days = [_calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:startDate];
		result = [startDate dateByAddingTimeInterval:_timeIntervalForOneDay*(days.length - 1)];
	}
			break;
  case QUARTER:{
		dateComp = [_calendar components:unit fromDate:startDate];
		[dateComp setMonth:dateComp.month + 3];
		NSDate *firstDateonNextQuarter = [_calendar dateFromComponents:dateComp];
		result = [firstDateonNextQuarter dateByAddingTimeInterval:-_timeIntervalForOneDay];
	}
			break;
  case YEAR:{
		dateComp = [_calendar components:unit fromDate:startDate];
		[dateComp setYear:(dateComp.year + 1)];
		NSDate *firstDateOfNextYear = [_calendar dateFromComponents:dateComp];
		result = [firstDateOfNextYear dateByAddingTimeInterval:-_timeIntervalForOneDay];
	}
			break;
  case ALL:{
		// Do nothing
	}
			break;
  case CUSTOM:{
		
		self.scrollView.scrollEnabled = NO;
		NSDate *endDate = (NSDate*) [[NSUserDefaults standardUserDefaults] objectForKey:@"EndDate_CustomDuaration"];
		NSDate *startDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:@"StartDate_CustomDuaration"];
		self.startDate_centerView = startDate;
		self.startDate_leftView = nil;
		self.startDate_rightView = nil;
//		NSLog(@"Endate_CustomDuration %@", endDate);
		result = endDate;
	}
			break;
  default:
			break;
	}
	
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setDateFormat:@"dd:MM:yyyy HH:mm:ss EEEE ZZZZ"];
//	NSLog(@"End: %@", [dateFormatter stringFromDate:result]);
	return result;
}

- (NSDate*)caculateStartDateForNewPageWithSwipeLeft:(BOOL)isSwipeLeft{
//	NSLog(@"\n\n\n");
	NSInteger tempt;
	NSDate *result;
	
	NSDateComponents *dateComp = [[NSDateComponents alloc] init];
	NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute;
	dateComp = [_calendar components:unit fromDate:_startDate_centerView];
	
	if(isSwipeLeft){
		tempt = 1;
	}else{
		tempt = -1;
	}
	switch (_timeDistance) {
		case DAY:{
			result = [_startDate_centerView dateByAddingTimeInterval:_timeIntervalForOneDay*tempt*2];
		}
			break;
		case WEEK:{
			result = [_startDate_centerView dateByAddingTimeInterval:_timeIntervalForOneDay*tempt*14];
		}
			break;
		case MONTH:{
			[dateComp setMonth:dateComp.month + tempt*2];
			result = [_calendar dateFromComponents:dateComp];
			
		}
			break;
		case QUARTER:{
			[dateComp setMonth:dateComp.month + tempt*6];
			result = [_calendar dateFromComponents:dateComp];
		}
			break;
		case YEAR:{
			[dateComp setYear:dateComp.year + tempt*2];
			result = [_calendar dateFromComponents:dateComp];
		}
			break;
		case ALL:{
			// Do nothing
		}
			break;
		case CUSTOM:{
			// Do nothing
		}
			break;
			
  default:
			break;
	}
	if(isSwipeLeft){
		self.startDate_leftView = _startDate_centerView;
		self.startDate_centerView = _startDate_rightView;
		self.startDate_rightView = result;
	}else{
		self.startDate_rightView = _startDate_centerView;
		self.startDate_centerView = _startDate_leftView;
		self.startDate_leftView = result;
	}
	
//	[self printfResult];
	
//	NSLog(@"caculateStartDateForNewPageWithSwipeLeft %@", result);
	return result;
}

- (void)caculateStartDateForLeftCenterAndRightView{

	self.scrollView.scrollEnabled = YES;

	NSDate *currentTimeInLocalTimezone = [NSDate date];
	
	
	NSDateComponents *dateComp = [[NSDateComponents alloc] init];
	NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute;
	

	
//	NSLog(@"************************************************");
	switch (_timeDistance) {
		case DAY:{
			self.startDate_centerView = currentTimeInLocalTimezone;
			self.startDate_leftView = [self.startDate_centerView dateByAddingTimeInterval:-_timeIntervalForOneDay];
			self.startDate_rightView = [self.startDate_centerView dateByAddingTimeInterval:_timeIntervalForOneDay];
			
//			[self printfResult];
		}
			break;
		case WEEK:{

			NSInteger firstDayOfWeek =  [self.calendar ordinalityOfUnit:NSCalendarUnitWeekday	inUnit:NSCalendarUnitWeekOfMonth forDate:currentTimeInLocalTimezone];
//			NSLog(@"%d",firstDayOfWeek);
			self.startDate_centerView = [currentTimeInLocalTimezone dateByAddingTimeInterval:(_timeIntervalForOneDay*(2-firstDayOfWeek))];
			self.startDate_leftView = [self.startDate_centerView dateByAddingTimeInterval:-_timeIntervalForOneDay*7];
			self.startDate_rightView = [self.startDate_centerView dateByAddingTimeInterval:_timeIntervalForOneDay*7];
			
//			[self printfResult];
		}
			break;
		case MONTH:{
			NSDateComponents *dateComp = [[NSDateComponents alloc] init];
			NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute;
			
			dateComp = [_calendar components:unit fromDate:currentTimeInLocalTimezone];
			[dateComp setDay:1];
		
			NSInteger centerView_month = dateComp.month;
			self.startDate_centerView = [_calendar dateFromComponents:dateComp];
			[dateComp setMonth: centerView_month - 1];
			self.startDate_leftView = [_calendar dateFromComponents:dateComp];
			[dateComp setMonth:centerView_month + 1];
			self.startDate_rightView = [_calendar dateFromComponents:dateComp];
			
//			[self printfResult];
		}
			break;
		case QUARTER:{
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			[dateFormatter setDateFormat:@"Q"];
			NSInteger quarter = [[dateFormatter stringFromDate:currentTimeInLocalTimezone] intValue];
			
			dateComp = [_calendar components:unit fromDate:currentTimeInLocalTimezone];
			[dateComp setMonth:((quarter - 1)*3 + 1)];
			[dateComp setDay:1];
			self.startDate_centerView = [_calendar dateFromComponents:dateComp];
			NSInteger firstMonthInQuater_centerView = [dateComp month];
			[dateComp setMonth: firstMonthInQuater_centerView - 3];
			self.startDate_leftView = [_calendar dateFromComponents:dateComp];
			[dateComp setMonth:firstMonthInQuater_centerView + 3];
			self.startDate_rightView = [_calendar dateFromComponents:dateComp];
			
//			[self printfResult];
		}
			break;
		case YEAR:{
			
			dateComp = [_calendar components:unit fromDate:currentTimeInLocalTimezone];
			[dateComp setMonth:1];
			[dateComp setDay:1];
			
			self.startDate_centerView = [_calendar dateFromComponents:dateComp];
			
			NSInteger year_centerView = [dateComp year];
			[dateComp setYear:year_centerView - 1];
			self.startDate_leftView = [_calendar dateFromComponents:dateComp];
			[dateComp setYear:year_centerView + 1];
			self.startDate_rightView = [_calendar dateFromComponents:dateComp];
			
//			[self printfResult];
		}
			break;
		case ALL:{
			self.scrollView.scrollEnabled = NO;
			self.startDate_centerView = nil;
			self.startDate_leftView = nil;
			self.startDate_rightView = nil;
		}
			break;
		case CUSTOM:{
	
			self.scrollView.scrollEnabled = NO;
			NSDate *startDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:@"StartDate_CustomDuaration"];
			self.startDate_centerView = startDate;
			self.startDate_leftView = nil;
			self.startDate_rightView = nil;

		}
			break;
		default:
			break;
	}
}

- (IBAction)rightOptionAction:(id)sender {

	OptionTableViewController *tableViewController = [[OptionTableViewController alloc] init];
	tableViewController.title = nil;
	tableViewController.delegate = self;
	
	self.popoverRightOption = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverRightOption.contentSize = CGSizeMake(280,180);
	self.popoverRightOption.border = NO;
	self.popoverRightOption.tint = FPPopoverLightGrayTint;

	[self.popoverRightOption presentPopoverFromView:sender];
//	NSLog(@"right Option action");
}


// Right option protocol
- (void)selectRowAtIndex:(NSInteger)index{
	[self.popoverRightOption dismissPopoverAnimated:YES];
	
	switch (index) {
  case 0:{
		// Quay ve ngay hom nay
		if(_timeDistance != ALL && _timeDistance != CUSTOM){
			[self caculateStartDateForLeftCenterAndRightView];
			__block NSDate *startDate;
			dispatch_async(_serialQueue, ^{
				
				for(int i = 0; i  < 3; i++){
					if(i == 0){
						startDate = _startDate_leftView;
					}else{
						if( i == 1){
							startDate = _startDate_centerView;
						}else{
							startDate = _startDate_rightView;
						}
					}
					ShowTransactionViewController *viewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:i];
					[viewController loadDataWithStartDate:startDate andEndDate:[self caculateEndDateForStartDate:startDate]];
				}
			});
		}
	}
			break;
  case 1:{
		// Chon khoang thoi gian
		
		ChooseDurationTableViewController *tableViewController = [[ChooseDurationTableViewController alloc] init];
		tableViewController.delegate = self;
//		tableViewController.title = nil;
	
		self.popoverChooseDuration = [[FPPopoverController alloc] initWithViewController:tableViewController];
		self.popoverChooseDuration.contentSize = CGSizeMake(280,320);
		self.popoverChooseDuration.border = NO;
		self.popoverChooseDuration.tint = FPPopoverWhiteTint;
		self.popoverChooseDuration.arrowDirection = FPPopoverNoArrow;
		
		[self.popoverChooseDuration presentPopoverFromView:self.walletButton];
	}
			break;
  case 2:{
		// Xem theo giao dich, nhom giao dich
		for(int i = 0; i  < [_contentViewsArray count]; i++){
			ShowTransactionViewController *centerViewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:i];
			[centerViewController changeDisplayMode];
		}
	}
			break;
  case 3:{
		// Dieu chinh so du
		UIAlertView *adjustBalance = [[UIAlertView alloc] initWithTitle:@"Thông báo"
																														message:@"Điều chính số dư"
																													 delegate:self
																									cancelButtonTitle:@"Cancel"
																									otherButtonTitles:@"Ok", nil];
		[adjustBalance setAlertViewStyle:UIAlertViewStylePlainTextInput];
		UITextField *textField = [adjustBalance textFieldAtIndex:0];
		textField.keyboardType = UIKeyboardTypeNumberPad;
		adjustBalance.tag = 2;
		[adjustBalance show];
		
	}
			break;
  case 4:{
		// Tim giao dich
		
	}
			break;
  case 5:{
		// Xoá toàn bộ giao dịch
		[[SQLiteManager database] deleteAllTransaction];
		dispatch_async(_serialQueue, ^{
			
			for(int i = 0; i  < 3; i++){
				ShowTransactionViewController *viewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:i];
				[viewController updateTransactionsTable];
			}
		});
	}
			break;
  default:
			break;
	}
}

- (void)selectedDurationWithIndex:(NSInteger)index{

	[self.popoverChooseDuration dismissPopoverAnimated:YES];
	
	if(index == CUSTOM){
		ChooseStartAndEndDateViewController *chooseCustomDateVC = [[ChooseStartAndEndDateViewController alloc] initWithNibName:@"ChooseStartAndEndDateViewController" bundle:nil];
		chooseCustomDateVC.delegate = self;
		
		[self.navigationController pushViewController:chooseCustomDateVC animated:YES];
	}else{
		_timeDistance = index;
		[[NSUserDefaults standardUserDefaults] setInteger:_timeDistance forKey:@"Duration"];
		[self caculateStartDateForLeftCenterAndRightView];
		__block NSDate *startDate;
		dispatch_async(_serialQueue, ^{
			
			for(int i = 0; i  < 3; i++){
				if(i == 0){
					startDate = _startDate_leftView;
				}else{
					if( i == 1){
						startDate = _startDate_centerView;
					}else{
						startDate = _startDate_rightView;
					}
				}
//				NSLog(@"%@", startDate);
				ShowTransactionViewController *viewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:i];
				[viewController loadDataWithStartDate:startDate andEndDate:[self caculateEndDateForStartDate:startDate]];
			}
		});
	}
}


// Implement ChooseCustomDateProtocol
- (void)chooseStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate{
	_timeDistance = CUSTOM;
	[[NSUserDefaults standardUserDefaults] setInteger:_timeDistance forKey:@"Duration"];
	self.scrollView.scrollEnabled = NO;
	[[NSUserDefaults standardUserDefaults] setObject:startDate forKey:@"StartDate_CustomDuaration"];
	[[NSUserDefaults standardUserDefaults] setObject:endDate forKey:@"EndDate_CustomDuaration"];
	
	dispatch_async(_serialQueue, ^{
		ShowTransactionViewController *viewController = (ShowTransactionViewController*)[_contentViewsArray objectAtIndex:1];
		[viewController loadDataWithStartDate:startDate andEndDate:endDate];
	});
}

// Implemeting AddTransactionViewController
- (void)updatateData{
//NSLog(@"Transaction vc updatateData");
	
	dispatch_async(_serialQueue, ^{
		for(int i = 0; i < [_contentViewsArray count]; i++){
				ShowTransactionViewController *temptViewController = (ShowTransactionViewController*)_contentViewsArray[i];
				[temptViewController updateTransactionsTable];
		}
	});
	[self updateBadgeForNotificationButton];
}

// Implementing ShowingTransactionViewController
- (void)updateTotalMoneyInWallet{
	long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];

	[self.walletButton setTitle:[MoneyStringFormatter stringMoneyWithLongLong:amountOfMoney] forState:UIControlStateNormal];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(alertView.tag == 1){
		
		if(buttonIndex == 0){
			
			[self.walletButton setTitle:[NSString stringWithFormat:@"%d đ", 0] forState:UIControlStateNormal];
			[[NSUserDefaults standardUserDefaults] setDouble:0.0 forKey:@"AmountOfMoney"];
			
		}else{
			
			UITextField *textField = [alertView textFieldAtIndex:0];
			long long amountOfMoney = [textField.text longLongValue];
			[self.walletButton setTitle:[MoneyStringFormatter stringMoneyWithLongLong:amountOfMoney] forState:UIControlStateNormal];
			[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:amountOfMoney] doubleValue] forKey:@"AmountOfMoney"];
			
			Transaction *transaction = [[Transaction alloc] init];
			transaction.money = amountOfMoney;
			transaction.note = @"Số dư ban đầu";
			transaction.category_id = (transaction.money >= 0) ? 19 : 12;
			transaction.category = [[SQLiteManager database] getCategoryWithId:transaction.category_id];
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			dateFormatter.dateFormat = @"yyyy-MM-dd EEEE";
			transaction.date = [dateFormatter stringFromDate:[NSDate date]];
			
			[[SQLiteManager database] insertTransaction:transaction];
			
			[self updatateData];
		}
		
	}else{
		if(alertView.tag == 2){
			if(buttonIndex == 0){
				// Donothing
			}else{
				
				long long oldAmountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
				
				UITextField *textField = [alertView textFieldAtIndex:0];
				long long newAmountOfMoney = [textField.text longLongValue];
				
				[self.walletButton setTitle:[MoneyStringFormatter stringMoneyWithLongLong: newAmountOfMoney] forState:UIControlStateNormal];
				[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:newAmountOfMoney] doubleValue]
																									forKey:@"AmountOfMoney"];
				
				Transaction *transaction = [[Transaction alloc] init];
				transaction.money = newAmountOfMoney - oldAmountOfMoney;
				transaction.note = @"Điều chỉnh số dư";
				transaction.category_id = (transaction.money >= 0) ? 19 : 12;
				transaction.category = [[SQLiteManager database] getCategoryWithId:transaction.category_id];
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				dateFormatter.dateFormat = @"yyyy-MM-dd EEEE";
				transaction.date = [dateFormatter stringFromDate:[NSDate date]];
				
				[[SQLiteManager database] insertTransaction:transaction];
				
				[self updatateData];
				
			}
		}
	}

	[[NSUserDefaults standardUserDefaults] synchronize];
}


- (IBAction)showNotifications:(id)sender {
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	NotificationCenter *notificationCenter = [[NotificationCenter alloc] initWithNibName:@"NotificationCenter" bundle:nil];
	
	[self updateBadgeForNotificationButton];
	
	[self.navigationController pushViewController:notificationCenter	animated:YES];
}

- (void)updateBadgeForNotificationButton{
	NSInteger badgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
//	NSLog(@"badgeNumber %d", badgeNumber);
	if(badgeNumber != 0){
		self.label_badge_noti.alpha = 1;
		self.label_badge_noti.text = [NSString stringWithFormat:@"%ld",(long)badgeNumber];
	}else{
		self.label_badge_noti.alpha = 0;
	}
}

- (void)dealloc{
	
  NSLog(@"TransactionViewController dealloc");
}
@end
