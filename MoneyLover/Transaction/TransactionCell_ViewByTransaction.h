//
//  TransactionCell_ViewByTransaction.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 9/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TransactionCell_ViewByTransaction: UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightNotesLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftNotesLabel;

@end


