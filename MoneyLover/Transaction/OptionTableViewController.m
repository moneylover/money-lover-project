//
//  OptionTableViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/8/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "OptionTableViewController.h"

@interface OptionTableViewController ()
@property (assign, nonatomic) BOOL isShowingBytransaction;
@end

@implementation OptionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	[self.tableView registerClass:[UITableViewCell class]  forCellReuseIdentifier:@"CellOptionIden"];
	self.tableView.scrollEnabled = NO;
	

	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOptionIden" forIndexPath:indexPath];
	switch (indexPath.row) {
  case 0:{
		cell.textLabel.text = @"Quay về ngày hôm nay";
		cell.imageView.image = [UIImage imageNamed:@"ic_ab_calendar_empty.png"];
	}
			break;
  case 1:{
		cell.textLabel.text = @"Chọn khoảng thời gian";
		cell.imageView.image = [UIImage imageNamed:@"ic_ab_calendar.png"];
	}
			break;
  case 2:{
		
		if([[NSUserDefaults standardUserDefaults] integerForKey:@"DisplayModeByTransaction"] == 1){
			cell.textLabel.text = @"Xem theo nhóm";
		}else{
			cell.textLabel.text = @"Xem theo giao dịch";
		}
		
		cell.imageView.image = [UIImage imageNamed:@"ic_ab_view_transaction.png"];
	}
			break;
  case 3:{
		cell.textLabel.text = @"Điều chỉnh số dư";
		cell.imageView.image = [UIImage imageNamed:@"ic_ab_adjustment.png"];
	}
			break;
  case 4:{
		cell.textLabel.text = @"Tìm giao dịch";
		cell.imageView.image = [UIImage imageNamed:@"ic_ab_search.png"];
	}
			break;
  case 5:{
		cell.textLabel.text = @"Xoá toàn bộ giao dịch";
		cell.imageView.image = [UIImage imageNamed:@"ic_bab_delete.png"];
	}
			break;
  default:
			break;
	}
	cell.textLabel.font = [UIFont systemFontOfSize:17];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 35;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self.delegate selectRowAtIndex:indexPath.row];
}


@end
