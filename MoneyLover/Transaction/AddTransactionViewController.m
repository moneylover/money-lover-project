//
//  AddTransactionViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "AddTransactionViewController.h"
#import "ChooseCategoryViewController.h"
#import "CategoryTransaction.h"
#import "Transaction.h"
#import "ChoosePeopleInContactViewController.h"
#import "SQLiteManager.h"
#import "Calendar.h"
#import "CKCalendarView.h"
#import "SavingManager.h"


@interface AddTransactionViewController () <ChooseCategoryProtocol, CalendarDelegate, UIGestureRecognizerDelegate, ChoosePeopleInContactProtocol, UIAlertViewDelegate>

@property (assign, nonatomic) NSInteger selectedCategory_id;

@property (strong, nonatomic) NSDate *selectedDate;

@end

@implementation AddTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Do any additional setup after loading the view from its nib.
	[self.chooseCategoryButton addTarget:self action:@selector(chooseCategory:) forControlEvents:UIControlEventTouchUpInside];
	[self.withWhoButton addTarget:self action:@selector(withWhoAction:) forControlEvents:UIControlEventTouchUpInside];
	[self.chooseDateButton addTarget:self action:@selector(chooseDateAction:) forControlEvents:UIControlEventTouchUpInside];
	
	NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
	[dateFormater setDateFormat:@"yyyy-MM-dd EEEE"];
	[_chooseDateButton setTitle:[dateFormater stringFromDate:[NSDate date]] forState:UIControlStateNormal];
	
	UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGes:)];
	panGes.delegate  = self;
	[self.view addGestureRecognizer:panGes];
	
	UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGes:)];
	tapGes.delegate = self;
	[self.view addGestureRecognizer:tapGes];
	
	if(_editedTransaction == nil){
		self.titleLabel.text = @"Nhóm mới";
		self.deleteTransactionButton.enabled = NO;

	}else{
		self.titleLabel.text = @"Sửa giao dịch";
		[self setupViewWithTransaction:self.editedTransaction];
		self.deleteTransactionButton.enabled = YES;
	}
	self.selectedDate = [NSDate date];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	
}

- (void)handlePanGes:(UIPanGestureRecognizer*)panGes{
	// Do nothing
}

- (void)handleTapGes:(UITapGestureRecognizer*)tapGes{
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
}

- (void)chooseCategory:(UIButton*)button{
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
	
	ChooseCategoryViewController *chooseCategoryVC = [[ChooseCategoryViewController alloc] initWithNibName:@"ChooseCategoryViewController" bundle:nil];
	chooseCategoryVC.delegate = self;
	[self.navigationController pushViewController:chooseCategoryVC animated:YES];
	
}

- (void)withWhoAction:(UIButton*)button{
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
	ChoosePeopleInContactViewController *choosePeopleInContact = [[ChoosePeopleInContactViewController alloc] initWithNibName:@"ChoosePeopleInContactViewController" bundle:nil];
	choosePeopleInContact.delegate = self;
	[self.navigationController pushViewController:choosePeopleInContact animated:YES];
}

- (void)chooseDateAction:(UIButton*)button{
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
	
	[_chooseDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

	[[Calendar getInstanceWithParentViewController:self] showCalendar];
	
}

- (IBAction)cancelAction:(id)sender {
//	NSLog(@"cancel ");
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)completeAction:(id)sender {
	
	if([_chooseCategoryButton.titleLabel.text isEqualToString:@"Chọn nhóm"]){
			UIAlertView *alertChooseCategory = [[UIAlertView alloc]
																					initWithTitle:nil
																					message:@"Bạn cần chọn nhóm giao dịch"
																					delegate:self
																					cancelButtonTitle:nil
																					otherButtonTitles:@"Đóng", nil];
		[alertChooseCategory show];
		return;
	}
	
	CGFloat money = [[_moneyTextField text] floatValue];
	
	if(money == 0){
			UIAlertView *alertMoneyField= [[UIAlertView alloc]  initWithTitle:nil message:@"Bạn cần nhập số tiền" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Đóng", nil];
		[alertMoneyField show];
		return;
	}

	Transaction *transaction = [[Transaction alloc] init];
	transaction.category_id = _selectedCategory_id;
	transaction.category = [[SQLiteManager database] getCategoryWithId:transaction.category_id];
	transaction.money = [[_moneyTextField text] longLongValue]*(transaction.category.isEarning?1:-1);
	transaction.note = [_noteTextField text];
	if([[_withWhoButton titleLabel].text isEqualToString:@"Với"]){
			transaction.who = nil;
	}else{
			transaction.who = [_withWhoButton titleLabel].text;
	}
	transaction.date = [_chooseDateButton titleLabel].text;
	
	long long currentAmountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue] ;
	long long amountOfMoney = transaction.money -  self.editedTransaction.money;
	
	if(_editedTransaction != nil){
		transaction.Id = _editedTransaction.Id;
		[[SQLiteManager database] updateTransaction:transaction];
		
//		NSLog(@"New money: %lld\nOld money %lld", transaction.money, self.editedTransaction.money);
//		NSLog(@"Chenh lech: %lld\nSau khi update %lld", amountOfMoney, (currentAmountOfMoney + amountOfMoney));

		[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:(currentAmountOfMoney + amountOfMoney)] doubleValue] forKey:@"AmountOfMoney"];

	}else{
		[[SQLiteManager database] insertTransaction:transaction];
		[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:(currentAmountOfMoney + amountOfMoney)] doubleValue] forKey:@"AmountOfMoney"];
	}
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	
//	[[SavingManager getInstance] cacutaleSaving];
	
	[self.delegate updatateData];
	
	[self.navigationController popViewControllerAnimated:YES];

}

// Implement ChooseCategoryViewController protocol

- (void)selectedCategoryTransaction:(CategoryTransaction*)categoryTransaction;{
	[_moneyTextField resignFirstResponder];
	[_noteTextField resignFirstResponder];
	
	if(categoryTransaction != nil){
		_categoryImage.image = [UIImage imageNamed:categoryTransaction.iconName];
		[_chooseCategoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[_chooseCategoryButton setTitle:categoryTransaction.name forState:UIControlStateNormal];
		_selectedCategory_id = categoryTransaction.Id;
	}
}

// Implement ChoosePeopleInContactViewController protocol
- (void)choosePeoplesName:(NSMutableArray*)nameArray{
	NSMutableString *withPeopleString = [[NSMutableString alloc] init];
	if([nameArray count] != 0){
		for(int i = 0;  i < [nameArray count]; i++){
			if( i == [nameArray count] - 1){
				[withPeopleString appendString:[NSString stringWithFormat:@"%@", (NSString*)nameArray[i] ]];
			}else{
				[withPeopleString appendString:[NSString stringWithFormat:@"%@ - ", (NSString*)nameArray[i] ]];
			}
		}
		[_withWhoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

		[_withWhoButton setTitle:withPeopleString forState:UIControlStateNormal];
	}
}

- (IBAction)deleteTransaction:(id)sender {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
									message:@"Bạn muốn xoá giao dịch này không?"
									delegate:self
									cancelButtonTitle:@"Huỷ lệnh"
									otherButtonTitles:@"Đồng ý", nil];
	[alertView show];
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 1){
		long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		
		amountOfMoney -= self.editedTransaction.money;
		
		[[NSUserDefaults standardUserDefaults] setDouble:[[NSNumber numberWithLongLong:amountOfMoney] doubleValue] forKey:@"AmountOfMoney"];
		
		[[SQLiteManager database] deleteTransaction:_editedTransaction];
		[self.delegate updatateData];
		[self.navigationController popToRootViewControllerAnimated:YES];
	}
}

- (void)setupViewWithTransaction:(Transaction*)transaction{
	
	[self.chooseCategoryButton setTitle:transaction.category.name forState:UIControlStateNormal];
	self.categoryImage.image = [UIImage imageNamed:transaction.category.iconName];
	self.moneyTextField.text = [NSString stringWithFormat:@"%.0f",fabs(transaction.money) ];
	self.noteTextField.text = [NSString stringWithFormat:@"%@", transaction.note];
	if([transaction.who isEqualToString:@"(null)"]){
			[self.withWhoButton setTitle:@"Với" forState:UIControlStateNormal];
	}else{
			[self.withWhoButton setTitle:transaction.who forState:UIControlStateNormal];
	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
	NSDate *date = [dateFormatter dateFromString:transaction.date];
	
	[self.chooseDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
	_selectedCategory_id = transaction.category_id;
	
	[_chooseDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[_withWhoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[_chooseCategoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}


- (void)selectedDate:(NSDate*)date{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"];
	self.selectedDate = date;
	[_chooseDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
	_chooseDateButton.titleLabel.textColor = [UIColor blackColor];
}

- (void)dealloc{
	NSLog(@"AddTransaction vc dealloc");
}
@end
