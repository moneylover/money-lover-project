//
//  OverviewViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/7/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverviewViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSString *timeString;

@property (strong, nonatomic) NSMutableArray *expenseArray;
@property (strong, nonatomic) NSMutableArray *incomeArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil transactionData:(NSMutableArray*)transactionsArray timeLabelString:(NSString*)timeLabelString andStartDate:(NSDate*)startDate;

@end
