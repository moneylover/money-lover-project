//
//  TrendingViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "TrendingViewController.h"
#import "PNChart.h"
#import "SQLiteManager.h"
#import "Transaction.h"

@interface TrendingViewController () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>



@property (nonatomic, strong) UISegmentedControl *segmentedControlCategory;
@property (nonatomic, strong) UISegmentedControl *segmentedControlTime;
@property (strong, nonatomic) UILabel *timeLabel;

@property (strong, nonatomic) UIView *selectTimeView;
@property (strong, nonatomic) UIPickerView *pickerView;

@property (strong, nonatomic) NSMutableArray *monthsArray;
@property (strong, nonatomic) NSMutableArray *yearsArray;
@property (strong, nonatomic) NSMutableArray *dateArray;
@property (assign, nonatomic) BOOL isShowingByMonth;
@property (assign, nonatomic) BOOL isShowingByIncoming;

@property (assign, nonatomic) NSInteger selectedMonth;
@property (assign, nonatomic) NSInteger selectedYear;

@property (strong, nonatomic) PNBarChart *barChart;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) NSMutableArray *yValueArray;

@end

@implementation TrendingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	self.barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(10, 15.0, SCREEN_WIDTH, 200.0)];
	self.barChart.isShowNumbers = NO;
	self.barChart.yLabelSuffix = @" ";
	self.barChart.yLabelSum = 8;
	
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	self.tableView.allowsSelection = NO;

	self.segmentedControlCategory = [[UISegmentedControl alloc]
														 initWithItems:@[@"Thu nhập", @"Chi tiêu"]];
	self.segmentedControlCategory.tintColor = [UIColor greenColor];
	self.segmentedControlCategory.translatesAutoresizingMaskIntoConstraints = NO;
	[self.segmentedControlCategory addTarget:self action:@selector(segmentCategoryChanged:) forControlEvents:UIControlEventValueChanged];
	self.segmentedControlCategory.selectedSegmentIndex = 0;
	
	self.segmentedControlTime = [[UISegmentedControl alloc]
																	 initWithItems:@[@"Tháng", @"Năm"]];
	self.segmentedControlTime.tintColor = [UIColor greenColor];
	self.segmentedControlTime.translatesAutoresizingMaskIntoConstraints = NO;
	[self.segmentedControlTime addTarget:self action:@selector(segmentTimeChanged:) forControlEvents:UIControlEventValueChanged];
	self.segmentedControlTime.selectedSegmentIndex = 1;
	
	self.timeLabel = [[UILabel alloc] init];
	self.timeLabel.textColor = [UIColor blueColor];
	self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.timeLabel sizeToFit];
	UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTime:)];
	[self.timeLabel addGestureRecognizer:tapGes];
	self.timeLabel.userInteractionEnabled = YES;

	self.monthsArray = [NSMutableArray arrayWithArray:@[@"Tháng 1", @"Tháng 2", @"Tháng 3", @"Tháng 4", @"Tháng 5", @"Tháng 6", @"Tháng 7", @"Tháng 8", @"Tháng 9", @"Tháng 10", @"Tháng 11", @"Tháng 12"]];
	self.yearsArray = [[ NSMutableArray alloc ]init];
	for(int i = 1970; i < 2071; i++){
		[self.yearsArray addObject:[NSString stringWithFormat:@"Năm %ld", (long)i]];
	}
	self.dateArray = [[ NSMutableArray alloc ]init];
	for(int i = 1; i < 32; i++){
		[self.dateArray addObject:[NSString stringWithFormat:@"%ld", (long)i]];
	}
	self.isShowingByMonth = NO;
	self.isShowingByIncoming = YES;
}



- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	
	self.selectTimeView = [[UIView alloc] initWithFrame:self.view.frame];
	self.selectTimeView.backgroundColor = [UIColor clearColor];
	UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
	backgroundView.backgroundColor = [UIColor grayColor];
	backgroundView.alpha = 0.8;
	self.selectTimeView.hidden = YES;
	[self.selectTimeView addSubview:backgroundView];
	UITapGestureRecognizer*tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePickerView:)];
	[self.selectTimeView addGestureRecognizer:tapGes];
	
	self.pickerView = [[UIPickerView alloc] init];
	self.pickerView.delegate = self;
	self.pickerView.dataSource = self;
	self.pickerView.center = self.view.center;
	self.pickerView.backgroundColor = [UIColor whiteColor];
	self.pickerView.delegate = self;
	[self.selectTimeView addSubview:self.pickerView];
	
	[self.view addSubview:self.selectTimeView];
	
	self.selectedMonth = 1;
	self.selectedYear = 2015;
	[self setTimeLabel];
}
	
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showSlideMenuAction:(id)sender {
	[self.delegate showSlideMenu];
}

- (void)segmentCategoryChanged:(UISegmentedControl *)paramSender{
//	NSLog(@"Change category");
	self.isShowingByIncoming = !self.isShowingByIncoming;
	[self.tableView reloadData];

}

- (void)segmentTimeChanged:(UISegmentedControl *)paramSender{
//	NSLog(@"Change time");
	self.isShowingByMonth = !_isShowingByMonth;
	[self.pickerView reloadAllComponents];
	[self setTimeLabel];
	
	[self.tableView reloadData];
	
}

#pragma - UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(section == 0){
		return 1;
	}else{
		
		if(_isShowingByMonth){
			return [self.dateArray count] + 1;
		}else{
			return [self.monthsArray count] + 1;
		}
	}
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	if(indexPath.section == 0){
		return 180;
	}else{
		if(indexPath.row == 0){
			return 200;
		}else{
			return 50;
		}
		
	}
	return 50;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden"];
	UITableViewCell *cell = [[UITableViewCell alloc] init];
	switch (indexPath.section) {
		case 0:{
			[cell addSubview:self.segmentedControlCategory];
			[cell addSubview:self.segmentedControlTime];
			[cell addSubview:self.timeLabel];
			[cell addConstraints:[self createConstraintForCell:cell]];
			
			UILabel *viewByLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 90, 100, 25)];
			viewByLabel.text = @"Thời gian";
			[viewByLabel setFont:[UIFont systemFontOfSize:13]];
			viewByLabel.textColor = [UIColor grayColor];
			[cell addSubview:viewByLabel];
			
		}
			break;
		case 1:{
			if(indexPath.row == 0){
				[cell addSubview:self.barChart];
				[self drawChartWithMonth:_selectedMonth andYear:_selectedYear];
			}else{
				if(_isShowingByMonth){
					cell.textLabel.text = [NSString stringWithFormat:@"Ngày %@", self.dateArray[indexPath.row - 1]];
				}else{
					cell.textLabel.text = (NSString *)self.monthsArray[indexPath.row - 1];
				}
				
				UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 150, 20)];
				moneyLabel.textAlignment = NSTextAlignmentRight;
				cell.accessoryView = moneyLabel;
				NSNumber *money = (NSNumber *)self.yValueArray[indexPath.row -1];
				moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:llabs([money longLongValue])];
				if(_isShowingByIncoming){
					moneyLabel.textColor = [UIColor blueColor];
				}else{
					moneyLabel.textColor = [UIColor redColor];
				}
			}

		}
			break;

  default:
			break;
	}
	
	return cell;
}

- (NSArray*)createConstraintForCell:(UITableViewCell *)cell {
	
	
	NSLayoutConstraint *centerXForCategorySegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlCategory
																																							 attribute:NSLayoutAttributeCenterX
																																							 relatedBy:NSLayoutRelationEqual
																																									toItem:cell
																																							 attribute:NSLayoutAttributeCenterX
																																							multiplier:1.0f
																																								constant:0.0f];
	
	NSLayoutConstraint *centerXForTimeSegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlTime
																																					 attribute:NSLayoutAttributeCenterX
																																					 relatedBy:NSLayoutRelationEqual
																																							toItem:cell
																																					 attribute:NSLayoutAttributeCenterX
																																					multiplier:1.0f
																																						constant:0.0f];
	
	NSLayoutConstraint *leftMarginForCategorySegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlCategory
																																								 attribute:NSLayoutAttributeLeft
																																								 relatedBy:NSLayoutRelationEqual
																																										toItem:cell
																																								 attribute:NSLayoutAttributeLeft
																																								multiplier:1.0f
																																									constant:20.0f];
	
	NSLayoutConstraint *leftMarginForTimeSegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlTime
																																							attribute:NSLayoutAttributeLeft
																																							relatedBy:NSLayoutRelationEqual
																																								 toItem:cell
																																							attribute:NSLayoutAttributeLeft
																																						 multiplier:1.0f
																																							 constant:20.0f];
	
	NSLayoutConstraint *constraintYForCategorySegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlCategory
																																									 attribute:NSLayoutAttributeTop
																																									 relatedBy:NSLayoutRelationEqual
																																											toItem:cell
																																									 attribute:NSLayoutAttributeTop
																																									multiplier:1.0f
																																										constant:10.0f];
	
	NSLayoutConstraint *constraintYForTimeSegment = [NSLayoutConstraint constraintWithItem:self.segmentedControlTime
																																							 attribute:NSLayoutAttributeTop
																																							 relatedBy:NSLayoutRelationEqual
																																									toItem:cell
																																							 attribute:NSLayoutAttributeTop
																																							multiplier:1.0f
																																								constant:50.0f];
	
	NSLayoutConstraint *constraintXForTimeLabel = [NSLayoutConstraint constraintWithItem:self.timeLabel
																																						 attribute:NSLayoutAttributeCenterX
																																						 relatedBy:NSLayoutRelationEqual
																																								toItem:cell
																																						 attribute:NSLayoutAttributeCenterX
																																						multiplier:1.0f
																																							constant:10.0f];
	
	NSLayoutConstraint *constraintYForTimeLabel = [NSLayoutConstraint constraintWithItem:self.timeLabel
																																						 attribute:NSLayoutAttributeTop
																																						 relatedBy:NSLayoutRelationEqual
																																								toItem:cell
																																						 attribute:NSLayoutAttributeTop
																																						multiplier:1.0f
																																							constant:130.0f];
	
	return @[centerXForCategorySegment,centerXForTimeSegment, leftMarginForCategorySegment, leftMarginForTimeSegment, constraintYForTimeSegment, constraintYForCategorySegment, constraintXForTimeLabel, constraintYForTimeLabel];

}


#pragma - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	if(_isShowingByMonth){
		return  2;
	}
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	if(_isShowingByMonth){
		switch (component) {
			case 0:{
				return [self.monthsArray count];
			}
				break;
			case 1:{
				return [self.yearsArray count];
			}
				break;
			default:
				break;
		}

	}else{
		switch (component) {
			case 0:{
				return [self.yearsArray count];
			}
				break;
			default:
				break;
		}
	}


	return 1;
}

#pragma - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
						forComponent:(NSInteger)component{
	if(_isShowingByMonth){
		switch (component) {
			case 0:{
				return ((NSString*)self.monthsArray[row]);
			}
				break;
			case 1:{
				return ((NSString*)self.yearsArray[row]);
			}
				break;
			default:
				break;
		}
	}else{
		switch (component) {
			case 0:{
				return ((NSString*)self.yearsArray[row]);
			}
				break;
			default:
				break;
		}
	}
	return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
//	NSLog(@"row: %d component: %d", row, component);
	if(_isShowingByMonth){
		if(component == 0){
			self.selectedMonth = row + 1;
		}else{
			self.selectedYear = 1970 + row;
		}
	}else{
		self.selectedYear = 1970 + row;
	}
//	NSLog(@"Month: %d Year: %d", self.selectedMonth, self.selectedYear);
}


#pragma - Setting selector for tap gesture recognizer
- (void)hidePickerView:(UITapGestureRecognizer *)pramaSender{
		[UIView animateWithDuration:0.5 animations:^{
			self.selectTimeView.alpha = 0;
		} completion:^(BOOL finished) {
			self.selectTimeView.hidden = YES;
		}];
	[self setTimeLabel];

	[self drawChartWithMonth:self.selectedMonth andYear:self.selectedYear];
}

- (void)selectTime:(UITapGestureRecognizer *)paramSender{
//	[self.pickerView reloadAllComponents];
	if(_isShowingByMonth){
		[self.pickerView selectRow:45 inComponent:1 animated:YES];
		[self.pickerView selectRow:0 inComponent:0 animated:YES];
	}else{
		[self.pickerView selectRow:45 inComponent:0 animated:YES];
	}
	self.selectedMonth = 1;
	self.selectedYear = 2015;
	
	self.selectTimeView.alpha = 0;
	self.selectTimeView.hidden = NO;
	
	[UIView animateWithDuration:0.5 animations:^{
		self.selectTimeView.alpha = 1;
	} completion:^(BOOL finished) {
		
	}];
}

- (void)setTimeLabel{
	if(_isShowingByMonth){
		self.timeLabel.text = [NSString stringWithFormat:@"Tháng %ld năm %ld", (long)self.selectedMonth, (long)self.selectedYear];
	}else{
		self.timeLabel.text = [NSString stringWithFormat:@"Năm %ld", (long)self.selectedYear];
	}
}

- (void)drawChartWithMonth:(NSInteger)month andYear:(NSInteger)year{

	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
	NSDate *startDate;
	NSDate *endDate;
	[dateComponents setCalendar:calendar];
	if(_isShowingByMonth){
		[dateComponents setYear:year];
		[dateComponents setMonth:month];
		[dateComponents setDay:1];
		
		startDate = [dateComponents date];
		[dateComponents setMonth:dateComponents.month + 1];
		endDate = [[dateComponents date] dateByAddingTimeInterval:-60*60*24];

	}else{
		[dateComponents setYear:year];
		[dateComponents setMonth:1];
		[dateComponents setDay:1];
		startDate = [dateComponents date];
		[dateComponents setYear:dateComponents.year + 1];
		endDate = [[dateComponents date] dateByAddingTimeInterval:-60*60*24];
	}
	self.dataArray = [[SQLiteManager database] getMoneyTransactionForTrendingFeatureWithStartDate:startDate endDate:endDate isShowingByMonth:_isShowingByMonth andIsIncoming:_isShowingByIncoming];

	if(_isShowingByMonth){
		[self.barChart setXLabels:self.dateArray];
		self.yValueArray = [[NSMutableArray alloc] init];
		NSMutableArray *temptArray = [[NSMutableArray alloc] init];
		for (int i = 0; i < [self.dateArray count]; i++) {
			NSString *dateFromArray = (NSString *)self.dateArray[i];
			
			NSNumber *aNumber = [NSNumber numberWithInt:0];
			NSNumber *aNumberForYValue = [NSNumber numberWithInt:0];
			for(int j = 0; j < [self.dataArray count]; j++){
				Transaction *transaction = (Transaction *)self.dataArray[j];
				NSString *date = [transaction.date substringWithRange:NSMakeRange(8, 2)];
	
				if([dateFromArray intValue] == [date intValue]){
					CGFloat money = ceilf(transaction.money/1000);
					if(transaction.money < 0){
						money = money*-1;
					}
					aNumber = [NSNumber numberWithInt:(int)money];
					aNumberForYValue = [NSNumber numberWithInt:(int)transaction.money];
					break;
				}
			}
			[temptArray addObject:aNumber];
			[self.yValueArray addObject:aNumberForYValue];
		}
		[self.barChart setYValues:temptArray];
	}else{
		[self.barChart setXLabels:@[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12]];
		self.yValueArray = [[NSMutableArray alloc] init];
		NSMutableArray *temptArray = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < [self.monthsArray count]; i++) {
			NSString *monthFromArray = [(NSString *)self.monthsArray[i] substringFromIndex:6];
			NSNumber *aNumber = [NSNumber numberWithInt:0];
			NSNumber *aNumberForYValue = [NSNumber numberWithInt:0];
			for(int j = 0; j < [self.dataArray count]; j++){
				Transaction *transaction = (Transaction *)self.dataArray[j];
				NSString *date = [transaction.date substringWithRange:NSMakeRange(5, 2)];
				
				if([monthFromArray intValue] == [date intValue]){
					CGFloat money = ceilf(transaction.money/1000);
					if(transaction.money < 0){
						money = money*-1;
					}
					aNumberForYValue = [NSNumber numberWithInt:(int)transaction.money];
					aNumber = [NSNumber numberWithInt:(int)money];
					break;
				}
			}
			[temptArray addObject:aNumber];
			[self.yValueArray addObject:aNumberForYValue];
		}
		[self.barChart setYValues:temptArray];
	}
	if(_isShowingByIncoming){
		self.barChart.strokeColor = [UIColor blueColor];
//		self.barChart.barColorGradientStart = [UIColor blueColor];
	}else{
		self.barChart.strokeColor = [UIColor redColor];
//		self.barChart.barColorGradientStart = [UIColor redColor];
	}
	[self.barChart strokeChart];
	
}


@end
