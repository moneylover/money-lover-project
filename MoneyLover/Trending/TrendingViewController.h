//
//  TrendingViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/17/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TrendingViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
