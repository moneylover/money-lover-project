//
//  OverViewSavingCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/23/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverViewSavingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *currentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *thisMonthAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *needingAmountLbl;

@end
