//
//  AddSavingViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "AddSavingViewController.h"
#import "ChooseCategoryIconViewController.h"
#import "SQLiteManager.h"
#import "SavingManager.h"


@interface AddSavingViewController () <UITextFieldDelegate, UIScrollViewDelegate, ChooseCategoryIconProtocol, UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;
@property (weak, nonatomic) IBOutlet UIButton *chooseMonthButton;

@property (copy, nonatomic) NSString *iconName;

// PickerView
@property (strong, nonatomic) UIView *selectTimeView;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *yearsArray;
@property (strong, nonatomic) NSMutableArray *monthsArray;

@property (assign, nonatomic) NSInteger forbidMonthRowInPickerView;
@property (assign, nonatomic) NSInteger forbidYearRowInPickerView;
@property (assign, nonatomic) NSInteger selectedMonthRowInPickerView;
@property (assign, nonatomic) NSInteger selectedYearRowInPickerView;

@property (strong, nonatomic) NSDate *selectedDate;
@end

@implementation AddSavingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	self.nameTextField.delegate = self;
	self.moneyTextField.delegate = self;
	self.scrollView.delegate = self;

	UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesHandle:)];
	[self.iconView addGestureRecognizer:tapGes];
	

	self.monthsArray = [NSMutableArray arrayWithArray:@[@"Tháng 1", @"Tháng 2", @"Tháng 3", @"Tháng 4", @"Tháng 5", @"Tháng 6", @"Tháng 7", @"Tháng 8", @"Tháng 9", @"Tháng 10", @"Tháng 11", @"Tháng 12"]];
	NSDate *toDay = [NSDate date];
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

//	NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
	NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:toDay];
	[dateComponents setMonth:dateComponents.month + 1];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"YYYY";
	NSInteger startYear = [[dateFormatter stringFromDate:toDay] integerValue];
	
	self.forbidMonthRowInPickerView = dateComponents.month - 2;
	self.forbidYearRowInPickerView = dateComponents.year/startYear - 1;
//	NSLog(@"%ld %d", (long)self.forbidMonthRowInPickerView, self.forbidYearRowInPickerView);
	self.yearsArray = [[NSMutableArray alloc] init];
	
	for(NSInteger i = startYear; i < (startYear + 101); i++){
		[self.yearsArray addObject:[NSString stringWithFormat:@"Năm %ld", (long)i]];
	}
	
	if(self.editedSaving != nil){
		self.nameTextField.text = self.editedSaving.name;
		self.iconName = self.editedSaving.iconName;
		self.iconView.image = [UIImage imageNamed:self.iconName];
		self.moneyTextField.text = [NSString stringWithFormat:@"%lld đ", self.editedSaving.money];
		self.selectedDate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.editedSaving.endDate];
		[self.chooseMonthButton setTitle:self.editedSaving.endDateString forState:UIControlStateNormal];
	}
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	
	self.scrollView.frame = CGRectMake(0, 68, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 68);
	[self.scrollView setContentSize:self.scrollView.frame.size];
	self.contentView.frame = CGRectMake(8, 8, [[UIScreen mainScreen] bounds].size.width - 16, [[UIScreen mainScreen] bounds].size.height - 68 - 8);
	
	self.selectTimeView = [[UIView alloc] initWithFrame:self.view.frame];
	self.selectTimeView.backgroundColor = [UIColor clearColor];
	UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
	backgroundView.backgroundColor = [UIColor grayColor];
	backgroundView.alpha = 0.8;
	self.selectTimeView.hidden = YES;
	[self.selectTimeView addSubview:backgroundView];
	UITapGestureRecognizer*tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePickerView:)];
	[self.selectTimeView addGestureRecognizer:tapGes];
	
	self.pickerView = [[UIPickerView alloc] init];
	self.pickerView.delegate = self;
	self.pickerView.dataSource = self;
	self.pickerView.center = self.view.center;
	self.pickerView.backgroundColor = [UIColor whiteColor];
	self.pickerView.delegate = self;
	[self.selectTimeView addSubview:self.pickerView];
	
	[self.view addSubview:self.selectTimeView];
	

}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - Setting selector for tap gesture recognizer

- (void)hidePickerView:(UITapGestureRecognizer *)pramaSender{
	
	[UIView animateWithDuration:0.5 animations:^{
		self.selectTimeView.alpha = 0;
	} completion:^(BOOL finished) {
		self.selectTimeView.hidden = YES;
	}];
	
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//	NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
	NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
	[dateComponents setMonth:self.selectedMonthRowInPickerView + 1];
	[dateComponents setYear: 2015 + self.selectedYearRowInPickerView];
	[dateComponents setDay:1];
	[dateComponents setHour:8];
	[dateComponents setMinute:0];
	[dateComponents setSecond:0];
	
	NSDate *selectedDate = [calendar dateFromComponents:dateComponents];
	self.selectedDate = selectedDate;

	[self.chooseMonthButton setTitle:[NSString stringWithFormat:@"%ld - %ld", (long)dateComponents.year, (long)dateComponents.month] forState:UIControlStateNormal];

}

- (void)tapGesHandle:(UITapGestureRecognizer *)tapGes{
	[self.moneyTextField resignFirstResponder];
	[self.nameTextField resignFirstResponder];
	
	ChooseCategoryIconViewController *chooseIcon = [[ChooseCategoryIconViewController alloc] initWithNibName:@"ChooseCategoryIconViewController" bundle:nil];
	chooseIcon.delegate = self;
	[self.navigationController pushViewController:chooseIcon animated:YES];
}


#pragma mark - ChooseCategoryIconProtocol

- (void)getIconName:(NSString *)iconName{
	self.iconName = iconName;
	self.iconView.image = [UIImage imageNamed:iconName];
}


#pragma mark - UIButton action

- (IBAction)chooseMonth:(id)sender {
	
	[self.moneyTextField resignFirstResponder];
	[self.nameTextField resignFirstResponder];
	
	self.selectedMonthRowInPickerView = (self.forbidMonthRowInPickerView + 1)%12;
	self.selectedYearRowInPickerView = 0;
	
	[self.pickerView selectRow:self.selectedMonthRowInPickerView inComponent:0 animated:YES];
	[self.pickerView selectRow:0 inComponent:1 animated:YES];
	

	
	self.selectTimeView.alpha = 0;
	self.selectTimeView.hidden = NO;
	
	[UIView animateWithDuration:0.5 animations:^{
		self.selectTimeView.alpha = 1;
	} completion:^(BOOL finished) {
		
	}];
}

- (IBAction)cancelAction:(id)sender {
	
	[self.moneyTextField resignFirstResponder];
	[self.nameTextField resignFirstResponder];
	
	[self.delegate cancelAddingSaving];
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

- (IBAction)completeAction:(id)sender {
	[self.moneyTextField resignFirstResponder];
	[self.nameTextField resignFirstResponder];
	
	if(self.iconName == nil){
		UIAlertView *alertView = [[UIAlertView alloc]
														initWithTitle:nil
														message:@"Bạn cần chọn ảnh đại diện"
														delegate:self
														cancelButtonTitle:nil
														otherButtonTitles:@"Đóng", nil];
		[alertView show];
		return;
	}
	
	
	NSString *removeWhiteSpaceString = [self.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if([removeWhiteSpaceString isEqualToString:@""]){
		UIAlertView *invalidName = [[UIAlertView alloc] initWithTitle:nil
																															message:@"Bạn cần nhập tên cho khoản tiết kiệm này"
																														 delegate:self
																										cancelButtonTitle:@"Đóng"
																										otherButtonTitles:nil];
		[invalidName show];
		return;
	}
	
	
	NSLog(@"%@", self.chooseMonthButton.titleLabel.text);
	if([self.chooseMonthButton.titleLabel.text isEqualToString:@"Chọn tháng"]){
		UIAlertView *invalidMonth = [[UIAlertView alloc] initWithTitle:nil
																													message:@"Bạn cần chọn thời gian cho khoản tiết kiệm này"
																												 delegate:self
																								cancelButtonTitle:@"Đóng"
																								otherButtonTitles:nil];
		[invalidMonth show];
		return;
	}
	
	
	CGFloat money = [[self.moneyTextField text] floatValue];
	if(money == 0){
		UIAlertView *alertMoneyField= [[UIAlertView alloc]  initWithTitle:nil
																  message:@"Bạn cần nhập số tiền"
																 delegate:self
														cancelButtonTitle:nil
														otherButtonTitles:@"Đóng", nil];
		[alertMoneyField show];
		return;
	}
	
	Saving *saving = [[Saving alloc] init];
	saving.name = removeWhiteSpaceString;
	saving.iconName = self.iconName;
	saving.money = [self.moneyTextField.text longLongValue];
	saving.endDate = [self.selectedDate timeIntervalSinceReferenceDate];
//	NSLog(@"End ss date: %f ", saving.endDate);

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"yyyy - MM";
	saving.endDateString = [dateFormatter stringFromDate:self.selectedDate];
	saving.expried = NO;
//	NSLog(@"End ss date: %@ %@", saving.endDateString, self.selectedDate);
	
	if(self.editedSaving == nil){
		
		if([[SQLiteManager database] checkForExistingSavingName:removeWhiteSpaceString andIsExpried:NO] == YES){
			
			UIAlertView *existingNameAlertView = [[UIAlertView alloc] initWithTitle:nil
																																			message:@"Tên này đã tồn tại"
																																		 delegate:self
																														cancelButtonTitle:@"Đóng"
																														otherButtonTitles:nil];
			[existingNameAlertView show];
			
			return;
			
		}else{
			
			UILocalNotification *localNotification = [[UILocalNotification alloc] init];
			localNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
			localNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
			localNotification.alertBody = NSLocalizedString(@"Bạn đã hoàn thành một khoản tiết kiệm.", nil);
			/* Action settings */
			localNotification.hasAction = YES;
			localNotification.alertAction = NSLocalizedString(@"View", nil);
			/* Badge settings */
			localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
			localNotification.userInfo = @{@"SavingName" : [NSString stringWithFormat:@"%@", saving.name]};
			localNotification.soundName = UILocalNotificationDefaultSoundName;
			/* Schedule the notification */
			[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//			NSLog(@"Local Notification:\n%@", localNotification);
			
			[[SQLiteManager database] insertSaving:saving];
			
		}
	}else{
		
		saving.Id = self.editedSaving.Id;
		[[SQLiteManager database] updateSaving:saving];
		
		NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
//		NSLog(@" updateSaving Notification array: %ld",(long) [localNotifications count]);
		
		for (UILocalNotification *localNotification in localNotifications){
//			NSLog(@"Local Notification:\n%@", localNotification);
			NSDictionary *userInfo = localNotification.userInfo;
			NSString *localNotificationWithSavingName = [userInfo valueForKey:@"SavingName"];
			if([localNotificationWithSavingName isEqualToString:self.editedSaving.name]){
				localNotification.fireDate = [NSDate dateWithTimeIntervalSinceReferenceDate: saving.endDate];
				localNotification.userInfo  = @{@"SavingName": [NSString stringWithFormat:@"%@", saving.name]};
				break;
			}
			
//			NSLog(@" updateSaving Notification array: %ld",(long) [localNotifications count]);
		}
	}
	
	[[SavingManager getInstance] caculateMonthlySaving];
	
	[self.delegate completeAddingSaving];
	
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UIScrollviewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	[self.moneyTextField resignFirstResponder];
	[self.nameTextField resignFirstResponder];
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
	
	if(component == 0){
		
		return [self.monthsArray count];
		
	}else{
		
		return [self.yearsArray count];
		
	}
	return 1;
	
}

- (NSString *)pickerView:(UIPickerView *)pickerView
						 titleForRow:(NSInteger)row
						forComponent:(NSInteger)component{
	
	switch (component) {
		case 0:{
			return ((NSString*)self.monthsArray[row]);
		}
			break;
			
		case 1:{
			return ((NSString*)self.yearsArray[row]);
		}
			break;
			
		default:
			break;
	}

	return nil;
}


#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView
			didSelectRow:(NSInteger)row
			 inComponent:(NSInteger)component{

	if(component == 0){
		self.selectedMonthRowInPickerView = row;
		
	}
	if(component == 1){
		self.selectedYearRowInPickerView = row;
	}
	if(self.selectedYearRowInPickerView == self.forbidYearRowInPickerView && self.selectedMonthRowInPickerView <= self.forbidMonthRowInPickerView){
		[pickerView selectRow:self.forbidMonthRowInPickerView + 1 inComponent:0 animated:YES];
		self.selectedMonthRowInPickerView = (self.selectedMonthRowInPickerView + 1)%12;
	};
}












		 
@end
