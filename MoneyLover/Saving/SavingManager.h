//
//  SavingManager.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/1/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavingManager : NSObject

@property (strong, nonatomic) NSMutableArray *savingArray;
@property (strong, nonatomic) NSMutableArray *savingArrayAfterBalanceMoney;
@property (strong, nonatomic) NSCalendar *calendar;

+ (SavingManager *)getInstance;
- (void)caculateSaving;
//- (void)balanceMoneyInMonths;
- (void)caculateMonthlySaving;

@end
