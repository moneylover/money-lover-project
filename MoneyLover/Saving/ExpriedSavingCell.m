//
//  ExpriedSavingCell.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/31/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "ExpriedSavingCell.h"

@implementation ExpriedSavingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)optionAction:(id)sender {
	[self.delegate selectedOptionButtonInExpriedSaving:self.saving andSender:sender];
}
@end
