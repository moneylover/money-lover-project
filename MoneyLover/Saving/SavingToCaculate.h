//
//  SavingToCaculate.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/1/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavingToCaculate : NSObject

@property (assign, nonatomic) long long savingMoneyInMonths;
@property (assign, nonatomic) long long savedMoneyInMonths;
@property (assign, nonatomic) double endDate;
@property (copy, nonatomic) NSString *endDateString;
@property (assign, nonatomic) NSInteger numberOfMonth;
@property (assign, nonatomic) long long moneyPerMonth;

@end
