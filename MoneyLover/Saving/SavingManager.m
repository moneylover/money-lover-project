//
//  SavingManager.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/1/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "SavingManager.h"
#import "SQLiteManager.h"
#import "SavingToCaculate.h"
#import "NotificationCenterManager.h"

@implementation SavingManager

+ (SavingManager *)getInstance{
	static SavingManager *_singletonInstance;
	if(_singletonInstance == nil){
		_singletonInstance = [[SavingManager alloc] initPrivate];
	}
	return _singletonInstance;
}


-(id)initPrivate{
	if((self = [super init])){
		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
		self.savingArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
		self.savingArrayAfterBalanceMoney = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
		[self balanceMoneyInMonths];
	}
	return self;
}


- (id)init{
	[self doesNotRecognizeSelector:_cmd];
	return  nil;
}


- (void)caculateSaving{
	
//	self.savingArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
//	self.savingArrayAfterBalanceMoney = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
//	[self balanceMoneyInMonths];
//
	
	if(self.savingArray.count == 0 || self.savingArrayAfterBalanceMoney.count == 0){
		
		return;
		
	}
	
	// Kiểm tra xem tháng này đã tính số tiền tiết kiệm chi tiết chưa ?
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"yyyy-MM";
	NSDate *toDay = [NSDate date];
	
	NSString *currentMonth = [dateFormatter stringFromDate:toDay];
	NSString *lastCaculateSavingTime = [[NSUserDefaults standardUserDefaults] stringForKey:@"LastCaculateSavingTime"];
	
	
	if(lastCaculateSavingTime == nil || ([lastCaculateSavingTime compare:currentMonth] == NSOrderedAscending)){
		
		// Tháng này chưa tính số tiền tiết kiệm chi tiết
		[[NSUserDefaults standardUserDefaults] setObject:currentMonth forKey:@"LastCaculateSavingTime"];
		
		
		// Đưa ra thông báo kết quả tiết kiệm trong tháng trước
		
		long long previousMonthAmountSaving = [[SQLiteManager database] getPreviousMonthAmountSaving];
		
		NSDateComponents *dateComp = [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:toDay];
		[dateComp setDay:1];

		NSDate *firstDayOfMonth = [self.calendar dateFromComponents:dateComp];

		long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
		long long totalMoneyInPrivMonth = amountOfMoneyInWallet - [[SQLiteManager database] getAmountOfMoneyFromDate:firstDayOfMonth];
		
		NSString *message = [NSString stringWithFormat:@"Kết quả tiết kiệm trong tháng trước\n\nSố tiền cần tiết kiệm: %@\nSố tiền đã tiết kiệm được: %@", [MoneyStringFormatter stringMoneyWithLongLong:previousMonthAmountSaving], [MoneyStringFormatter stringMoneyWithLongLong:totalMoneyInPrivMonth]];
		NSString *title;
		
		if(totalMoneyInPrivMonth < previousMonthAmountSaving){
			
			title = [NSString stringWithFormat:@"Tiết kiệm thất bại"];
			
		}else{
			
			title = [NSString stringWithFormat:@"Tiết kiệm thành công"];
		}

		[[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
		
		
		// Lưu số tiền cần cho mỗi tháng, thực hiện hàm này cả khi thêm, sửa, xoá một khoản tiết kiệm
		[self caculateMonthlySaving];
		
		// Đưa ra thông bao nếu có
		[self  pushSavingNotification];
		
	}else{
		
		// Không cần làm gì
		
	}
	
}

// Tính toán lại tiền cần tiết kiệm cho mỗi tháng khi sang tháng mới hoặc thêm, sửa, xoá một khoản tiết kiệm
- (void)caculateMonthlySaving{
	
	self.savingArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
	self.savingArrayAfterBalanceMoney = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
	[self balanceMoneyInMonths];
	
	[[SQLiteManager database] deletePlaningMonthlySaving];
	
	for(int j = 0; j < self.savingArray.count; j++){
		
		SavingToCaculate *saving = (SavingToCaculate *)self.savingArray[j];
		
		NSDate *tempt = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
		NSDateComponents *dateCom= [[NSDateComponents alloc] init];
		
		for(int k = 0; k < saving.numberOfMonth; k++){
			
			dateCom.month = k - saving.numberOfMonth;
			
			NSDate *date = [self.calendar dateByAddingComponents:dateCom toDate:tempt options:0];
			NSDateComponents *dateComponents = [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
			
			
			for (int i = 0; i < self.savingArrayAfterBalanceMoney.count; i++) {
				
				SavingToCaculate *savingAfterFlatten = (SavingToCaculate *)self.savingArrayAfterBalanceMoney[i];
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				dateFormatter.dateFormat = @"yyyy - MM";
				NSString *stringFromDate = [dateFormatter stringFromDate:date];
				
				if ([savingAfterFlatten.endDateString compare:stringFromDate] == NSOrderedDescending) {
					
					NSString *monthString = [NSString stringWithFormat:@"Tháng %ld năm %ld", (long)dateComponents.month, (long)dateComponents.year];
					
//					NSLog(@"%@ %@", monthString,[MoneyStringFormatter stringMoneyWithLongLong:savingAfterFlatten.moneyPerMonth]);
					[[SQLiteManager database] insertPlaningSavingWithMonth:monthString andAmount:savingAfterFlatten.moneyPerMonth];
					break;
				}
				
				
			}
		}
	}

}

- (void)pushSavingNotification{

//	self.savingArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
	
	if(self.savingArray.count == 0){
		
		return;
		
	}
	
	SavingToCaculate *savingInCurrentMonth = (SavingToCaculate *)self.savingArray[0];
	
	long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
	
	if(amountOfMoneyInWallet < savingInCurrentMonth.moneyPerMonth){
		NSString *message = [NSString stringWithFormat:@"Số tiền hiện tại: %lld đ \nTháng này bạn cần %lld đ cho khoản tiết kiệm gần nhất.", amountOfMoneyInWallet, savingInCurrentMonth.moneyPerMonth];
		NSString *title = [NSString stringWithFormat:@"Tiến độ tiết kiệm không đảm bảo"];
		//		NSLog(@"%@", message);
		[[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
		
	}else{
		
		[self balanceMoneyInMonths];
		
		SavingToCaculate *savingInCurrentMonthAfterBalance = (SavingToCaculate *)self.savingArrayAfterBalanceMoney[0];
		
		if(amountOfMoneyInWallet < savingInCurrentMonthAfterBalance.moneyPerMonth){
			NSString *message = [NSString stringWithFormat:@"Số tiền hiện tại: %lld đ \nTháng này bạn cần %lld đ cho các khoản tiết kiệm.", amountOfMoneyInWallet, savingInCurrentMonthAfterBalance.moneyPerMonth];
			NSString *title = [NSString stringWithFormat:@"Tiến độ tiết kiệm không đảm bảo"];
			//			NSLog(@"%@", message);
			[[NotificationCenterManager getInstance] createNotificationWithContent:message andTitle:title];
		}
	}
	
}


- (void)balanceMoneyInMonths{
	
	self.savingArrayAfterBalanceMoney = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
	
	BOOL isFinish = NO;
	if(self.savingArrayAfterBalanceMoney.count == 0){
		
		return ;
	}
	
	
//	NSLog(@"\n\n***********************TRƯỚC khi \"làm mịn\"***************************\n\n");
//	long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
//	NSLog(@"\nTổng tiền: %lld", amountOfMoneyInWallet);
//	for (int i = 0; i < self.savingArray.count; i++) {
//		SavingToCaculate *saving = (SavingToCaculate *)self.savingArray[i];
//		
//		NSLog(@"\n\nKhoản tiết kiệm số: %d\nCần thêm: %lld\nĐã tiết kiệm được: %lld\nTháng: %@\nSố tháng tiết kiệm: %ld\nSố tiền mỗi tháng: %lld\n", i + 1,saving.savingMoneyInMonths, saving.savedMoneyInMonths, saving.endDateString, (long)saving.numberOfMonth, saving.moneyPerMonth);
//	}
	
	
	do{
		NSInteger startIndex = -1;
		NSInteger endIndex = - 1;
		NSMutableArray *groupedSaving = [[NSMutableArray alloc] init];
		NSMutableArray *newSavingArray = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < (self.savingArrayAfterBalanceMoney.count - 1); i++) {
			SavingToCaculate *prevSaving = (SavingToCaculate *)self.savingArrayAfterBalanceMoney[i];
			SavingToCaculate *nextSaving = (SavingToCaculate *)self.savingArrayAfterBalanceMoney[i+1];
			
			if(prevSaving.moneyPerMonth <= nextSaving.moneyPerMonth){
				if(groupedSaving.count == 0){
					startIndex = i;
					endIndex = i + 1;
					[groupedSaving addObject:prevSaving];
					[groupedSaving addObject:nextSaving];
				}else{
					[groupedSaving addObject:nextSaving];
					endIndex = i + 1;
				}
			}else{
				if(groupedSaving.count !=0){
					break;
				}
			}
		}
		
		if(groupedSaving.count != 0){
			SavingToCaculate *finalSaving = [[SavingToCaculate alloc] init];
			for (int j = 0; j < groupedSaving.count; j++) {
				SavingToCaculate *iteratorSaving = (SavingToCaculate *)groupedSaving[j];
				finalSaving.savingMoneyInMonths += iteratorSaving.savingMoneyInMonths;
				finalSaving.savedMoneyInMonths  += iteratorSaving.savedMoneyInMonths;
				finalSaving.numberOfMonth += iteratorSaving.numberOfMonth;
				
				if(j == groupedSaving.count - 1){
					finalSaving.endDate = iteratorSaving.endDate;
					finalSaving.endDateString  = iteratorSaving.endDateString;
					finalSaving.moneyPerMonth = finalSaving.savingMoneyInMonths/finalSaving.numberOfMonth;
				}
			}
			
			for (int i = 0; i <= (startIndex - 1); i++) {
				[newSavingArray addObject:self.savingArrayAfterBalanceMoney[i]];
			}
			
			[newSavingArray addObject:finalSaving];
			
			for (NSInteger i = (endIndex + 1); i < self.savingArrayAfterBalanceMoney.count; i++) {
				[newSavingArray addObject:self.savingArrayAfterBalanceMoney[i]];
			}
			self.savingArrayAfterBalanceMoney = newSavingArray;
		}else{
			isFinish = YES;
		}
		
	}while(isFinish == NO);
	
	
//	NSLog(@"\n\n***********************SAU khi \"làm mịn\"***************************\n\n");
//	NSLog(@"\nTổng tiền: %lld", amountOfMoneyInWallet);
//	for (int i = 0; i < self.savingArray.count; i++) {
//		SavingToCaculate *saving = (SavingToCaculate *)self.savingArray[i];
//		
//		NSLog(@"\n\nKhoản tiết kiệm số: %d\nCần thêm: %lld\nĐã tiết kiệm được: %lld\nTháng: %@\nSố tháng tiết kiệm: %ld\nSố tiền mỗi tháng: %lld\n", i + 1,saving.savingMoneyInMonths, saving.savedMoneyInMonths, saving.endDateString, (long)saving.numberOfMonth, saving.moneyPerMonth);
//		
//	}
	
	
	//	return self.savingArray;
}




/*
- (void)balanceMoneyInMonths{
	
	self.savingArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
	
	BOOL isFinish = NO;
	if(self.savingArray.count == 0){
		
		return ;
	}
	
	
	 NSLog(@"\n\n***********************TRƯỚC khi \"làm mịn\"***************************\n\n");
	 long long amountOfMoneyInWallet = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
	 NSLog(@"\nTổng tiền: %lld", amountOfMoneyInWallet);
	 for (int i = 0; i < self.savingArray.count; i++) {
		SavingToCaculate *saving = (SavingToCaculate *)self.savingArray[i];
		
		NSLog(@"\n\nKhoản tiết kiệm số: %d\nCần thêm: %lld\nĐã tiết kiệm được: %lld\nTháng: %@\nSố tháng tiết kiệm: %ld\nSố tiền mỗi tháng: %lld\n", i + 1,saving.savingMoneyInMonths, saving.savedMoneyInMonths, saving.endDateString, (long)saving.numberOfMonth, saving.moneyPerMonth);
	 }
	
	
	do{
		NSInteger startIndex = -1;
		NSInteger endIndex = - 1;
		NSMutableArray *groupedSaving = [[NSMutableArray alloc] init];
		NSMutableArray *newSavingArray = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < (self.savingArray.count - 1); i++) {
			SavingToCaculate *prevSaving = (SavingToCaculate *)self.savingArray[i];
			SavingToCaculate *nextSaving = (SavingToCaculate *)self.savingArray[i+1];
			
			if(prevSaving.moneyPerMonth <= nextSaving.moneyPerMonth){
				if(groupedSaving.count == 0){
					startIndex = i;
					endIndex = i + 1;
					[groupedSaving addObject:prevSaving];
					[groupedSaving addObject:nextSaving];
				}else{
					[groupedSaving addObject:nextSaving];
					endIndex = i + 1;
				}
			}else{
				if(groupedSaving.count !=0){
					break;
				}
			}
		}
		
		if(groupedSaving.count != 0){
			SavingToCaculate *finalSaving = [[SavingToCaculate alloc] init];
			for (int j = 0; j < groupedSaving.count; j++) {
				SavingToCaculate *iteratorSaving = (SavingToCaculate *)groupedSaving[j];
				finalSaving.savingMoneyInMonths += iteratorSaving.savingMoneyInMonths;
				finalSaving.savedMoneyInMonths  += iteratorSaving.savedMoneyInMonths;
				finalSaving.numberOfMonth += iteratorSaving.numberOfMonth;
				
				if(j == groupedSaving.count - 1){
					finalSaving.endDate = iteratorSaving.endDate;
					finalSaving.endDateString  = iteratorSaving.endDateString;
					finalSaving.moneyPerMonth = finalSaving.savingMoneyInMonths/finalSaving.numberOfMonth;
				}
			}
			
			for (int i = 0; i <= (startIndex - 1); i++) {
				[newSavingArray addObject:self.savingArray[i]];
			}
			
			[newSavingArray addObject:finalSaving];
			
			for (NSInteger i = (endIndex + 1); i < self.savingArray.count; i++) {
				[newSavingArray addObject:self.savingArray[i]];
			}
			self.savingArray = newSavingArray;
		}else{
			isFinish = YES;
		}
		
	}while(isFinish == NO);
	
	
	 NSLog(@"\n\n***********************SAU khi \"làm mịn\"***************************\n\n");
	 NSLog(@"\nTổng tiền: %lld", amountOfMoneyInWallet);
	 for (int i = 0; i < self.savingArray.count; i++) {
		SavingToCaculate *saving = (SavingToCaculate *)self.savingArray[i];
		
		NSLog(@"\n\nKhoản tiết kiệm số: %d\nCần thêm: %lld\nĐã tiết kiệm được: %lld\nTháng: %@\nSố tháng tiết kiệm: %ld\nSố tiền mỗi tháng: %lld\n", i + 1,saving.savingMoneyInMonths, saving.savedMoneyInMonths, saving.endDateString, (long)saving.numberOfMonth, saving.moneyPerMonth);
		
	 }
	
	
//	return self.savingArray;
}
*/


@end
