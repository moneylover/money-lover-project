//
//  SavingCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/24/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Saving.h"

@protocol SavingCellDelegate <NSObject>

@required
- (void)selectedOptionButtonInRuningSaving:(Saving *)saving andSender:(UIButton *)sender;

@end

@interface SavingCell : UITableViewCell

//@property (assign, nonatomic) NSInteger cellIndex;
@property (weak, nonatomic) id <SavingCellDelegate> delegate;
@property (strong, nonatomic) Saving *saving;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goalMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *neededMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *optionButton;

@end
