//
//  OptionTableForSavingCellViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/31/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OptionTableForSavingCellViewControllerDelegate

@required
- (void)selectedOptionAtIndex:(NSInteger)index;

@end

@interface OptionTableForSavingCellViewController : UITableViewController

@property (weak, nonatomic) id <OptionTableForSavingCellViewControllerDelegate> delegate;
@property (assign, nonatomic) NSInteger numberOfOptions;

- (instancetype)initWithNumberOfOption:(NSInteger)numberOfOption;

@end
