//
//  SavingsViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/24/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface SavingsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *addSavingButton;

@end
