//
//  SavingsViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/24/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "SavingsViewController.h"
#import "Saving.h"
#import "SavingCell.h"
#import "SQLiteManager.h"
#import "AddSavingViewController.h"
#import "ExpriedSavingCell.h"
#import "FPPopoverController.h"
#import "OptionTableForSavingCellViewController.h"
#import "OptionTableViewController.h"
#import "SavingManager.h"
#import "DetailSavingTableView.h"
#import "OverViewSavingCell.h"
#import "SavingToCaculate.h"

@interface SavingsViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate,	AddSavingDelegate, SavingCellDelegate, ExpriedSavingCellDelegate, OptionTableForSavingCellViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *runingButton;
@property (weak, nonatomic) IBOutlet UIButton *finishedButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UITableView *runningSavingTableView;
@property (strong, nonatomic) UITableView *expriedSavingsTableView;

@property (strong, nonatomic) NSMutableArray *runningSavingArray;
@property (strong, nonatomic) NSMutableArray *expriedSavingArray;
@property (strong, nonatomic) NSMutableArray *moneyArrayWhenSavingsExpried;

@property (strong, nonatomic) FPPopoverController *popoverViewController;
@property (strong, nonatomic) Saving *selectedSaving;
@property (strong, nonatomic) FPPopoverController *showDetailPopoverViewController;

@property (strong, nonatomic) UILabel *emptyDataLabel_left;
@property (strong, nonatomic) UILabel *emptyDataLabel_right;

@end

@implementation SavingsViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	//	[[SavingManager getInstance] caculateSavingForCurrentMonth];
	
	//	self.runningSavingArray = [[SQLiteManager database] getSavingsIsExpried:NO];
	//	self.expriedSavingArray = [[SQLiteManager database] getSavingsIsExpried:YES];
	
	CGFloat tableViewWidth = [[UIScreen mainScreen] bounds].size.width;
	CGFloat tableViewHeight = [UIScreen mainScreen].bounds.size.height - 108;
	
	[self.scrollView setContentSize:CGSizeMake(tableViewWidth*2, tableViewHeight)];
	self.scrollView.delegate = self;
	
	
	// Tạo UILabel bên trái khi không có dữ liệu
	self.emptyDataLabel_left = [[UILabel alloc] initWithFrame:CGRectMake(70, tableViewHeight/2, 100, 50)];
	self.emptyDataLabel_left.text = @"Không có dữ liệu";
	[self.emptyDataLabel_left setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel_left setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel_left sizeToFit];
	
	[self.scrollView addSubview:self.emptyDataLabel_left];
	
	
	// Tạo UILabel bên phải khi không có dữ liệu
	self.emptyDataLabel_right = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width + 70, tableViewHeight/2, 100, 30)];
	self.emptyDataLabel_right.text = @"Không có dữ liệu";
	[self.emptyDataLabel_right setFont:[UIFont systemFontOfSize:23]];
	[self.emptyDataLabel_right setTextColor:[UIColor lightGrayColor]];
	[self.emptyDataLabel_right sizeToFit];
	
	[self.scrollView addSubview:self.emptyDataLabel_right];
	
	
	// Khởi tạo bảng cho running saving
	self.runningSavingTableView = [[UITableView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, tableViewWidth - 20, tableViewHeight - 10) style:UITableViewStyleGrouped];
	self.runningSavingTableView.delegate = self;
	self.runningSavingTableView.dataSource = self;
	self.runningSavingTableView.tag = 1;
	[self.runningSavingTableView registerNib:[UINib nibWithNibName:@"SavingCell" bundle:nil] forCellReuseIdentifier:@"CellIden"];
	self.runningSavingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	[self.scrollView addSubview:_runningSavingTableView];
	
	
	// Khởi tạo bảng cho expried saving
	self.expriedSavingsTableView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewWidth + 10, 10.0f, tableViewWidth - 20 , tableViewHeight - 10) style:UITableViewStyleGrouped];
	self.expriedSavingsTableView.delegate = self;
	self.expriedSavingsTableView.dataSource = self;
	self.expriedSavingsTableView.tag = 2;
	[self.expriedSavingsTableView registerNib:[UINib nibWithNibName:@"ExpriedSavingCell" bundle:nil] forCellReuseIdentifier:@"ExpriedSavingCellIden"];
	self.expriedSavingsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.scrollView addSubview:_expriedSavingsTableView];
	
	
	// Thiết lập UIScrollView
	self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tableViewHeight - 6, 0);
	self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
	self.scrollView.pagingEnabled = YES;
	
	[_runingButton addTarget:self action:@selector(runingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	[_finishedButton addTarget:self action:@selector(finishedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	
	
	[self.runningSavingTableView registerNib:[UINib nibWithNibName:@"OverViewSavingCell" bundle:nil] forCellReuseIdentifier:@"OverviewSavingCellIden"];
	
	_runingButton.enabled = NO;
	_finishedButton.enabled = YES;
	
	[self reloadSavngViewController];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	//	[self reloadSavngViewController];
	
}

- (IBAction)showDetailSaving:(id)sender {
	
	[[SavingManager getInstance] caculateSaving];
	
	DetailSavingTableView *detailTableSaving = [[DetailSavingTableView alloc] initWithStyle:UITableViewStyleGrouped];
	//	detailTableSaving.title = @"Số tiền cần tiết kiệm hằng tháng";
	detailTableSaving.title = @"Tiết kiệm hằng tháng";
	self.showDetailPopoverViewController = [[FPPopoverController alloc] initWithViewController:detailTableSaving];
	
	self.showDetailPopoverViewController.contentSize = CGSizeMake(300,400);
	self.showDetailPopoverViewController.border = NO;
	self.showDetailPopoverViewController.arrowDirection = FPPopoverNoArrow;
	self.showDetailPopoverViewController.tint = FPPopoverWhiteTint;
	
	[self.showDetailPopoverViewController presentPopoverFromView:sender];

}


#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	
	if(tableView.tag == 1){
		
		if(self.runningSavingArray.count == 0){
			return 0;
		}else{
			return [self.runningSavingArray count] + 1;
		}
		
		
	}else{
		
		return [self.expriedSavingArray count];
		
	}
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	if(tableView.tag == 1){
		
		if(indexPath.section == 0){
			
			return 195;
			
		}else{
			
			return 240.0;
			
		}
		
	}else{
		
		return 260.0;
		
	}
	
	return 10.0;
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	if(tableView.tag == 1){
		
		if(indexPath.section == 0){
			
			OverViewSavingCell *overViewCell = (OverViewSavingCell *)[tableView dequeueReusableCellWithIdentifier:@"OverviewSavingCellIden"];
			
			long long amountOfMoney = [[NSNumber numberWithDouble:[[NSUserDefaults standardUserDefaults] doubleForKey:@"AmountOfMoney"]] longLongValue];
			
			SavingToCaculate *saving = (SavingToCaculate*)[SavingManager getInstance].savingArrayAfterBalanceMoney[0];
			
			
			overViewCell.currentAmountLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:amountOfMoney];
			overViewCell.thisMonthAmountLbl.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.moneyPerMonth];
			long long tempt = saving.moneyPerMonth - amountOfMoney;
			
			overViewCell.needingAmountLbl.text = [MoneyStringFormatter stringMoneyWithLongLong:tempt >= 0 ? tempt : 0];
			
			return overViewCell;
			
		}else{
			Saving *saving = (Saving *)self.runningSavingArray[indexPath.section - 1];
			SavingCell *cell = (SavingCell *)[tableView dequeueReusableCellWithIdentifier:@"CellIden"];
			
			cell.iconImageView.image = [UIImage imageNamed:saving.iconName];
			cell.nameLabel.text = saving.name;
			cell.goalMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.money];
			cell.savedMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.savedMoney];
			cell.neededMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.money - saving.savedMoney];
			
			NSString *year = [saving.endDateString substringToIndex:4];
			NSString *month = [saving.endDateString substringFromIndex:7];
			cell.timeLabel.text = [NSString stringWithFormat:@"Tháng %@ - Năm %@", month, year];
			cell.progressView.progress = (saving.savedMoney*1.0)/saving.money;
			
			cell.saving = saving;
			cell.delegate = self;
			
			return cell;
		}
		
	
	}else{
		
		
		Saving *saving = (Saving *)self.expriedSavingArray[indexPath.section];
		
		ExpriedSavingCell *cell = (ExpriedSavingCell *)[tableView dequeueReusableCellWithIdentifier:@"ExpriedSavingCellIden"];
		
		cell.iconImageView.image = [UIImage imageNamed:saving.iconName];
		cell.nameLabel.text = saving.name;
		cell.goalMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.money];
		cell.savedMoneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:saving.savedMoney];
		NSString *year = [saving.endDateString substringToIndex:4];
		NSString *month = [saving.endDateString substringFromIndex:7];
		cell.timeLabel.text = [NSString stringWithFormat:@"Tháng %@ - Năm %@", month, year];
		
		//		NSLog(@"%@ %lld", saving.name, saving.savedMoney);
		
		if(saving.money == saving.savedMoney){
			cell.stateSavingLabel.text = @"Thành công";
			cell.stateSavingLabel.textColor = [UIColor blueColor];
		}else{
			cell.stateSavingLabel.text = @"Thất bại";
			cell.stateSavingLabel.textColor = [UIColor redColor];
		}
		
		
		cell.saving = saving;
		cell.delegate = self;
		return cell;
	}
	return nil;
}



- (void)runingButtonAction:(UIButton*)button{
	_runingButton.enabled = NO;
	_finishedButton.enabled = YES;
	
	[_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)finishedButtonAction:(UIButton*)button{
	_finishedButton.enabled = NO;
	_runingButton.enabled = YES;
	
	[_scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	CGFloat contentOffSetX = scrollView.contentOffset.x;
	if(contentOffSetX == 0){
		_finishedButton.enabled = YES;
		_runingButton.enabled = NO;
	}else{
		if(scrollView.contentOffset.x == [[UIScreen mainScreen] bounds].size.width){
			_finishedButton.enabled = NO;
			_runingButton.enabled = YES;
		}
	}
}

#pragma - Slidemenu methods
- (IBAction)showSlideMenu:(id)sender {
	[self.delegate showSlideMenu];
}

#pragma - Add savings
- (IBAction)AddSavings:(id)sender {
	AddSavingViewController *addSavingVC = [[AddSavingViewController alloc] initWithNibName:@"AddSavingViewController" bundle:nil];
	addSavingVC.delegate = self;
	
	[self.navigationController pushViewController:addSavingVC animated:YES];
}


#pragma - AddSavingDelegate

- (void)completeAddingSaving{
	
	[self reloadSavngViewController];
	
}

- (void)cancelAddingSaving{
	//	NSLog(@"Cancel");
}

- (void)reloadSavngViewController{
	
	self.runningSavingArray = [[SQLiteManager database] getSavingsIsExpried:NO];
	self.expriedSavingArray = [[SQLiteManager database] getSavingsIsExpried:YES];
	
	[self.runningSavingTableView reloadData];
	[self.expriedSavingsTableView reloadData];
	
	
	if([self.runningSavingArray count] == 0){
		
		self.emptyDataLabel_left.hidden = NO;
		self.runningSavingTableView.hidden = YES;
		//		self.runningSavingTableView.alpha = 0;
	}else{
		
		self.emptyDataLabel_left.hidden = YES;
		self.runningSavingTableView.hidden = NO;
		
	}
	
	
	if([self.expriedSavingArray count] == 0){
		
		self.emptyDataLabel_right.hidden = NO;
		self.expriedSavingsTableView.hidden = YES;
		
	}else{
		
		self.emptyDataLabel_right.hidden = YES;
		self.expriedSavingsTableView.hidden = NO;
	}
}

#pragma - ExpriedSavingCellDelegate

- (void)selectedOptionButtonInExpriedSaving:(Saving *)saving andSender:(UIButton *)sender{
	self.selectedSaving = saving;
	OptionTableForSavingCellViewController *tableViewController = [[OptionTableForSavingCellViewController alloc] initWithNumberOfOption:1];
	
	tableViewController.delegate = self;
	
	self.popoverViewController = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverViewController.contentSize = CGSizeMake(140,65);
	self.popoverViewController.border = NO;
	self.popoverViewController.arrowDirection = FPPopoverArrowDirectionUp;
	self.popoverViewController.tint = FPPopoverLightGrayTint;
	
	[self.popoverViewController presentPopoverFromView:sender];
	
}

#pragma - SavingCellDelegate

- (void)selectedOptionButtonInRuningSaving:(Saving *)saving andSender:(UIButton *)sender{
	
	self.selectedSaving = saving;
	OptionTableForSavingCellViewController *tableViewController = [[OptionTableForSavingCellViewController alloc] initWithNumberOfOption:2];
	
	tableViewController.delegate = self;
	
	self.popoverViewController = [[FPPopoverController alloc] initWithViewController:tableViewController];
	self.popoverViewController.contentSize = CGSizeMake(140,90);
	self.popoverViewController.border = NO;
	self.popoverViewController.arrowDirection = FPPopoverArrowDirectionVertical;
	self.popoverViewController.tint = FPPopoverLightGrayTint;
	
	[self.popoverViewController presentPopoverFromView:sender];
	
}

- (void)selectedOptionAtIndex:(NSInteger)index{
	//	NSLog(@"%d", index);
	[self.popoverViewController dismissPopoverAnimated:YES];
	
	if(self.selectedSaving.expried == YES){
		
		if (index == 0) {
			
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
																message:@"Bạn có chắc muốn xoá khoản tiết kiệm này không?"
															   delegate:self
													  cancelButtonTitle:@"Không"
													  otherButtonTitles:@"Có", nil];
			[alertView show];
			
		}
		
	}else{
		
		if (index == 0) {
			
			AddSavingViewController *addSavingVC = [[AddSavingViewController alloc] initWithNibName:@"AddSavingViewController" bundle:nil];
			addSavingVC.delegate = self;
			addSavingVC.editedSaving = self.selectedSaving;
			
			[self.navigationController pushViewController:addSavingVC animated:YES];
			
		}else{
			
			if(index == 1){
				
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cảnh báo"
																	message:@"Bạn có chắc muốn xoá khoản tiết kiệm này không?"
																   delegate:self
														  cancelButtonTitle:@"Không"
														  otherButtonTitles:@"Có", nil];
				[alertView show];
				
			}
			
		}
		
	}
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(buttonIndex == 0){
		
	}else{
		[[SQLiteManager database] deleteSavingWithId:self.selectedSaving.Id];
		
		//		self.runningSavingArray = [[SQLiteManager database] getSavingsIsExpried:NO];
		//		self.expriedSavingArray = [[SQLiteManager database] getSavingsIsExpried:YES];
		//		[self.runningSavingTableView reloadData];
		//		[self.expriedSavingsTableView reloadData];
		
		[self reloadSavngViewController];
		
		// Cancel local notification
		NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
		for (UILocalNotification *localNotification in localNotifications){
			NSDictionary *userInfo = localNotification.userInfo;
			NSString *localNotificationWithSavingName = [userInfo valueForKey:@"SavingID"];
			if([localNotificationWithSavingName isEqualToString:self.selectedSaving.name]){
				[[UIApplication sharedApplication] cancelLocalNotification:localNotification];
				break;
			}
		}
		
		[[SavingManager getInstance] caculateMonthlySaving];
	}
}

@end
