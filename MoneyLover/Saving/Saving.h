//
//  Saving.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Saving : NSObject

@property (assign, nonatomic) NSInteger Id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *iconName;
@property (assign, nonatomic) long long money;
@property (assign, nonatomic) double endDate;
@property (copy, nonatomic) NSString *endDateString;
@property (assign, nonatomic) BOOL expried;

@property (assign, nonatomic) long long savedMoney;

@end
