//
//  ExpriedSavingCell.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/31/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Saving.h"

@protocol ExpriedSavingCellDelegate <NSObject>

@required
- (void)selectedOptionButtonInExpriedSaving:(Saving *)saving andSender:(UIButton *)sender;

@end

@interface ExpriedSavingCell : UITableViewCell

//@property (assign, nonatomic) NSInteger cellIndex;
@property (weak, nonatomic) id <ExpriedSavingCellDelegate> delegate;
@property (strong, nonatomic) Saving *saving;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *optionButton;
@property (weak, nonatomic) IBOutlet UILabel *goalMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateSavingLabel;

@end
