//
//  AddSavingViewController.h
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/30/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Saving.h"

@protocol AddSavingDelegate <NSObject>

@required
- (void) completeAddingSaving;
- (void) cancelAddingSaving;

@end

@interface AddSavingViewController : UIViewController

@property (weak, nonatomic) id <AddSavingDelegate> delegate;
@property (strong, nonatomic) Saving *editedSaving;

@end
