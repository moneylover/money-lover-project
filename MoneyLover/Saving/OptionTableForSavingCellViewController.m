//
//  OptionTableForSavingCellViewController.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 10/31/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "OptionTableForSavingCellViewController.h"

@interface OptionTableForSavingCellViewController ()

@end

@implementation OptionTableForSavingCellViewController

- (instancetype)initWithNumberOfOption:(NSInteger)numberOfOption{
	self = [self init];
	self.numberOfOptions = numberOfOption;
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	[self.tableView registerClass:[UITableViewCell class]  forCellReuseIdentifier:@"CellOptionIden"];
	self.tableView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
	return _numberOfOptions;
	
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOptionIden" forIndexPath:indexPath];
	
	if(_numberOfOptions == 1){
		cell.imageView.image = [UIImage imageNamed:@"ic_bab_delete.png"];
		cell.textLabel.text = @"Xoá";
	}else{
		if(_numberOfOptions == 2){
			switch (indexPath.row) {
				case 0:{
					cell.imageView.image = [UIImage imageNamed:@"ic_ab_adjustment.png"];
					cell.textLabel.text = @"Sửa";
				}
					break;
				case 1:{
					cell.imageView.image = [UIImage imageNamed:@"ic_bab_delete.png"];
					cell.textLabel.text = @"Xoá";
				}
					break;
				default:
					break;
			}
		}
	}

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 25;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self.delegate selectedOptionAtIndex:indexPath.row];
}

@end
