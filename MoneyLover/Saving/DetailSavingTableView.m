//
//  DetailSavingTableView.m
//  MoneyLover
//
//  Created by Ngo Huu Tuan on 11/2/15.
//  Copyright (c) 2015 Ngo Huu Tuan. All rights reserved.
//

#import "DetailSavingTableView.h"
#import "SavingToCaculate.h"
#import "SavingManager.h"
#import "SQLiteManager.h"

@interface DetailSavingTableView ()

@property (strong, nonatomic) NSMutableArray *planingSavingArray;
@property (strong, nonatomic) NSMutableArray *savingsArray;
//@property (strong, nonatomic) NSMutableArray	*savingsArrayAfterFlatten;

@property (assign, nonatomic) NSInteger currentMonth;
@property (assign, nonatomic) NSInteger currentYear;
@property (assign, nonatomic) NSInteger endMonth;
@property (assign, nonatomic) NSInteger endYear;
@property (strong, nonatomic) NSCalendar *calendar;
@end

@implementation DetailSavingTableView

- (void)viewDidLoad {
    [super viewDidLoad];
	
//	[[SavingManager getInstance] balanceMoneyInMonths];
	
//	self.savingsArray = [[SQLiteManager database] getRuningSavingsToCaculateSaving];
//	
//	[[SavingManager getInstance] balanceMoneyInMonths];
	
	self.planingSavingArray = [[SQLiteManager database] getPlaningMonthlySaving];
//	NSLog(@"%ld", (long)self.planingSavingArray.count);
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIden"];
//	self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//	self.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
	self.title = @"Tiết kiệm hằng tháng";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
//	if(self.savingsArray.count != 0){
//		return [self.savingsArray count];
//	}
//	NSLog(@"numberOfSectionsInTableView %ld", (long)[SavingManager getInstance].savingArray.count);
	return [SavingManager getInstance].savingArray.count;
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if([SavingManager getInstance].savingArray.count != 0){
		SavingToCaculate *saving = (SavingToCaculate *)[SavingManager getInstance].savingArray[section];
//		NSLog(@" numberOfRowsInSection%ld", (long)saving.numberOfMonth);
		return saving.numberOfMonth;
	}
	
	// Return the number of rows in the section.
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden" forIndexPath:indexPath];
//	
//	SavingToCaculate *saving = (SavingToCaculate *)self.savingsArray[indexPath.section];
//	
//	NSDate *tempt = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
//	NSDateComponents *dateCom= [[NSDateComponents alloc] init];
//	dateCom.month = -saving.numberOfMonth + indexPath.row;
//	
//	NSDate *date = [self.calendar dateByAddingComponents:dateCom toDate:tempt options:0];
//	NSDateComponents *dateComponents = [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
	
	NSInteger indexOfPlaingSaving = indexPath.row;
	for (int i = 0; i < indexPath.section; i++) {
		SavingToCaculate *saving = (SavingToCaculate *)[SavingManager getInstance].savingArray[i];
		indexOfPlaingSaving += saving.numberOfMonth;
	}
	
	NSMutableDictionary *aDictionary = (NSMutableDictionary *)self.planingSavingArray[indexOfPlaingSaving];
	
	cell.textLabel.text = (NSString *) [aDictionary objectForKey:@"Month"];
	cell.textLabel.font = [UIFont systemFontOfSize:13];
	cell.textLabel.textColor = [UIColor darkGrayColor];
	
	
	UILabel *moneyLabel = [[UILabel alloc] init];
	moneyLabel.font = [UIFont systemFontOfSize:13];
	moneyLabel.textColor = [UIColor darkGrayColor];
	moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:((NSNumber *)[aDictionary objectForKey:@"Amount"]).longLongValue];
	[moneyLabel sizeToFit];
	cell.accessoryView = moneyLabel;
//	NSLog(@"%@", moneyLabel.text);
	/*
	for (int i = 0; i < [SavingManager getInstance].savingArray.count; i++) {
		SavingToCaculate *savingAfterFlatten = (SavingToCaculate *)[SavingManager getInstance].savingArray[i];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		dateFormatter.dateFormat = @"yyyy - MM";
		NSString *tempt = [dateFormatter stringFromDate:date];
		
		if ([savingAfterFlatten.endDateString compare:tempt] == NSOrderedDescending) {
			UILabel *moneyLabel = [[UILabel alloc] init];
			moneyLabel.font = [UIFont systemFontOfSize:13];
			moneyLabel.textColor = [UIColor darkGrayColor];
			moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:savingAfterFlatten.moneyPerMonth];
			[moneyLabel sizeToFit];
			cell.accessoryView = moneyLabel;
			break;
		}
	}
	*/
	return cell;
	
}





/*

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	if(self.savingsArray.count != 0){
		return [self.savingsArray count];
	}
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if(self.savingsArray.count != 0){
		SavingToCaculate *saving = (SavingToCaculate *)self.savingsArray[section];
		return saving.numberOfMonth;
	}
    // Return the number of rows in the section.
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIden" forIndexPath:indexPath];

	SavingToCaculate *saving = (SavingToCaculate *)self.savingsArray[indexPath.section];
	
	NSDate *tempt = [NSDate dateWithTimeIntervalSinceReferenceDate:saving.endDate];
	NSDateComponents *dateCom= [[NSDateComponents alloc] init];
	dateCom.month = -saving.numberOfMonth + indexPath.row;

	NSDate *date = [self.calendar dateByAddingComponents:dateCom toDate:tempt options:0];
	NSDateComponents *dateComponents = [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
	
	cell.textLabel.text = [NSString stringWithFormat:@"Tháng %ld năm %ld", (long)dateComponents.month, (long)dateComponents.year];
	cell.textLabel.font = [UIFont systemFontOfSize:13];
	cell.textLabel.textColor = [UIColor darkGrayColor];
	
	for (int i = 0; i < [SavingManager getInstance].savingArray.count; i++) {
		SavingToCaculate *savingAfterFlatten = (SavingToCaculate *)[SavingManager getInstance].savingArray[i];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		dateFormatter.dateFormat = @"yyyy - MM";
		NSString *tempt = [dateFormatter stringFromDate:date];
		
		if ([savingAfterFlatten.endDateString compare:tempt] == NSOrderedDescending) {
			UILabel *moneyLabel = [[UILabel alloc] init];
			moneyLabel.font = [UIFont systemFontOfSize:13];
			moneyLabel.textColor = [UIColor darkGrayColor];
			moneyLabel.text = [MoneyStringFormatter stringMoneyWithLongLong:savingAfterFlatten.moneyPerMonth];
			[moneyLabel sizeToFit];
			cell.accessoryView = moneyLabel;
			break;
		}
	}
	
  return cell;

}

*/

@end
